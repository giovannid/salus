<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

    public function index($msg = null, $codigo = null, $filtrar = null, $valor = null)
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;

        $dia_atual = new DateTime();
        $dia_atual->setTime(0,0,0);
        $data['dia_atual']      = $dia_atual;
        $data['dia_atual_data'] = $dia_atual->format('d/m/Y');
        $data['data_consulta']  = $dia_atual;

        $data['consultas'] = $this->consulta->consultar(null, $dia_atual);
        $data['pacientes'] = $this->paciente->consultar();
        
        if($this->input->post('consulta_data') !== null) {
            redirect('consultas/index/0/0/data/' . str_replace('/', '-', $this->input->post('consulta_data')) );
        }

        if($this->input->post('consulta_paciente') !== null) {
            redirect('consultas/index/0/0/paciente/' . $this->input->post('consulta_paciente') );
        }

        if($filtrar == 'data' && $valor != null) {
            try {
                $data_consulta = new DateTime($valor);
                $data_consulta->setTime(0,0,0);
                $data['data_consulta']  = $data_consulta;
                $data['consultas']      = $this->consulta->consultar(null, $data_consulta);
                $data['dia_atual_data'] = $data_consulta->format('d/m/Y');
            } catch(Exception $e) {
                redirect('consultas/index/erro/61');
            }
        } else if($filtrar == 'paciente' && $valor != null) {
            $consultas            = $this->consulta->consultar(null, null, null, $valor);
            $data['consultas']    = $consultas;
            $data['paciente_sel'] = $valor;

            if(!empty($consultas)) {
                $data['paciente_nome'] = array_shift($consultas)[0]->get_paciente()->get_nome_completo();            
            }
        } else if($filtrar == 'todas') {
            $data['consultas'] = $this->consulta->consultar();
        }
        $this->load->view('consulta-consultar', $data);
    }

    public function cadastrar($msg = null, $codigo = null, $id_paciente = null)
    {
        $data['msg']       = $msg;
        $data['codigo']    = $codigo;
        $data['pacientes'] = $this->paciente->consultar();
        
        if( $this->input->post('consulta_novo_paciente') != null ) {
            redirect('consultas/cadastrar/0/0/' . $this->input->post('consulta_novo_paciente'));
        }

        $paciente = $this->paciente->consultar($id_paciente);
        if( $id_paciente !== null && !empty($paciente) ) {

            $data['dia_hoje'] = (new DateTime())->format('d/m/Y');
            $data['horarios'] = $this->consulta->pegarHorarios();
            $data['paciente'] = $paciente;

            $supervisores = $this->usuario->consultar(null, null, null, [2]);
            // usuários vinculados a este paciente
            $usuarios_vinculados = $this->prontuario->consultarUsuariosPorPaciente($paciente, true);

            $data['usuarios'] =  array_merge($supervisores, $usuarios_vinculados);

            $this->load->view('consulta-cadastrar-parte-2', $data);

        } else {
            $this->load->view('consulta-cadastrar', $data);
        }

    }
    
    public function cadastrar_form($id_paciente = null) 
    {
        $form_info = $this->input->post();

        if( $id_paciente                            !== null && 
            $form_info['consulta_novo_data']        !== '' &&
            $form_info['consulta_novo_horario']     !== '' &&
            $form_info['consulta_novo_usuario']     !== '' ) {

            $paciente = $this->paciente->consultar($id_paciente);
            if(empty($paciente)) {
                redirect('consultas/index/erro/80');
                return;
            }

            $horario = $this->consulta->pegarHorarios($form_info['consulta_novo_horario']);
            $horario = array_shift($horario);
            
            try {
                $data = new DateTime( str_replace('/', '-', $form_info['consulta_novo_data']) );
                $data->setTime(0,0,0);
            } catch(Exception $e) {
                log_message('error', 'Não foi possivel converter a data passada na pagina de cadastro de consultas');
                redirect('consultas/index/erro/59' );
                return;
            }

            $data_atual = new DateTime();
            $data_atual->setTime(0,0,0);

            try {
                $hora_final = (new DateTime())->setTimestamp(strtotime($horario[0]));
                $hora_atual = new DateTime();

                if($data == $data_atual) {
                    if( $hora_final < $hora_atual ) {
                        redirect('consultas/cadastrar/erro/60/' . $id_paciente);
                        return;
                    }
                }

            } catch (Exception $e) {
                log_message('error', 'Não foi possivel criar instancia DateTime com horario passado no formulario de cadastro da Consulta');
                redirect('consultas/index/erro/59' );
                return;
            }

            if($data < $data_atual) {
                redirect('consultas/cadastrar/erro/60'. $id_paciente);
                return;
            } else {

                // consultas podem ser cadastradas na mesma data e mesmo dia, mas somente para pacientes diferentes
                $verificar_consulta = $this->consulta->consultar(null, null, [$data, $horario[0], $horario[1], $paciente->get_id_paciente()]);
                
                if( !empty($verificar_consulta) ) {
                    $verificar_consulta = array_shift($verificar_consulta);
                    foreach($verificar_consulta as $index => $consulta) {
                        if( $consulta->get_paciente()->get_id_paciente() == $id_paciente ) {
                            redirect('consultas/cadastrar/erro/57/' . $paciente->get_id_paciente() );
                            return;
                        }
                    }
                }
                    
                $nova_consulta = new Consulta();
                $nova_consulta->set_usuario( $form_info['consulta_novo_usuario'] );
                $nova_consulta->set_paciente( $paciente->get_id_paciente() );
                $nova_consulta->set_data( $data->format('Y-m-d') );
                $nova_consulta->set_hora_inicio( $horario[0] );
                $nova_consulta->set_hora_fim( $horario[1] );

                if( $nova_consulta->cadastrar() ) {
                    redirect('consultas/index/sucesso/58' );
                } else {
                    redirect('consultas/index/erro/59' );
                }

            }
        }     
    }

    public function consultar($id_consulta = null, $msg = null, $codigo = null)
    {   
        $consulta = $this->consulta->consultar($id_consulta);
        if(!empty($id_consulta) && $id_consulta !== null){

            if( session_visao([3]) && current($consulta)[0]->get_usuario()->get_id_usuario() != $this->session->usuario['id_usuario'] ) {
                redirect('inicio/index/erro/51');
                return;
            }

            $data['consulta_data'] = ( !empty($consulta) ) ? array_keys($consulta)[0] : $consulta;
            $data['consulta'] = ( !empty($consulta) ) ? array_shift($consulta)[0] : $consulta;

            $data['msg'] = $msg;
            $data['codigo'] = $codigo;

            $this->load->view('consulta-consultar-unico', $data);
        } else {
            show_404();
        }         
    }

    public function remover($id_consulta = null)
    {
        $consulta = $this->consulta->consultar($id_consulta);
        if( !empty($consulta) && $id_consulta !== null ) {
            
            $data_atual = new DateTime();
            $data_atual->setTime(0,0,0);
            try{
                $data_consulta = new DateTime(array_keys($consulta)[0]);
                
                if( $data_consulta < $data_atual) {
                    redirect('consultas/consultar/' . $id_consulta . '/erro/64');
                } else {
                    if( $this->consulta->remover($id_consulta) ) {
                        redirect('consultas/index/sucesso/62' );
                    } else {
                        redirect('consultas/index/erro/63' );
                    }
                }
                
            } catch(Exception $e) {
                log_message('error', 'Metodo [remover] do controller [Consultas] Não foi possivel remover a consulta id: ' . $id_consulta );
            }
        } else {
            show_404();
        }
    }
    
    public function editar($id_consulta = null, $msg = null, $codigo = null) 
    {
        $consulta = $this->consulta->consultar($id_consulta);
        if( !empty($consulta) && $id_consulta !== null ) {
            
            $consulta_data = new DateTime(array_keys($consulta)[0]);
            $consulta_data->setTime(0,0,0);
            
            // consulta data y-m-d
            $data['consulta_data'] = $consulta_data->format('Y-m-d');

            // consulta obj
            $data['consulta'] = current($consulta)[0];

            // usuario obj -> supervisores e usuários vinculados a este paciente
            $supervisores = $this->usuario->consultar(null, null, null, [2]);
            $usuarios_vinculados = $this->prontuario->consultarUsuariosPorPaciente( $data['consulta']->get_paciente(), true);
            $data['usuarios'] =  array_merge($supervisores, $usuarios_vinculados);

            $dia_hoje = new DateTime();
            $dia_hoje->setTime(0,0,0);

            // dia hoje d/m/y
            $data['dia_hoje'] = $dia_hoje->format('d/m/Y');

            // verificar se consulta ja passou
            $data['consulta_passada'] = ( $consulta_data < $dia_hoje ) ? true : false;

            // se a consulta ja passou estagiario não podera editar a consulta, somente supervisor e admin
            if( !session_visao([1,2]) && $data['consulta_passada'] ) {
                redirect('consultas/consultar/' . $id_consulta . '/erro/66');
                return;
            }

            // pegar os horarios cadastrados
            $data['horarios'] = $this->consulta->pegarHorarios();
            
            $data['msg'] = $msg;
            $data['codigo'] = $codigo;
            
            $this->load->view( 'consulta-editar', $data );

        } else {
            show_404();
        }
    }

    public function editar_form($id_consulta = null) 
    {
        $consulta = $this->consulta->consultar($id_consulta);
        if( !empty($id_consulta) && !empty($consulta) ) {

            $form_info = $this->input->post();

            $data_consulta_salva = new DateTime( current($consulta)[0]->get_data() );
            $data_consulta_salva->setTime(0,0,0);

            $data_atual = new DateTime();
            $data_atual->setTime(0,0,0);

            $editar_consulta = new Consulta();                     
            $editar_consulta->set_id_consulta($id_consulta);

            // se a consulta ja passou estagiario não podera editar a consulta, somente supervisor e admin
            if( !session_visao([1,2]) && $data_consulta_salva < $data_atual ) {
                redirect('consultas/consultar/' . $id_consulta . '/erro/66');
                return;
            }

            // se a consulta salva é de hoje ou para uma data futura deixar editar
            if( $data_consulta_salva >= $data_atual ) {

                if( session_visao([1,2,4]) ) {

                    if( $form_info['consulta_editar_data']     === '' ||
                        $form_info['consulta_editar_horario']  === '' ||
                        $form_info['consulta_editar_usuario']  === '' ) {
                        
                        redirect('consultas/editar/' . $id_consulta . '/erro/65' );
                        return;
                    }

                    $usuario        = $this->usuario->consultar($form_info['consulta_editar_usuario']);
                    $paciente       = current($consulta)[0]->get_paciente();
                    $data_editada   = new DateTime( str_replace('/', '-', $form_info['consulta_editar_data']) ); 
                
                    if( empty($usuario) ) {
                        show_404();
                        return;
                    }

                    $horario = $this->consulta->pegarHorarios($form_info['consulta_editar_horario']);
                    $horario = array_shift($horario);

                    // verificar se existe alguma consulta na data/hora que foi marcada para o mesmo paciente, se existir somente uma, então é essa que estamos editando
                    $verificar_consulta = $this->consulta->consultar(null, null, [$data_editada, $horario[0], $horario[1], $paciente->get_id_paciente()]);
                    $verificar_consulta = current($verificar_consulta);

                    // se vazio significa que não tem uma consulta marcada para essa data e hora, se tiver um valor significa que estamos editando a mesma consulta nessa data e hora.
                    if( empty($verificar_consulta) || ($verificar_consulta[0]->get_id_consulta() == $id_consulta) ) {

                        $editar_consulta->set_usuario( $form_info['consulta_editar_usuario'] );
                        $editar_consulta->set_paciente( $paciente->get_id_paciente() );
                        $editar_consulta->set_data( $data_editada->format('Y-m-d') );
                        $editar_consulta->set_hora_inicio( $horario[0] );
                        $editar_consulta->set_hora_fim( $horario[1] );

                    } else {
                        redirect('consultas/editar/'.$id_consulta.'/erro/57');
                        return;
                    }
                }

            }

            // admin, supervisor
            if( session_visao([1,2]) ) {
                $editar_consulta->set_comparecimento_paciente( $form_info['consulta_editar_paciente_compareceu'] );
                $editar_consulta->set_justi_paciente( ( $form_info['consulta_editar_paciente_compareceu'] == '0' ) ? $form_info['consulta_editar_justificacao_paciente'] : '' );
                $editar_consulta->set_comparecimento_estagiario( $form_info['consulta_editar_usuario_compareceu'] );
                $editar_consulta->set_justi_estagiario( ( $form_info['consulta_editar_usuario_compareceu'] == '0' ) ? $form_info['consulta_editar_justificacao_usuario'] : '' );
                $editar_consulta->set_observacao_paciente( $form_info['consulta_editar_obs_paciente'] );
                $editar_consulta->set_observacao_estagiario( $form_info['consulta_editar_user_paciente'] );
            }

            // estagiario
            if( session_visao([3]) ) {
                $editar_consulta->set_comparecimento_paciente( $form_info['consulta_editar_paciente_compareceu'] );
                $editar_consulta->set_justi_paciente( ( $form_info['consulta_editar_paciente_compareceu'] == '0' ) ? $form_info['consulta_editar_justificacao_paciente'] : '' );
                $editar_consulta->set_observacao_paciente( $form_info['consulta_editar_obs_paciente'] );
            }

            if( $editar_consulta->editar() ) {
                redirect('consultas/editar/'.$id_consulta.'/sucesso/67' );
            } else {
                redirect('consultas/editar/'.$id_consulta.'/erro/68' );
            }

        } else {
            show_404();
        }
    }

}