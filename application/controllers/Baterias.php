<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Baterias extends CI_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index($msg = null, $codigo = null)
    {
        $data['baterias'] = $this->bateria->consultar(null, true);
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;
        
        $this->load->view('bateria-consultar', $data);
    }

    public function cadastrar($msg = null, $codigo = null)
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;

        $this->load->view('bateria-cadastrar', $data);
    }

    public function cadastrar_form() 
    {
        if( $this->input->post('criar_nome_bateria') !== '' && 
            $this->input->post('criar_data_inicio') !== '' &&
            $this->input->post('criar_data_fim') !== '' ) {

            try{
                
                
                $data_inicio = (new DateTime( str_replace('/', '-', $this->input->post('criar_data_inicio')) ))->format('Y-m-d');
                $data_fim    = (new DateTime( str_replace('/', '-', $this->input->post('criar_data_fim')) ))->format('Y-m-d');

                if( $data_fim <= $data_inicio ) {
                    redirect('baterias/cadastrar/erro/86');
                    return;
                }

                $this->bateria->set_nome_bateria( $this->input->post('criar_nome_bateria') );
                $this->bateria->set_data_inicio( $data_inicio );
                $this->bateria->set_data_fim( $data_fim );

                $resultado = $this->bateria->cadastrar();

                if( $resultado == true ) {
                    redirect('baterias/index/sucesso/21' );
                } else {
                    redirect('baterias/index/erro/22' );
                }

            } catch (Exception $e) {
                redirect('baterias/cadastrar/erro/71');
            }
            
        } else {
            redirect('baterias/cadastrar/erro/70');
        }
    }

    public function consultar($id_bateria = null)
    {
        $bateria = $this->bateria->consultar( $id_bateria );

        if( !empty($bateria)  ){
            $data['bateria'] = $bateria;
            $data['usuarios'] = $this->usuario->consultarUsuarioPorBateria( $id_bateria );
            $this->load->view('bateria-consultar-unico', $data);
        } else {
            show_404();
        }
    }

    public function editar($id_bateria = null, $msg = null, $codigo = null) 
    {
        $bateria = $this->bateria->consultar( $id_bateria );

        if( !empty($bateria) ) {

            if($bateria->get_bloqueado() == true) {
                redirect('baterias/index/erro/47');
                return;
            }
            
            $data['msg'] = $msg;
            $data['codigo'] = $codigo;

            $data['bateria'] = $bateria;
            
            $this->load->view( 'bateria-editar', $data );
        } else {
            show_404();
        }
    }

    public function editar_form($id_bateria = null) 
    {
        if( $this->input->post('editar_nome_bateria') !== '' && $this->input->post('editar_nome_bateria') !== null &&
            $this->input->post('editar_data_inicio') !== '' && $this->input->post('editar_data_inicio') !== null &&
            $this->input->post('editar_data_fim') !== '' && $this->input->post('editar_data_fim') !== null &
            $id_bateria !== null ) {

            try{

                $data_inicio = (new DateTime( str_replace('/', '-', $this->input->post('editar_data_inicio')) ))->format('Y-m-d');
                $data_fim    = (new DateTime( str_replace('/', '-', $this->input->post('editar_data_fim')) ))->format('Y-m-d');

                $bateria = $this->bateria->consultar( $id_bateria );

                $bateria->set_nome_bateria( $this->input->post('editar_nome_bateria') );
                $bateria->set_data_inicio( $data_inicio );
                $bateria->set_data_fim( $data_fim );
                
                $resultado = $bateria->editar();
                
                if( $resultado == true ) {
                    redirect('baterias/editar/' . $id_bateria . '/sucesso/19' );
                } else {
                    redirect('baterias/editar/' . $id_bateria . '/erro/20' );
                }

            } catch (Exception $e) {
                redirect('baterias/cadastrar/erro/71');
            }
            
        } else {
            redirect('baterias/cadastrar/erro/70');
        }
        
    }
    
    public function remover($id_bateria)
    {
        $bateria = $this->bateria->consultar( $id_bateria );
        $resultado = $bateria->remover();

        if( $resultado == true ) {
            redirect('baterias/index/sucesso/23' );
        } else {
            redirect('baterias/index/erro/24' );
        }
    }

}