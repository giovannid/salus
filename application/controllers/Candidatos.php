<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidatos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index($msg = null, $codigo = null, $filtrar = null, $por = null)
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;

        if( $filtrar == 'filtrar' && $por == 'nome' ) {
            $sqlFiltrarPor = 'nome_completo';
            $sqlFiltrarOrdem = 'ASC';
            $data['por_nome'] = true;

        } else if( $filtrar == 'filtrar' && $por == 'idade' ) {
            $sqlFiltrarPor = 'data_nascimento';
            $sqlFiltrarOrdem = 'DESC';
            $data['por_idade'] = true;
        } else {
            $sqlFiltrarPor = null;
            $sqlFiltrarOrdem = 'ASC';
        }
        
        $data['candidatos'] = $this->paciente->consultar(null, true, $sqlFiltrarPor, $sqlFiltrarOrdem);
        $this->load->view('candidato-consultar', $data); 
    }

    public function consultar($id_paciente = null) 
    {
        $candidato = $this->paciente->consultar($id_paciente);

        if( !empty($candidato) && $candidato->get_candidato() == true ) {
            $data['candidato'] = $candidato;
            $this->load->view('candidato-consultar-unico', $data);
        } else {
            show_404();
        }
    }

    public function buscar($info = null){

        if( !empty( $this->input->post('info') ) ){
            redirect('candidatos/buscar/'. $this->input->post('info'));
        }

        $buscar_por = urldecode($info);

        $data['candidatos'] = $this->paciente->buscar($buscar_por, true);
        $data['buscando_por'] = $buscar_por;

        $this->load->view( 'candidato-consultar', $data );
    }

    public function cadastrar()
    {
        $data['candidato'] = new Paciente();
        $this->load->view('candidato-cadastrar', $data);
    }

    public function cadastrar_form()
    {
        $id_paciente = $this->paciente->cadastrar();
        if( $id_paciente !== false  ) {
            redirect('candidatos/index/sucesso/45');
        } else {
            redirect('candidatos/index/erro/46');
        }
    }

    public function editar($id_paciente = null, $msg = null, $codigo = null) 
    {
        $candidato = $this->paciente->consultar($id_paciente);

        if( !empty($candidato) && $candidato->get_candidato() == true ) {
            $data['candidato'] = $candidato;
            $data['msg'] = $msg;
            $data['codigo'] = $codigo;

            $this->load->view('candidato-editar', $data);
        } else {
            show_404();
        }
    }

    public function editar_form($id_paciente = null)
    {	
        $candidato = $this->paciente->consultar($id_paciente);
    
        if( !empty($candidato) && $candidato->get_candidato() == true ) {
            
            $candidato_ob = new Paciente();
            $candidato_ob->set_id_paciente( $id_paciente );
            
            if( $candidato_ob->editar() ) {
                redirect('candidatos/editar/'. $id_paciente .'/sucesso/35');
            } else {
                redirect('candidatos/editar/'. $id_paciente .'/erro/36');
            }


        } else {
            show_404();
        }
    }

    public function remover($id_paciente = null)
    {
        $candidato = $this->paciente->consultar($id_paciente);
    
        if( !empty($candidato) && $candidato->get_candidato() == true ) {
            
            $candidato_ob = new Paciente();
            $candidato_ob->set_id_paciente( $id_paciente );
            
            if( $candidato_ob->desabilitar() ) {
                redirect('candidatos/index/sucesso/37');
            } else {
                redirect('candidatos/index/erro/38');
            }


        } else {
            show_404();
        }
    }

    public function mover_para_paciente($id_paciente = null)
    {
        $candidato = $this->paciente->consultar($id_paciente);
    
        if( !empty($candidato) && $candidato->get_candidato() == true ) {
            
            if( $candidato->moverParaCandidato(false) ) {
                redirect('candidatos/index/sucesso/39');
            } else {
                redirect('candidatos/index/erro/40');
            }
        
        } else {
            show_404();
        }
    }

}
