<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index( $msg = null, $codigo = null )
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;
        $data['usuarios'] = array();

        $usuario = $this->usuario->consultar();
        foreach($usuario as $valor) {
            $prontuarios = $this->prontuario->consultar(null, null, $valor);
            $prontuarios_livres = 0;
            foreach($prontuarios as $key => $prontuario){
                $prontuarios_livres = ($prontuario->get_bloqueado() != true) ? $prontuarios_livres + 1 : $prontuarios_livres;
            }
            if ( $prontuarios_livres > 0 ) {
                $data['usuarios'][] = array( 'info' => $valor, 'prontuario' => true );
            } else {
                $data['usuarios'][] = array( 'info' => $valor, 'prontuario' => false );
            }
        }
        $this->load->view('usuario-consultar', $data);
    }

    public function buscar($info = null){
        
        $form_info = $this->input->post();

        if( isset($form_info['info']) ){

            if( empty($form_info['info']) ) redirect('usuarios/index');
            redirect('usuarios/buscar/'. urlencode($form_info['info']) );
        } 

        $buscar_por = urldecode($info);

        $data['usuarios'] = array();
        $usuario = $this->usuario->buscar($buscar_por);
       
        if( !empty($usuario) ) {
            foreach($usuario as $valor) {
                $prontuarios = $this->prontuario->consultar(null, null, $valor);
                $prontuarios_livres = 0;
                foreach($prontuarios as $key => $prontuario){
                    $prontuarios_livres = ($prontuario->get_bloqueado() != true) ? $prontuarios_livres + 1 : $prontuarios_livres;
                }
                if ( $prontuarios_livres > 0 ) {
                    $data['usuarios'][] = array( 'info' => $valor, 'prontuario' => true );
                } else {
                    $data['usuarios'][] = array( 'info' => $valor, 'prontuario' => false );
                }
            }
        }
        $data['buscando_por'] = $buscar_por;
        $this->load->view( 'usuario-consultar', $data );
    }

    public function cadastrar()
    {
        $baterias = $this->bateria->consultar();
        $data['baterias'] = array();
        foreach($baterias as $key => $bateria){
            if($bateria->get_bloqueado() !== true) {
                $data['baterias'][] = $bateria;
            }
        }
        $this->load->view('usuario-cadastrar', $data);
    }

    public function cadastrar_form()
    {
        $usuario = new Usuario();
        $usuario->set_nome_usuario( $this->input->post('cadastrar_nome_usuario') );
        $usuario->set_email( $this->input->post('cadastrar_email') );
        $usuario->set_senha( $this->input->post('cadastrar_senha') );
        $usuario->set_ra( $this->input->post('cadastrar_ra') );
        $usuario->set_tipo_usuario( $this->input->post('cadastrar_tipo_usuario') );
        $usuario->set_bateria( $this->input->post('cadastrar_id_bateria') );
        $usuario->set_forcar_troca_senha(true);

        if( $usuario->cadastrar() ) {
            redirect('usuarios/index/sucesso/12' );
        } else {
            redirect('usuarios/index/erro/13' );
        }
    }

    public function consultar($id_usuario = null, $msg = null, $codigo = null, $replace = null)
    {
        $usuario = $this->usuario->consultar( $id_usuario );

        if( !empty($usuario)  ) {
            $data['msg'] = $msg;
            $data['codigo'] = $codigo;
            $data['usuario'] = $usuario;
            $data['baterias'] = $this->bateria->consultar();
            $data['pacientes'] = $this->paciente->consultar();
            $data['faltas'] = $this->consulta->pegarFaltas($id_usuario);
            $data['prontuarios'] = array();
            $data['replace'] = $replace;
            $prontuarios = $this->prontuario->consultar(null, null, $usuario);
            foreach($prontuarios as $key => $prontuario){
                if( $prontuario->get_bloqueado() != true ) {
                    $data['prontuarios'][] = $prontuario;
                }
            }

            $this->load->view('usuario-consultar-unico', $data);
        } else {
            show_404();
        }
    }

    public function editar( $id_usuario = null, $msg = null, $codigo = null )
    {
        $usuario = $this->usuario->consultar( $id_usuario );
        if( !empty($usuario) ) {

            $data['msg'] = $msg;
            $data['codigo'] = $codigo;
            $data['usuario'] = $usuario;

            $this->load->view( 'usuario-editar', $data );

        } else {
            show_404();
        }
    }

    public function editar_form($id_usuario = null) 
    {
        $usuario = $this->usuario->consultar( $id_usuario );
        
        if( !empty($usuario) ) {
            $usuario->set_nome_usuario( $this->input->post('editar_nome_usuario') );
            $usuario->set_email( $this->input->post('editar_email') );
            $usuario->set_ra( $this->input->post('editar_ra') );
            $usuario->set_tipo_usuario( $this->input->post('editar_tipo_usuario') );

            if( $usuario->editar() ) {
                redirect('usuarios/editar/' . $id_usuario . '/sucesso/8' );
            } else {
                redirect('usuarios/editar/' . $id_usuario . '/erro/9' );
            }
        } else {
            show_404();
        }
    }

    public function remover( $id_usuario = null ) 
    {
        $usuario = $this->usuario->consultar( $id_usuario );

        if( !empty($usuario) ) {

            if( $usuario->remover() ) {

                $prontuarios = $this->prontuario->consultar(null, null, $usuario);

                foreach($prontuarios as $index => $prontuario) {
                    $this->prontuario->bloquear( $prontuario->get_id_prontuario() );
                }

                redirect('usuarios/index/sucesso/10' );
            } else {
                redirect('usuarios/index/erro/11' );
            }
            
        } else {
            show_404();
        }
    }

    public function trocar_bateria($id_usuario = null)
    {	
        $usuario = $this->usuario->consultar($id_usuario);
        if( !empty($usuario) && $usuario->get_desabilitado() !== true ) {
            
            $usuario->set_bateria( $this->input->post('editar_id_bateria') );

            if( $usuario->trocarBateria() ) {
                redirect('usuarios/consultar/'. $id_usuario  .'/sucesso/26#trocar-bateria');
            } else {
                redirect('usuarios/consultar/'. $id_usuario  .'/erro/27#trocar-bateria');
            }

        } else {
            show_404();
        }
    }

    public function trocar_paciente($id_usuario = null)
    {	
        $usuario = $this->usuario->consultar($id_usuario);
        if( !empty($usuario) && $usuario->get_desabilitado() !== true && $usuario->get_tipo_usuario() != 0 && $usuario->get_tipo_usuario() != 4 ) {

            $pacientes_atuais = $this->prontuario->consultarPacientesPorUsuario($usuario);
            $pacientes_sel = $this->paciente->checarSeExiste( $this->input->post('pacientes') );

            // salvar sem modificar, nao importa a ordem no array
            if( $pacientes_atuais == $pacientes_sel  ) {
                redirect('/usuarios/consultar/' . $id_usuario . '/sucesso/28#vincular-paciente');
            } else {

                if( !empty($pacientes_sel) ) {
                    
                    // remover os pacientes atuais que não está na lista de pacientes novos
                    foreach($pacientes_atuais as $key => $id_paciente) {
                        if( in_array($id_paciente, $pacientes_sel) === false ) {
                            if( !$this->prontuario->bloquearProntuarioPorPacienteEUsuario( $id_paciente , $usuario->get_id_usuario() ) ) {
                                throw new Exception('Não foi possivel tirar esse paciente desse usuário! Contate um administrador.');
                            }
                        }
                    }

                    // adicionar os pacientes novos que não esá na lista de pacientes atuais
                    foreach($pacientes_sel as $key => $id_paciente) {
                        $paciente = $this->paciente->consultar($id_paciente);

                        if( empty($paciente) ) {
                            redirect('usuarios/consultar/' . $id_usuario . '/erro/80#vincular-paciente' );
                            return;
                        }

                        if( in_array($id_paciente, $pacientes_atuais) === false ) {

                            // verificar se ja existe um estagiario vinculado a este paciente, se tiver, não adicionar.
                            // ^ com excecao dos supervisores
                            $verificar_paciente = $this->prontuario->consultarUsuariosPorPaciente($paciente, true);
                            
                            if(!empty($verificar_paciente) && $verificar_paciente[0]->get_id_usuario() != $usuario->get_id_usuario() && $usuario->get_tipo_usuario() != 2 ) {
                                redirect('usuarios/consultar/' . $id_usuario . '/erro/83/'. rawurlencode(ucwords($paciente->get_nome_completo())) .'#vincular-paciente' );
                                return;
                            }
                            
                            $pront_ob = new Prontuario();
                            $pront_ob->set_data_criacao();
                            $pront_ob->set_data_atualizacao();
                            $pront_ob->set_usuario( $usuario->get_id_usuario() );
                            $pront_ob->set_paciente( $paciente->get_id_paciente() );
                            $pront_ob->set_bloqueado( false );
                            $pront_ob->cadastrar();
                        }

                    }
                    redirect('usuarios/consultar/' . $id_usuario . '/sucesso/28#vincular-paciente');
                
                } else {
                    
                    // usuario tem pacientes mas removeu todos
                    foreach($pacientes_atuais as $key => $id_paciente) {
                        if( !$this->prontuario->bloquearProntuarioPorPacienteEUsuario( $id_paciente , $usuario->get_id_usuario() ) ) {
                            throw new Exception('Não foi possivel desvincular esse paciente desse usuário! Contate um administrador.');
                        }
                    }
                    redirect('/usuarios/consultar/' . $id_usuario . '/sucesso/28#vincular-paciente');
                }

            }

        } else {
            show_404();
        }
    }

    public function trocar_senha($id_usuario = null)
    {
        $usuario = $this->usuario->consultar($id_usuario);
        if( $id_usuario !== null && !empty($usuario) ) {

            $data['usuario'] = $usuario;
            $this->load->view('usuario-trocar-senha', $data);

        } else {
            show_404();
        }
    }

    public function trocar_senha_form($id_usuario = null) 
    {
        $usuario = $this->usuario->consultar($id_usuario);
        $senha_provisoria = $this->input->post('senha-provisoria');
        if( $id_usuario !== null && !empty($usuario) && !empty( $senha_provisoria ) ) {

            $usuario->set_senha($senha_provisoria);
            $usuario->set_forcar_troca_senha(true);

            if( $usuario->trocarSenha() ) {
                redirect('usuarios/consultar/' . $usuario->get_id_usuario() . '/sucesso/84' );
            } else {
                redirect('usuarios/consultar/' . $usuario->get_id_usuario() . '/erro/85' );
            }

        } else {
            show_404();
        }
    }
}
