<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index($msg = null, $codigo = null)
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;
        $this->load->view('login-index', $data);
    }

    public function entrar()
    {

        $usuario = Usuario::autenticar( $this->input->post('login-email'), $this->input->post('login-senha') );
        
        if( $usuario != false ) {
            $this->session->usuario = array(
                'id_usuario' => $usuario->get_id_usuario(),
                'nome_usuario' => $usuario->get_nome_usuario(),
                'email' => $usuario->get_email(),
                'tipo_usuario' => $usuario->get_tipo_usuario(),
                'id_bateria' => ( $usuario->get_bateria() ) ? $usuario->get_bateria()->get_id_bateria() : 0 ,
                'forcar_troca_senha' => $usuario->get_forcar_troca_senha()
            );
            redirect('inicio');
        } else {
            redirect('login/index/erro/1');
        }

    }

    public function trocar_senha($msg = null, $codigo = null)
    {
        if( $this->session->usuario !== null) {
            $data['msg'] = $msg;
            $data['codigo'] = $codigo;
            $this->load->view('login-trocar-senha', $data);
        } else {
            show_404();
        }
    }

    public function trocar_senha_form()
    {

        $senha_atual = $this->input->post('senha-atual');
        $nova_senha = $this->input->post('nova-senha');
        $nova_senha_novamente = $this->input->post('nova-senha-novamente');

        if( Usuario::autenticar( $this->session->usuario['email'], $senha_atual ) ) {

            if( $nova_senha == $nova_senha_novamente ) {

                $usuario_obj = new Usuario();
                $usuario_obj->set_id_usuario( $this->session->usuario['id_usuario'] );
                $usuario_obj->set_forcar_troca_senha(false);
                $usuario_obj->set_senha($nova_senha);
                
                $resultado = $usuario_obj->trocarSenha();

                if( $resultado == true ){
                    $this->session->sess_destroy();
                    redirect('login/index/sucesso/7');
                } else {
                    redirect('login/trocar_senha/erro/6');
                }

            } else {
                redirect('login/trocar_senha/erro/5');
            }

        } else {
            redirect('login/trocar_senha/erro/4');
        }

    }

    public function sair()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}
