<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index( $msg = null, $codigo = null, $todas = null )
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;

        $dia_atual = new DateTime();
        $dia_atual->setTime(0,0,0);
        $data['dia_atual'] = $dia_atual;

        $dia_seguinte = new DateTime("+1 day");
        $dia_seguinte->setTime(0,0,0);
        $data['dia_seguinte'] = $dia_seguinte;
        $data['todas_consultas'] = false;


        // admin, supervisor, teclab conseguiram ver *todas* as consultas de hoje, estagiarios somente *as deles* 
        if( session_visao([1,2,4]) ) {
            $consultas = $this->consulta->consultar(null, $dia_atual, null, null, null);
        } else {
            $consultas = $this->consulta->consultar(null, $dia_atual, null, null, $this->session->usuario['id_usuario']);
        }

        if( $todas == 'todas' && session_visao([3]) ) {
            $data['todas_consultas'] = true;
            $data['consultas'] = $this->consulta->consultar(null, null, null, null, $this->session->usuario['id_usuario']);
        } else {
            $data['consultas'] = $consultas;
        }

        $usuario = $this->usuario->consultar( $this->session->usuario['id_usuario'] );
        $data['prontuarios'] = $this->prontuario->consultar(null, null, $usuario, null, true);
        
        $data['bateria'] = ( $this->session->usuario['id_bateria'] == 0) ? null : $usuario->get_bateria();
        
        $this->load->view('inicio-index', $data);
    }

}