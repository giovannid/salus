<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends CI_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index($msg = null, $codigo = null)
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;
        $data['horarios'] = $this->consultar_horarios();
        $data['campos'] = $this->prontCamposDeOpcoes()['prontuario'];

        $this->load->view('configuracoes-index', $data);
    }

    public function horario_permitidos()
    {
        $info = $this->input->post();

        if( !empty($info) ) {

            // verificar se existe algum horario para ser removido
            if( isset( $info['horario_remover'] ) ) {
                $remover = $info['horario_remover'];
                unset($info['horario_remover']);

                foreach($remover as $index => $rm) {
                    if( !empty( $this->db->get_where('opcoes', array('campo' => 'horario', 'id_opcoes' => $rm) )->result_array() ) ){
                        $this->db->where('id_opcoes', $rm);
                        $this->db->delete('opcoes');
                    }
                }
            }

            // se editou ou adicionou novo horario
            foreach($info as $index => $horario){

                // se atualizou campos
                if( count($horario) === 3 ) {
                    if( isset($horario[2]) && !empty( $this->consultar_horarios( $horario[2] ) ) ){
                        try{
                            $horario_inicio = new DateTime( $horario[0] );
                            $horario_fim = new DateTime( $horario[1] );

                            if($horario_fim <= $horario_inicio) {
                                redirect('configuracoes/index/erro/73');
                                return;
                            }
                            $valor_campo = array( 
                                0 => $horario_inicio->format('H:i'), 
                                1 => $horario_fim->format('H:i')  
                            );

                            $valor_campo = json_encode( $valor_campo );

                            // atualizar campo
                            $this->db->set('opcao', $valor_campo);
                            $this->db->where( 'id_opcoes', $horario[2] );
                            $this->db->update('opcoes');
                        } catch(Exception $e) {
                            redirect('configuracoes/index/erro/72');
                            return;
                        }
                    }
                // se adicionou campos
                } else if( count($horario) === 2 ) {
                    try{
                        $horario_inicio = new DateTime( $horario[0] );
                        $horario_fim = new DateTime( $horario[1] );

                        if($horario_fim <= $horario_inicio) {
                            redirect('configuracoes/index/erro/73');
                            return;
                        }
                        $valor_campo = array( 
                            0 => $horario_inicio->format('H:i'), 
                            1 => $horario_fim->format('H:i')  
                        );

                        $valor_campo = json_encode( $valor_campo );

                        // criar campo
                        $data = array(
                            'campo' => 'horario',
                            'opcao' => $valor_campo
                        );
                        $this->db->insert('opcoes', $data);

                    } catch(Exception $e) {
                        redirect('configuracoes/index/erro/72');
                        return;
                    }
                }
            }

            redirect('configuracoes/index/sucesso/54');
            
            
        } else {
            show_404();
        }
    }

    public function opcoes_prontuarios()
    {
        $error = false;
        
        // campos enviados pela pagina de configuracoes
        $campos_enviados = $this->input->post();

        if( !empty($campos_enviados) ) {

            $insert_this = array();
            $update_this = array();
            $delete_this = array();

            foreach($campos_enviados as $campo => $opcoes) {

                $campos_cadastrados = [];
                foreach($this->db->select('id_opcoes')->get_where( 'opcoes', array('campo' => $campo) )->result_array() as $index => $id_opcao) {
                    $campos_cadastrados[] = $id_opcao['id_opcoes'];
                }

                if( is_array($opcoes) ) {
                    foreach($opcoes as $index => $info) {
                        /**
                        * Se algum campo não for enviado significa que ele foi removido, pegamos todos as opcoes
                        * cadastradas para esse campo e removemos as que foram enviadas. No final teremos os ids
                        * das opcoes que foram removidas para esse campo.
                        */
                        if( isset($campos_cadastrados) && !empty($campos_cadastrados) && isset($info['update']) ) {
                            $procurar_cadastrados = array_search( $info['update'], $campos_cadastrados );
                            unset($campos_cadastrados[$procurar_cadastrados]);
                        }
                        /**
                        * Se o campo ja estiver cadastrado, a informacao vai conter uma chave 'update' no array que guarda o ID do registro no banco.
                        * Vamos usar esse ID para pegar essa opcao cadastrada no banco e comparar se ela foi modificada, se sim, fazer update no banco
                        * se não, não precisamos fazer nada.
                        *
                        * Se a informacao nao conter uma chave 'update' vamos considerar como uma nova opcao para esse campo, iniciando o cadastro da mesma no banco.
                        */
                        if( array_key_exists('update', $info) ) {
                            $registro = $this->db->get_where('opcoes', [ 'id_opcoes' => $info['update'] ] )->row();
                            
                            $info_obs = ( isset($info['obs']) && $info['obs'] == '1' ) ? 's' : null;
                            if( !empty($registro) && $registro->opcao != trim($info['opcao']) || $registro->obs != $info_obs ) {
                                $update_this[] = array(
                                    'opcao' => trim($info['opcao']),
                                    'obs' => $info_obs,
                                    'id_opcoes' => $info['update']
                                );
                            }
                        } else {
                            if( !empty($info['opcao']) ) {
                                $insert_this[] = array(
                                    'campo' => $campo,
                                    'opcao' => trim($info['opcao']),
                                    'obs' => ( isset($info['obs']) && $info['obs'] == '1' ) ? 's' : null
                                );
                            }
                        }
                    }
                }

                foreach($campos_cadastrados as $index => $id_opcao) {
                    $delete_this[] = $id_opcao;
                }
            }

            if( !empty($insert_this) ) {
                if( $this->db->insert_batch('opcoes', $insert_this) < 1 ) {
                    log_message('error', 'Metodo [opcoes_prontuarios] Não foi possivel INSERIR novos registros na tabela opções!' . $this->db->error() );
                    $error = true;
                }
            }

            if( !empty($update_this) ) {
                if( $this->db->update_batch('opcoes', $update_this, 'id_opcoes') < 1 ) {
                    log_message('error', 'Metodo [opcoes_prontuarios] Não foi possivel ATUALIZAR registros na tabela opções! ' . $this->db->error() );
                    $error = true;
                }
            }

            if( !empty($delete_this) ) {
                $this->db->where_in('id_opcoes', $delete_this);
                if( !$this->db->delete('opcoes') ) {
                    log_message('error', 'Metodo [opcoes_prontuarios] Não foi possivel DELETAR registros na tabela opções! ' . $this->db->error() );
                    $error = true;
                }
            }

            if( $error !== true ) {
                redirect('configuracoes/index/sucesso/55#campos-personalizaveis');
            } else {
                redirect('configuracoes/index/sucesso/56#campos-personalizaveis');
            }

        } else {
            show_404();
        }
    }

    private function consultar_horarios( $id_horario = null)
    {
        if( $id_horario !== null ) {
            $resultado = $this->db->get_where('opcoes', array( 'id_opcoes' => $id_horario, 'campo' => 'horario' ))->result_array();
        } else {
            $saida = array();
            $resultado = $this->db->get_where('opcoes', array('campo' => 'horario'))->result_array();
            foreach($resultado as $index => $value) {
                $horarios = json_decode( $value['opcao'] );
                $saida[] = array( 'index' => $index+1, 'inicio' => $horarios[0], 'fim' => $horarios[1] , 'record' => $value['id_opcoes']);
            }
        }
        return ( $id_horario !== null ) ? $resultado : $saida;
    }

    private function prontCamposDeOpcoes()
    {
        $campos = array(
            'prontuario' => array(
                'Avaliação do Desenvolvimento Motor' => array(
                    'Visão' => 'adm_visao',
                    'Audição' => 'adm_audicao',
                    'Linguagem' => 'adm_linguagem',
                    'Cognitivo' => 'adm_cognitivo',
                    'Reflexos Primitivos' => 'adm_reflexos_prim'
                ),
                'Supino' => array(
                    'Simetria' => 'supino_simetria',
                    'Alinhamento' => 'supino_alinhamento',
                    'Movimentação Ativa' => 'supino_movimentacao_ativa'
                ),
                'Prono' => array(
                    'Controle Cervical' => 'prono_controle_cervical',
                    'Controle Escapular' => 'prono_controle_escapular',
                    'Simetria' => 'prono_simetria',
                    'Alinhamento' => 'prono_alinhamento',
                    'Movimentação Ativa' => 'prono_movimentacao_ativa'
                ),
                'Rolar' => array(
                    'Opções' => 'rolar'
                ),
                'Sentado' => array(
                    'Controle Cervical' => 'sentado_controle_cervical',
                    'Controle de Tronco' => 'sentado_controle_tronco',
                    'Simetria' => 'sentando_simetria',
                    'Alinhamento' => 'sentado_alinhamento',
                    'Movimentação Ativa' => 'sentado_movimentacao_ativa'
                ),
                'Troca Postural de Supino para Sentado' => array(
                    'Postura de Quadril' => 'sentado_postura_quadril',
                    'Deformidade da Coluna' => 'sentado_deformidade_coluna',
                    'Deformidade de Quadril' => 'sentado_deformidade_quadril'
                ),
                'Engatinhar' => array(
                    'Opções' => 'engatinhar'
                ), 
                'Arrastar' => array(
                    'Opções' => 'arrastar'
                ),
                'Ortostatismo' => array(
                    'Opções' => 'ortostatismo'
                ),
                'Marcha' => array(
                    'Opções' => 'marcha'
                ),
                'Tônus de Base' => array(
                    'Hipertonia Elástica (grupos musculares)' => 'tonus_base_hipertonia_elastica',
                    'Hipertonia Plástica' => 'tonus_base_hipertonia_plastica',
                    'Discinesias' => 'tonus_base_discinesias',
                    'Hipotonia' => 'tonus_base_hipotonia',
                    'Incoordenação de movimentos' => 'tonus_base_incordenacao_movimentos'
                ),
                'Atividades de Vida Diária' => array(
                    'Alimentação' => 'atividades_vida_diaria_alimentacao',
                    'Higiene' => 'atividades_vida_diaria_higiene',
                    'Vestuário' => 'atividades_vida_diaria_vestuario',
                    'Locomoção' => 'atividades_vida_diaria_locomocao'
                ),
            ),
            'paciente' => array(
                'Candidato/Paciente Opções' => array(
                    'ADNPM' => 'adnpm',
                    'Síndrome de Down' => 'sindrome_de_down',
                    'Paralisia Braquial Congênita' => 'paralisia_braquial',
                    'Mielomeningocele' => 'mielo',
                    'Encefalopatia Crônica infantil não Progressiva' => 'encefalopatia',
                    'Classificação Topográfica' => 'class_topografia',
                    'Nivel' => 'nivel',
                    'História da Moléstia Atual/Pregressa: Intercorrência' => 'historia_molestia'
                )
            )
        );
        return $campos;
    }

    public function relatorio()
    {
        $this->load->view('configuracoes-relatorio');
    }

    public function gerar_relatorio($opcao = null)
    {
        switch ($opcao) {
            case 'consulta':
                exportaConsulta('consultas');
                break;

            case 'bateria':
                exportaBateria('baterias');
                break;
                
            case 'paciente':
                exportaPaciente('pacientes');
                break;
            
            case 'candidato':
                exportaPaciente('candidatos', true);
                break;
            
            case 'prontuario':
                exportaProntuario('prontuarios');
                break;

            case 'estatisticas':
                exportaEstatisticas('estatisticas');
                break;
            
            default:
                show_404();
                break;
        }

    }

}