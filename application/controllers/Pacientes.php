<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pacientes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index($msg = null, $codigo = null, $filtrar = null, $por = null)
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;

        if( $filtrar == 'filtrar' && $por == 'nome' ) {
            $sqlFiltrarPor = 'nome_completo';
            $sqlFiltrarOrdem = 'ASC';
            $data['por_nome'] = true;
        } else if( $filtrar == 'filtrar' && $por == 'idade' ) {
            $sqlFiltrarPor = 'data_nascimento';
            $sqlFiltrarOrdem = 'DESC';
            $data['por_idade'] = true;
        } else {
            $sqlFiltrarPor = null;
            $sqlFiltrarOrdem = 'ASC';
        }

        $data['pacientes'] = $this->paciente->consultar(null, null, $sqlFiltrarPor, $sqlFiltrarOrdem);

        if( $filtrar == "alta" ) {
            $data['pacientes'] = $this->paciente->consultar(null, null, null, null, true);
            $data['com_alta'] = true;
        }

        $this->load->view( 'paciente-consultar', $data ); 
    }

    public function buscar($info = null){

        if( !empty( $this->input->post('info') ) ){
            redirect('pacientes/buscar/'. $this->input->post('info'));
        }

        $buscar_por = urldecode($info);

        $data['pacientes'] = $this->paciente->buscar($buscar_por, false);
        $data['buscando_por'] = $buscar_por;

        $this->load->view( 'paciente-consultar', $data );
    }

    public function remover( $id_paciente )
    {
        $paciente = $this->paciente->consultar( $id_paciente );
        
        if( $id_paciente !== null && !empty($paciente) && $paciente->get_candidato() == false ) {

            // remover todos os prontuarios
            $prontuarios = new Prontuario();
            if( !$prontuarios->removerProntuarioPorPaciente($id_paciente) ) {
                log_message('error', '[Classe] Prontuario [Metodo] remover : Não foi possivel remover todos os prontuarios do paciente id: ' . $id_paciente);
            }

            // remover todos as consultas
            $consultas = new Consulta();
            if( !$consultas->removerConsultaPorPaciente($id_paciente) ) {
                log_message('error', '[Classe] Prontuario [Metodo] remover : Não foi possivel remover todas as consultas do paciente id: ' . $id_paciente);
            }

            // remover paciente
            $paciente_ob = new Paciente();
            $paciente_ob->set_id_paciente( $id_paciente );
            if( $paciente_ob->remover() ) {
                redirect('pacientes/index/sucesso/52');
            } else {
                redirect('pacientes/consultar/'. $id_paciente .'/erro/53');
            }

        } else {
            show_404();
        }
    }

    public function consultar( $id_paciente = null, $msg = null, $codigo = null ) 
    {
        $paciente = $this->paciente->consultar( $id_paciente );

        if( $id_paciente !== null && !empty($paciente) && $paciente->get_candidato() == false ) {

            $data['paciente'] = $paciente;
            
            if( session_visao([3]) ) {
                $consulta = $this->consulta->consultar(null, null, null, $id_paciente, $this->session->usuario['id_usuario']);
            } else {
                $consulta = $this->consulta->consultar(null, null, null, $id_paciente);
            }
            
            $data['consultas']  = $consulta;
            
            $dia_atual = new DateTime();
            $dia_atual->setTime(0,0,0);
            $data['dia_atual'] = $dia_atual;

            $data['msg']       = $msg;
            $data['codigo']    = $codigo;
            $data['faltas']    = $this->consulta->pegarFaltas(null, $id_paciente);
            
            if( session_visao([1, 2]) ) {
                $data['prontuarios'] = $this->prontuario->consultar( null, $paciente, null, null, true );
            } else {
                $usuario = $this->usuario->consultar( $this->session->usuario['id_usuario'] );
                $data['prontuarios'] = $this->prontuario->consultar( null, $paciente, $usuario, 'tudo' );
            }

            $this->load->view( 'paciente-consultar-unico', $data );

        } else {
            show_404();
        }
    }

    public function editar($id_paciente = null, $msg = null, $codigo = null) 
    {
        $paciente = $this->paciente->consultar($id_paciente);

        if( $id_paciente !== null && !empty($paciente) && $paciente->get_candidato() == false ) {

            if( $paciente->get_desabilitado() == true ) {
                redirect('pacientes/consultar/' . $id_paciente . '/erro/82' );
                return;
            }

            $data['paciente'] = $paciente;
            $data['msg'] = $msg;
            $data['codigo'] = $codigo;

            $this->load->view('paciente-editar', $data);
        } else {
            show_404();
        }
    }

    public function editar_form($id_paciente = null)
    {	
        $paciente = $this->paciente->consultar($id_paciente);
    
        if( $id_paciente !== null && !empty($paciente) && $paciente->get_candidato() == false ) {

            if( $paciente->get_desabilitado() == true ) {
                redirect('pacientes/consultar/' . $id_paciente . '/erro/82' );
                return;
            }
            
            $paciente_ob = new Paciente();
            $paciente_ob->set_id_paciente( $id_paciente );
            
            if( $paciente_ob->editar() ) {
                redirect('pacientes/editar/'. $id_paciente .'/sucesso/43');
            } else {
                redirect('pacientes/editar/'. $id_paciente .'/erro/44');
            }

        } else {
            show_404();
        }
    }

    public function mover_para_candidato($id_paciente = null)
    {
        $paciente = $this->paciente->consultar( $id_paciente );
        if( $id_paciente !== null && !empty($paciente) && $paciente->get_candidato() == false ) {

            if( $paciente->moverParaCandidato(true) ) {

                // bloquear todos os prontuarios desse paciente
                $prontuarios = $this->prontuario->consultar( null, $paciente );
                foreach($prontuarios as $index => $prontuario) {
                    $this->prontuario->bloquear( $prontuario->get_id_prontuario() );
                }

                redirect('pacientes/index/sucesso/41');
            } else {
                redirect('pacientes/index/erro/42');
            }

        } else {
            show_404();
        }
    }

    public function dar_alta($id_paciente = null)
    {

        $paciente = $this->paciente->consultar($id_paciente);

        if( $id_paciente !== null && !empty($paciente) && !empty( $this->input->post('motivo_alta') ) ) {
            
            if( $paciente->get_desabilitado() != true ) {
                // bloquear todos os prontuarios desse paciente
                $prontuario = new Prontuario();
                if( !$prontuario->bloquearProntuarioPorPaciente($id_paciente) ) {
                    log_message('error', '[Metodo] dar_alta : Não foi possivel bloquear os prontuarios do paciente id: ' . $id_paciente );
                }

                // desabilitar o paciente            
                $paciente = new Paciente();
                $paciente->set_id_paciente($id_paciente);
                $motivo = $this->input->post('motivo_alta');
                if( $paciente->desabilitar($motivo) ) {
                    redirect('pacientes/index/sucesso/74');
                } else {
                    redirect('pacientes/index/erro/75');
                }
            } else {
                redirect('pacientes/index/sucesso/76/alta');
            }
        } else {
            redirect('pacientes/index/erro/75');
        }
    }
}
