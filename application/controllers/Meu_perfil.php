<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Meu_Perfil extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $data['usuario'] = $this->usuario->consultar( $this->session->usuario['id_usuario'] );
        $this->load->view('meu-perfil-index', $data);
    }

    public function editar( $msg = null, $codigo = null )
    {
        $data['usuario'] = $this->usuario->consultar( $this->session->usuario['id_usuario'] );
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;
        $this->load->view('meu-perfil-editar', $data);
    }

    public function editar_form()
    {
        $usuario_obj = new Usuario();
        $usuario_obj->set_id_usuario( $this->session->usuario['id_usuario'] );
        $usuario_obj->set_nome_usuario( $this->input->post('editar_nome_usuario') );
        $usuario_obj->set_email( $this->input->post('editar_email') );
        $usuario_obj->set_tipo_usuario( $this->session->usuario['tipo_usuario'] );
        $usuario_obj->set_ra( $this->input->post('editar_ra') );

        $resultado = $usuario_obj->editar();

        if($resultado == true) {
            redirect('meu-perfil/editar/sucesso/16' );
        } else {
            redirect('usuarios/editar/erro/17' );
        }
    }

    public function trocar_senha( $msg = null, $codigo = null )
    {
        $data['msg'] = $msg;
        $data['codigo'] = $codigo;
        $this->load->view('meu-perfil-trocar-senha', $data);
    }

    public function trocar_senha_form()
    {
        $senha_atual = $this->input->post('senha-atual');
        $nova_senha = $this->input->post('nova-senha');
        $nova_senha_novamente = $this->input->post('confirmar-nova-senha');

        if( Usuario::autenticar( $this->session->usuario['email'], $senha_atual ) ) {

            if( $nova_senha == $nova_senha_novamente ) {

                $usuario_obj = new Usuario();
                $usuario_obj->set_id_usuario( $this->session->usuario['id_usuario'] );
                $usuario_obj->set_forcar_troca_senha(false);
                $usuario_obj->set_senha($nova_senha);
                
                $resultado = $usuario_obj->trocarSenha();

                if( $resultado == true ){
                    redirect('meu-perfil/trocar-senha/sucesso/18');
                } else {
                    redirect('meu-perfil/trocar-senha/erro/6');
                }

            } else {
                redirect('meu-perfil/trocar-senha/erro/5');
            }

        } else {
            redirect('meu-perfil/trocar-senha/erro/4');
        }
    }

}
