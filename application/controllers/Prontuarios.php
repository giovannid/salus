<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prontuarios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function consultar( $id_prontuario = null, $msg = null, $codigo = null )
    {
        $prontuario = $this->prontuario->consultar( $id_prontuario );

        if( !empty($prontuario) && $id_prontuario != null ) {
            
            if( $prontuario[0]->get_usuario()->get_id_usuario() !== $this->session->usuario['id_usuario'] && !in_array( $this->session->usuario['tipo_usuario'], [1, 2] ) ) {
                redirect('pacientes/consultar/' . $prontuario[0]->get_paciente()->get_id_paciente() . '/erro/49#prontuarios' );
            }

            if( $prontuario[0]->get_paciente()->get_candidato() == true ) {
                redirect('inicio/index/erro/51');
            }

            $data['prontuario'] = $prontuario[0];
            $data['prontCampos'] = $this->prontCampos();
            $data['msg'] = $msg;
            $data['codigo'] = $codigo;

            $this->load->view( 'prontuario-consultar', $data );

        } else {
            show_404();
        }
    }

    public function editar( $id_prontuario = null, $msg = null, $codigo = null ) 
    {
        $prontuario = $this->prontuario->consultar( $id_prontuario );
        if($prontuario[0]->get_bloqueado() == true) {
            redirect('prontuarios/consultar/'. $id_prontuario .'/erro/48');
            return;
        }

        if( !empty($prontuario) ) {

            $data['msg'] = $msg;
            $data['codigo'] = $codigo;
            $data['prontuario'] = $prontuario[0];
            $data['prontCampos'] = $this->prontCampos();
            
            $this->load->view( 'prontuario-editar', $data );

        } else {
            show_404();
        }
    }

    public function editar_form($id_prontuario = null) 
    {
        if( $id_prontuario !== null ) {
            
            $data = array();

            foreach($this->input->post() as $campo => $escolha){

                if( method_exists('Prontuario', 'set_' . $campo) ) {

                    if( !empty($escolha) ) {

                        if( is_array($escolha) && count($escolha) === 1 && $escolha[0] == 0) {
                            $data[$campo] = null;
                        } elseif( is_array($escolha) && count($escolha) > 1 ) {
                            unset($escolha[0]);
                            $data[$campo] = json_encode($escolha);
                        } else {
                            // campo de texto
                            $data[$campo] = ( strlen($escolha) > 15000 ) ? substr($escolha, 0, 14999) : $escolha;
                        }

                    } else {
                        $data[$campo] = null;
                    }

                }

            }

            if( !$this->prontuario->editar($data, $id_prontuario) ) {
                log_message('error', 'Metodo [editar_form] Não foi possivel ATUALIZAR novos registros na tabela prontuário!' . $this->db->error() );
                redirect('prontuarios/editar/' . $id_prontuario . '/erro/15' );
            } else {
                redirect('prontuarios/editar/' . $id_prontuario . '/sucesso/14' );
            }

        } else {
            show_404();
        }
    }

    public function remover( $id_prontuario = null, $id_paciente = null) 
    {

        if( !empty($id_prontuario) && !empty($id_paciente) ) {

            $resultado = $this->prontuario->remover( $id_prontuario );
            if($resultado == true) {
                redirect('pacientes/consultar/' . $id_paciente . '/sucesso/32#prontuarios' );
            } else {
                redirect('pacientes/consultar/' . $id_paciente . '/erro/33#prontuarios' );
            }
            
        } else {
            show_404();
        }
    }

    private function prontCampos()
    {
        $campos = array(
            'prontuario' => array(
                'Avaliação do Desenvolvimento Motor' => array(
                    'Visão'                 => array('opcao' => 'adm_visao'),
                    'Audição'               => array('opcao' => 'adm_audicao'),
                    'Linguagem'             => array('opcao' => 'adm_linguagem'),
                    'Cognitivo'             => array('opcao' => 'adm_cognitivo'),
                    'Reflexos Primitivos'   => array('opcao' => 'adm_reflexos_prim')
                ),
                'Supino' => array(
                    'Simetria'           => array('opcao' => 'supino_simetria'),
                    'Alinhamento'        => array('opcao' => 'supino_alinhamento'),
                    'Movimentação Ativa' => array('opcao' => 'supino_movimentacao_ativa'),
                    'Observação'         => array('texto' => ['nome' => 'supino_obs', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Prono' => array(
                    'Controle Cervical'  => array('opcao' => 'prono_controle_cervical'),
                    'Controle Escapular' => array('opcao' => 'prono_controle_escapular'),
                    'Simetria'           => array('opcao' => 'prono_simetria'),
                    'Alinhamento'        => array('opcao' => 'prono_alinhamento'),
                    'Movimentação Ativa' => array('opcao' => 'prono_movimentacao_ativa'),
                    'Observação'         => array('texto' => ['nome' => 'prono_obs', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Rolar' => array(
                    'Opções' => array('opcao' => 'rolar')
                ),
                'Sentado' => array(
                    'Controle Cervical'  => array('opcao' => 'sentado_controle_cervical'),
                    'Controle de Tronco' => array('opcao' => 'sentado_controle_tronco'),
                    'Simetria'           => array('opcao' => 'sentando_simetria'),
                    'Alinhamento'        => array('opcao' => 'sentado_alinhamento'),
                    'Movimentação Ativa' => array('opcao' => 'sentado_movimentacao_ativa'),
                    'Observação'         => array('texto' => ['nome' => 'sentado_obs', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Troca Postural de Supino para Sentado' => array(
                    'Postura de Quadril'     => array('opcao' => 'sentado_postura_quadril'),
                    'Deformidade da Coluna'  => array('opcao' => 'sentado_deformidade_coluna'),
                    'Deformidade de Quadril' => array('opcao' => 'sentado_deformidade_quadril'),
                    'Observação'             => array('texto' => ['nome' => 'sentado_troca_postural', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Engatinhar' => array(
                    'Opções'    => array('opcao' => 'engatinhar'),
                    'Descrição' => array('texto' => ['nome' => 'engatinhar_obs', 'rows' => 3, 'maxlength' => 15000])
                ), 
                'Arrastar' => array(
                    'Opções'    => array('opcao' => 'arrastar'),
                    'Descrição' => array('texto' => ['nome' => 'arrastar_obs', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Ortostatismo' => array(
                    'Opções' => array('opcao' => 'ortostatismo')
                ),
                'Marcha' => array(
                    'Opções'    => array('opcao' => 'marcha'),
                    'Descrição' => array('texto' => ['nome' => 'marcha_obs', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Observações' => array(
                    'Descrição' => array('texto' => ['nome' => 'observacao', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Tônus de Base' => array(
                    'Hipertonia Elástica (grupos musculares)' => array('opcao' => 'tonus_base_hipertonia_elastica'),
                    'Hipertonia Elástica: Sinais Clinicos'    => array('texto' => ['nome' => 'tonus_base_hipertonia_elastica_sinais_clinicos', 'rows' => 3, 'maxlength' => 15000]),
                    'Asworth'                                 => array('texto' => ['nome' => 'tonus_base_asworth', 'rows' => 3, 'maxlength' => 15000]),
                    'Hipertonia Plástica'                     => array('opcao' => 'tonus_base_hipertonia_plastica'),
                    'Hipertonia Plástica: Sinais Clinicos'    => array('texto' => ['nome' => 'tonus_base_hipertonia_plastica_sinais_clinicos', 'rows' => 3, 'maxlength' => 15000]),
                    'Discinesias'                             => array('opcao' => 'tonus_base_discinesias'),
                    'Hipotonia'                               => array('opcao' => 'tonus_base_hipotonia'),
                    'Incoordenação de movimentos'             => array('opcao' => 'tonus_base_incordenacao_movimentos')
                ),
                'Tônus Dinâmico' => array(
                    'Descrição' => array('texto' => ['nome' => 'tonus_dinamico', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Encurtamentos Musculares e Deformidades' => array(
                    'Descrição' => array('texto' => ['nome' => 'encurtamento_musculares_deformidades', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Força Muscular' => array(
                    
                ),
                'Mecanismo Reflexo Postural' => array(
                    
                ),
                'Atividades de Vida Diária' => array(
                    'Alimentação' => array('opcao' => 'atividades_vida_diaria_alimentacao'),
                    'Alimentação: Observações' => array('texto' => ['nome' => 'atividades_vida_diaria_alimentacao_obs', 'rows' => 3, 'maxlength' => 15000]),
                    'Higiene'     => array('opcao' => 'atividades_vida_diaria_higiene'),
                    'Higiene: Observações' => array('texto' => ['nome' => 'atividades_vida_diaria_higiene_obs', 'rows' => 3, 'maxlength' => 15000]),
                    'Vestuário'   => array('opcao' => 'atividades_vida_diaria_vestuario'),
                    'Vestuário: Observações' => array('texto' => ['nome' => 'atividades_vida_diaria_vestuario_obs', 'rows' => 3, 'maxlength' => 15000]),
                    'Locomoção'   => array('opcao' => 'atividades_vida_diaria_locomocao'),
                    'Locomoção: Observações' => array('texto' => ['nome' => 'atividades_vida_diaria_locomocao_obs', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Sistema Respiratório' => array(
                    'Descrição' => array('texto' => ['nome' => 'sistema_respiratorio', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Objetivos' => array(
                    'Descrição' => array('texto' => ['nome' => 'objetivos', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Condutas' => array(
                    'Descrição' => array('texto' => ['nome' => 'condutas', 'rows' => 3, 'maxlength' => 15000])
                ),
                'Evolução do Período' => array(
                    'Descrição' => array('texto' => ['nome' => 'evolucao_periodo', 'rows' => 15, 'maxlength' => 15000])
                ),
            )
        );
        return $campos;
    }

}
