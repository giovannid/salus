<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="meu-perfil">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?= base_url('usuarios'); ?>">Usuários</a>
                        </li>
                        <li>
                            <a href="<?= base_url('usuarios/consultar/' . $usuario->get_id_usuario() ); ?>"><?= $usuario->get_nome_usuario(); ?></a>
                        </li>
                        <li class="active">Nova Senha Provisória</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3" id="bateria">
                    <div class="btn-group" role="group">
                        <button class="btn btn-style06" id="salvarUsuario">Salvar</button>
                        <a class="btn btn-style06" href="<?= base_url('usuarios/consultar/' . $usuario->get_id_usuario() ); ?>">Voltar</a>
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

            <?= form_open('usuarios/trocar-senha-form/' . $usuario->get_id_usuario(),
                array( 'id' => 'usuarios_form', 'name' => 'usuarios_form' ) ); ?>

                <section class="grupo-pront">
                    <div class="row">

                        <div class="col-md-4">
                            <h4 class="title">Detalhes do Usuário</h4>
                        </div> <!-- col-md-4 -->

                        <div class="col-md-8">

                            <div class="form-group">
                                <label>Nova Senha Provisória</label>
                                <input type="password" name="senha-provisoria" class="form-control" >
                            </div>

                        </div> <!-- col-md-8 -->

                    </div> <!-- row -->
                </section> <!-- grupo-pront -->

        </div> <!-- container -->
    </section> <!-- padrao-conteudo -->

</div> <!-- meu-perfil -->
<?php $this->load->view('fixos/rodape'); ?>