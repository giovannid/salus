<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="candidato">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li class="active">Pacientes</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        
                    </div>
                </div> <!-- cold-md-3 -->
                
            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">

        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="row">
                        
                        <div class="col-md-4">
                            <div class="filtrar-candidato">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="texto-style10">Filtrar</span>
                                        <div class="input-group">
                                            <span class="input-group-addon">Filtrar por:</span>
                                            <span class="input-group-btn">
                                                <a class="btn <?= (isset($por_nome) && $por_nome == true ) ? 'btn-style06 btn-fix' : 'btn-style08'; ?>" href="<?php echo base_url('pacientes/index/0/0/filtrar/nome'); ?>">Nome</a>
                                                <a class="btn <?= (isset($por_idade) && $por_idade == true ) ? 'btn-style06 btn-fix' : 'btn-style08'; ?>" href="<?php echo base_url('pacientes/index/0/0/filtrar/idade'); ?>">Idade</a>
                                                <a class="btn <?= (isset($com_alta) && $com_alta == true ) ? 'btn-style06 btn-fix' : 'btn-style08'; ?>" href="<?php echo base_url('pacientes/index/0/0/alta'); ?>">Com Alta</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- filtrar-paciente -->
                        </div> <!-- col-md-3 -->

                        <div class="col-md-8">
                            <div class="buscar-candidato">
                                <?= form_open('pacientes/buscar', ['name' => 'buscar_paciente']); ?>
                                    <div class="form-group">
                                        <span class="texto-style10">Buscar</span>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="info"  placeholder="Digite um nome, nome do responsável ou queixa principal" <?= (isset($buscando_por)) ? 'value="' . $buscando_por . '"' : ''; ?>>
                                            <span class="input-group-btn">
                                                <input class="btn btn-style08" type="submit" value="Buscar">
                                            </span>
                                        </div><!-- /input-group -->
                                        
                                    </div>
                                </form>
                            </div>
                        </div> <!-- col-md-9 -->

                    </div> <!-- row -->

                    <div class="padrao-lista" id="candidato-lista">

                        <?php if( !empty($pacientes) || !empty($buscando_por) ): ?>

                            <?php if(isset($buscando_por)): ?>
                            <div class="buscando_por">
                                <span>Buscando por: <?= $buscando_por; ?></span>

                                <?php if(empty($pacientes)): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            Não foi encontrado nenhum paciente
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <?php else: ?>
                            
                            <div class="topo-lista-personalizada">
                                <span class="pull-left">Nome do Paciente</span>
                                <span class="pull-right">Ações</span>
                            </div>

                            <?php endif; ?>

                        <?php foreach($pacientes as $paciente): ?>

                            <div class="padrao-item">
                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div class="pull-left titulo-lista-personalizada">
                                            <?php if(isset($com_alta) && $com_alta == true): ?>
                                                    <div class="label label-bloqueado" style="float: left; margin-top: 9px; margin-right: 10px; ">Com Alta</div>
                                                <?php endif; ?>
                                            <div class="tipo_usuario">
                                                
                                                <span class="label label-success">Paciente</span>
                                            </div>
                                            
                                            <div class="nome_usuario">
                                                <span class="text-capitalize"><?php echo $paciente->get_nome_completo(); ?></span> 
                                            </div>

                                        </div> <!-- titulo-lista-personalizada -->

                                        <div class="pull-right acoes-lista-personalizada">

                                            <div class="btn-group" role="group">
                                                <a class="btn btn-style06" href="<?php echo base_url('/pacientes/consultar/' . $paciente->get_id_paciente() ); ?>">Ver</a>

                                                <a class="btn btn-style06" href="<?php echo base_url('/pacientes/consultar/' . $paciente->get_id_paciente() ); ?>#consultas">Consultas</a>

                                                <?php if( session_visao([1,2]) ): ?>
                                                    <button type="button" class="btn btn-style06 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="<?php echo base_url('/pacientes/mover-para-candidato/' . $paciente->get_id_paciente() ); ?>">Mover Para Candidato</a>
                                                        </li>
                                                        <li>
                                                            <a href="" data-toggle="modal" data-target="#alta_paciente_<?= $paciente->get_id_paciente(); ?>" >Dar alta</a>
                                                        </li>
                                                    </ul>
                                                <?php endif; ?>

                                            </div> <!-- btn-group -->
                                            
                                        </div> <!-- acoes-lista-personalizada -->

                                    </div> <!-- panel-body -->

                                    <div class="panel-footer">
                                        <div class="padrao-lista-info">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Idade: <span class="link-local"><?php echo $paciente->calcularIdade(); ?></span>
                                                </div>
                                                <div class="col-md-4">
                                                    Queixa Principal: <span class="link-local text-capitalize"><?php echo ( $paciente->get_queixa_principal() ) ? $paciente->get_queixa_principal() : 'Nenhuma'; ?></span>
                                                </div>
                                                <div class="col-md-4">
                                                    Faltas: 
													<a class="link-local" href="<?= base_url('pacientes/consultar/' . $paciente->get_id_paciente() . '#consultas#faltasPaciente' ); ?>"><?= count($this->consulta->pegarFaltas(null, $paciente->get_id_paciente())); ?></a>
                                                </div>
                                            </div> <!-- row -->
                                        </div> <!-- candidato-info -->
                                    </div>

                                </div> <!-- panel-default -->
                            </div> <!-- padrao-item -->

                            <div class="modal fade alta_modal" id="alta_paciente_<?= $paciente->get_id_paciente(); ?>" tabindex="-1">
                                <div class="modal-dialog">
                                    
                                    <?php echo form_open('pacientes/dar_alta/' . $paciente->get_id_paciente() , ['class' => 'alta_form'] ); ?>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            <h4 class="modal-title">Dar alta</h4>
                                        </div> <!-- modal-header -->
                                        
                                        <div class="modal-body">
                                            
                                            <div class="form-group">
                                                <label class="control-label">Paciente:</label>
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <?= $paciente->get_nome_completo(); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Motivo:</label>
                                                <textarea class="form-control" name="motivo_alta" style="resize: none;" rows="3" required></textarea>
                                            </div>

                                        </div> <!-- modal-body -->

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-style08" data-dismiss="modal">Fechar</button>
                                            <input type="submit" name="salvar_alta" class="btn btn-danger" value="Salvar">
                                        </div> <!-- modal-footer -->
                                    </div> <!-- modal-content -->
                                    </form>

                                </div> <!-- modal-dialog -->
                            </div> <!-- modal -->

                        <?php endforeach; ?>

                        <?php else: ?>

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?php if(isset($com_alta) && $com_alta == true): ?>
                                        Não exitem pacientes com alta.
                                    <?php else: ?>
                                        Não existem pacientes cadastrados.
                                    <?php endif; ?>
                                </div>
                            </div>

                        <?php endif; ?>

                    </div> <!-- usuarios-lista -->

                </div> <!-- col-md-12 -->

            </div> <!-- row -->
        </div> <!-- container -->

    </section> <!-- padrao-conteudo -->

</div> <!-- candidato -->

<?php $this->load->view('fixos/rodape'); ?>