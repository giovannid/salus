<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="configuracoes">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li class="active">Configurações</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3" id="bateria">
                    
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <section class="padrao-info">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                   
                    <ul class="list-inline lista-abas" role="tablist" id="abas_padrao">

                        <li role="presentation" class="active">
                            <a href="#horario-consultas" role="tab" data-toggle="tab">Horários das Consultas</a>
                        </li>

                        <li role="presentation">
                            <a href="#campos-personalizaveis" role="tab" data-toggle="tab">Opções do Prontuário</a>
                        </li>
                        
                    </ul> <!-- lista-abas -->

                </div> <!-- col-md-12 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-info -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

            <div class="tab-content">

                <section role="tabpanel" class="tab-pane active grupo-pront" id="horario-consultas">

                    <div class="row">

                        <div class="col-md-4">
                            <h4 class="title">Horários Permitidos</h4>
                        </div> <!-- col-md-4 -->

                        <div class="col-md-8">

                            <?php  echo form_open('configuracoes/horario-permitidos',
                            array( 'id' => 'configuracoes_horarios_form', 'name' => 'configuracoes_horarios_form') ); ?>

                                <div class="config-horario-section">
                                    <label>Horários Cadastrados</label>

                                    <div class="row">
                                        <div class="col-md-10">
                                            <ul id="horarios_lista">
                                                <?php if( !empty($horarios) ): ?>

                                                    <?php foreach($horarios as $index => $horario): ?>

                                                        <li class="horario-item" data-hid="<?=$horario['index']; ?>">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <span class="horario-item-nome">Horario <?=$horario['index']; ?></span>
                                                                </div>

                                                                <div class="col-md-7">
                                                                    <div class="horario-item-inputs">
                                                                        <div class="horario-item-inicio">
                                                                            Inicio:
                                                                            <input class="horario_picker" type="text" name="horario_<?=$horario['index']; ?>[]" value="<?=$horario['inicio']; ?>">
                                                                            <span class=" glyphicon icone-direita glyphicon-arrow-right"></span>
                                                                        </div>

                                                                        <div class="horario-item-fim">
                                                                            Fim:
                                                                            <input class="horario_picker" type="text" name="horario_<?=$horario['index']; ?>[]" value="<?=$horario['fim']; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="horario-item-remover">
                                                                        <span class="glyphicon remover glyphicon-remove" data-record="<?=$horario['record']; ?>" data-hid="<?=$horario['index']; ?>"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="horario_<?=$horario['index']; ?>[]" value="<?=$horario['record']; ?>">
                                                        </li>

                                                    <?php endforeach; ?>

                                                <?php else: ?>
                                                    <li class="horario-item nenhum">Nenhum horário cadastrado.</li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>

                                        <div class="col-md-2">
                                            <button class="btn btn-style06" id="adicionarHorario">Adicionar</button>
                                        </div> <!-- col-md-2 -->
                                    </div>

                                </div> <!-- form-group -->

                                <div class="form-group">
                                    <button class="btn btn-style06" type="submit" id="salvarHorario">Salvar</button>
                                </div>

                            </form> <!-- form -->

                        </div> <!-- cold-md-8 -->

                    </div> <!-- row -->
                    
                </section> <!-- horario-consultas -->

                <section role="tabpanel" class="tab-pane grupo-pront no-border-bottom no-padding-top"  id="campos-personalizaveis">

                    <?php   echo form_open('configuracoes/opcoes-prontuarios',
                            array( 'id' => 'configuracoes_opcoes_prontuarios', 'name' => 'configuracoes_opcoes_prontuarios') ); ?>
                    
                    <section class="campos-perso">
                        <div class="row">

                            <div class="col-md-4">
                                <h4 class="title">Gerenciar Opções</h4>
                            </div> <!-- col-md-4 -->

                            <div class="col-md-8">
                                <div class="quadrado-redondo">
                                    <label>Crie/Edite/Remova as opções para cada seção do prontuário.</label>
                                    <button class="btn btn-style06" id="configuracoes_opcoes_prontuarios_salvar">Salvar Opções</button>
                                </div>
                            </div> <!-- cold-md-8 -->

                        </div> <!-- row -->
                    </section> <!-- campos-perso -->

                    <?php foreach($campos as $secao => $campos): ?>

                    <section class="campos-perso">
                        <div class="row">

                            <div class="col-md-4">
                                <h4 class="title cinza"><?= $secao; ?></h4>
                            </div> <!-- col-md-4 -->

                            <div class="col-md-8">

                                <?php foreach($campos as $nome => $field): ?>

                                <div class="grupo-campo-perso dynamic_here" id="<?= $field; ?>">
                                    <div class="form-group">
                                        <label><?= $nome; ?></label>
                                        <div class="panel panel-default">
                                            <div class="panel-body" id="<?= $field; ?>_parent">

                                                <?php 
                                                $opcoes_pront = $this->prontuario->pegarOpcoesParaProntuario( $field );
                                                if( !empty( $opcoes_pront ) ): ?>

                                                <?php foreach( $opcoes_pront as $index => $registro ): ?>

                                                    <div class="opcao" id="<?= $registro['campo'] . '_' . $index;  ?>">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" data-name="<?=$registro['campo']; ?>" name="<?=$registro['campo']; ?>[<?= $index; ?>][opcao]" placeholder="Nome da Opção" value="<?=$registro['opcao']; ?>">
                                                            <input type="hidden" name="<?=$registro['campo']; ?>[<?= $index; ?>][update]" value="<?=$registro['id_opcoes']; ?>">
                                                            <span class="input-group-addon">
                                                                <label>
                                                                    <input type="checkbox" name="<?=$registro['campo']; ?>[<?= $index; ?>][obs]" value="1" <?= ( $registro['obs'] == 's' ) ? 'checked' : ''; ?>>
                                                                    Precisa Descrição?
                                                                </label>
                                                            </span>
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-style08" type="button" id="<?=$registro['campo']; ?>_remove_current" data-remove="<?= $index; ?>"> 
                                                                    <span class="glyphicon glyphicon-remove opcao_remover"></span>
                                                                </button>
                                                            </span>
                                                        </div><!-- input-group -->
                                                    </div> <!-- opcao -->
                                                
                                                <?php endforeach; ?>

                                                <?php else: ?>
                                                    <span class="vazio">Não existem opções cadastradas.</span> <input type="hidden" name="<?= $field; ?>" value="0">
                                                <?php endif; ?>
                                                
                                            </div>
                                            <div class="panel-footer">
                                                <button class="btn btn-style08" type="button" id="<?= $field; ?>_add_new">Adicionar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- CAMPOS-ID -->

                                <?php endforeach; ?>

                            </div> <!-- cold-md-8 -->

                        </div> <!-- row -->
                    </section> <!-- campos-perso -->

                    <?php endforeach; ?>

                    </form> <!-- configuracoes_opcoes_prontuarios -->
                    
                </section> <!-- campos-personalizaveis -->

            </div>

        </div> <!-- contianer -->
    </section> <!-- padrao-conteudo --> 

</div> <!-- meu-perfil -->

<?php $this->load->view('fixos/rodape'); ?>