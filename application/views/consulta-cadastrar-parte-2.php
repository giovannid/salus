<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="consulta">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url('consultas'); ?>">Consultas</a></li>
                        <li class="active">
                            Cadastrar Consulta
                        </li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        <button class="btn btn-style06" id="salvarConsulta">Salvar</button>
                        <a class="btn btn-style06" href="<?php echo base_url('consultas/index'); ?>">Voltar</a>
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

            <?php if($horarios != null): ?>
        
                <?php echo form_open('consultas/cadastrar_form/' . $paciente->get_id_paciente() ,
                        array( 'id' => 'consultas_form', 'name' => 'consultas_form' ) ); ?>

                    <section class="grupo-pront consulta_horario">

                        <div class="row">
                            <div class="col-md-4">
                                <h4 class="title">Paciente</h4>
                                
                            </div>

                            <div class="col-md-8">

                                <div class="form-group">
                                    <label>Paciente</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?= $paciente->get_nome_completo(); ?>
                                        </div>
                                    </div>
                                    </select>
                                </div>
                                
                            </div>
                        </div> <!-- row -->

                    </section>

                    <section class="grupo-pront consulta_horario">

                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="title">Detalhes da Consulta</h4>

                                <div class="horario_consulta_topo">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-left: -15px;">Data</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label>Horário</label>
                                        </div>
                                        <div class="col-md-3">
                                            <label>
                                                Estágiario/Supervisor 
                                                <i  class="fa fa-question-circle" style="cursor: pointer; font-size: 16px; margin-left: 5px;" 
                                                    aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="<?= erro_msg(79); ?>"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="consulta_horario_item">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" name="consulta_novo_data" class="form-control consulta_horario_campo_data" placeholder="Data" value="<?= $dia_hoje; ?>" required>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="consulta_novo_horario" class="form-control required" required>
                                                <option value="">Selecione um horário</option>
                                                <?php $contador = 1; foreach($horarios as $id => $hora): ?>
                                                    <optgroup label="Horario <?= $contador; ?>">    
                                                        <option value="<?= $id; ?>">Hora Inicio: <?= $hora[0]; ?> - Hora Fim: <?= $hora[1]; ?></option>
                                                    </optgroup>
                                                <?php $contador++; endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-3">
                                            <select name="consulta_novo_usuario" class="form-control required" required>
                                                <option value="">Selecione um usuário</option>
                                                <?php foreach($usuarios as $index => $usuario): ?>
                                                    <option value="<?= $usuario->get_id_usuario(); ?>"><?= $usuario->get_nome_usuario(); ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- row -->

                    </section> <!-- consulta_horario -->

                </form>
            
            <?php else: ?>

                <br>
                 <div class="panel panel-default">
                    <div class="panel-body">

                        <span><?= erro_msg(78); ?></span>

                    </div> <!-- panel-body -->
                </div> <!-- panel-default -->

            <?php endif; ?>

        </div> <!-- container -->
    </section> <!-- padrao-conteudo -->

</div> <!-- bateria -->

<?php $this->load->view('fixos/rodape'); ?>