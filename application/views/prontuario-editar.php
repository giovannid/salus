<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="prontuario">

    <section class="padrao-topo navbar-fixed-top1">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <?php if( session_visao( [1, 2, 4] ) ): ?>
                        <li>
                            <a href="<?php echo base_url('pacientes'); ?>">Pacientes</a>
                        </li>
                        <?php endif; ?>

                        <?php if( session_visao( [3] ) ): ?>
                        <li class="active">
                            Pacientes
                        </li>
                        <?php endif; ?>
                        <li>
                            <a href="<?php echo base_url('pacientes/consultar/' . $prontuario->get_paciente()->get_id_paciente() . '#prontuarios' ); ?>" class="text-capitalize">
                                <?php echo $prontuario->get_paciente()->get_nome_completo(); ?>
                            </a>
                        </li>
                        <li class="active">Editar Prontuário</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">

                        <!-- Salvar -->
                        <button class="btn btn-style06" id="salvarProntuario">Salvar</button>
                        <!-- Salvar -->
                        
                        <!-- Voltar -->
                        <a class="btn btn-style06" href="<?php echo base_url('prontuarios/consultar/' . $prontuario->get_id_prontuario() ); ?>">Voltar</a>
                        <!-- Voltar -->

                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->

    </section> <!-- padrao-topo -->

    <section class="padrao-info">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="prontuario-criado-por">
                        <p class="pull-left">Vinculado ao Usuário: <?php echo $prontuario->get_usuario()->get_nome_usuario(); ?></p>
                        <p class="pull-right">Ultima atualização: <?php echo $prontuario->get_data_atualizacao(); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- padrao-info -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

            <?php echo form_open('prontuarios/editar_form/' . $prontuario->get_id_prontuario(), 
                    array( 'id' => 'prontuarios_form', 'name' => 'prontuarios_form' ) ); ?>

                <?php foreach($prontCampos['prontuario'] as $secao => $campos): ?>

                    <section class="grupo-pront">

                        <div class="row">

                            <div class="col-md-4">
                                <h4 class="title"><?= $secao; ?></h4>
                            </div> <!-- col-md-4 -->

                            <div class="col-md-8">

                                <?php foreach($campos as $campo => $specs): ?>

                                    <div class="form-group">
                                        <label><?= $campo; ?></label>
                                        <div class="form-group">
                                            <?php if( array_key_exists('opcao', $specs) ): ?>

                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <input type="hidden" name="<?= $specs['opcao'] ?>[]" value="0">
                                                        <?php   $opcoes = $prontuario->pegarOpcoesParaProntuario( $specs['opcao'] );
                                                                echo pront_radio_input( $opcoes, $prontuario->{'get_'.$specs['opcao']}() ); ?>
                                                    </div>
                                                </div>

                                            <?php elseif( array_key_exists('texto', $specs) ): ?>
                                                
                                                <?php   $texto = array(
                                                    'name' => $specs['texto']['nome'],
                                                    'id' => $specs['texto']['nome'],
                                                    'value' => $prontuario->{'get_'.$specs['texto']['nome']}(),
                                                    'rows' => $specs['texto']['rows'],
                                                    'maxlength' => $specs['texto']['maxlength'],
                                                    'class' => 'form-control'
                                                );
                                                echo form_textarea( $texto ); ?>

                                            <?php endif; ?>
                                        </div>
                                    </div> <!-- form-group -->

                                <?php endforeach; ?>




                                <?php if( $secao == 'Força Muscular' ): ?>

                                    <table class="table table-bordered">
                                        <thead style="text-align: center;">
                                            <tr>
                                                <td width="50%" colspan="2">Grupos Musculares</td>
                                                <td width="25%">D</td>
                                                <td width="25%">E</td>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td width="25%" style="text-align:center; vertical-align: middle;">MMSS</td>
                                            <td>
                                                <?php   $forca_muscular_mms_gm = array(
                                                            'name' => 'forca_muscular_mms_gm',
                                                            'id' => 'forca_muscular_mms_gm',
                                                            'value' => $prontuario->get_forca_muscular_mms_gm(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $forca_muscular_mms_gm ); ?>
                                            </td>
                                            <td>
                                                <?php   $forca_muscular_mms_d = array(
                                                            'name' => 'forca_muscular_mms_d',
                                                            'id' => 'forca_muscular_mms_d',
                                                            'value' => $prontuario->get_forca_muscular_mms_d(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $forca_muscular_mms_d ); ?>
                                            </td>
                                            <td>
                                                <?php   $forca_muscular_mms_e = array(
                                                            'name' => 'forca_muscular_mms_e',
                                                            'id' => 'forca_muscular_mms_e',
                                                            'value' => $prontuario->get_forca_muscular_mms_e(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $forca_muscular_mms_e ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="text-align:center; vertical-align: middle;">MMII</td>
                                            <td>
                                                <?php   $forca_muscular_mmii_gm = array(
                                                            'name' => 'forca_muscular_mmii_gm',
                                                            'id' => 'forca_muscular_mmii_gm',
                                                            'value' => $prontuario->get_forca_muscular_mmii_gm(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                    echo form_textarea( $forca_muscular_mmii_gm ); ?>
                                            </td>
                                            <td>
                                                <?php   $forca_muscular_mmii_d = array(
                                                            'name' => 'forca_muscular_mmii_d',
                                                            'id' => 'forca_muscular_mmii_d',
                                                            'value' => $prontuario->get_forca_muscular_mmii_d(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                    echo form_textarea( $forca_muscular_mmii_d ); ?>
                                            </td>
                                            <td>
                                                <?php   $forca_muscular_mmii_e = array(
                                                            'name' => 'forca_muscular_mmii_e',
                                                            'id' => 'forca_muscular_mmii_e',
                                                            'value' => $prontuario->get_forca_muscular_mmii_e(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $forca_muscular_mmii_e ); ?>
                                            </td>
                                        </tr>
                                    </table>





                                <?php elseif( $secao == 'Mecanismo Reflexo Postural' ): ?>




                                    <table class="table table-bordered">
                                        <thead style="text-align: center;">
                                            <tr>
                                                <td width="33.333333333%">Reações Posturais</td>
                                                <td width="33.333333333%">Postura Sentada</td>
                                                <td width="33.333333333%">Bípede</td>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td width="33.333333333%" style="font-size:15px; text-align:center; vertical-align: middle;">Reações de Endireitamento (A, I, C)</td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_endireitamento_postura_sentada = array(
                                                            'name' => 'mrp_reacoes_endireitamento_postura_sentada',
                                                            'id' => 'mrp_reacoes_endireitamento_postura_sentada',
                                                            'value' => $prontuario->get_mrp_reacoes_endireitamento_postura_sentada(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_endireitamento_postura_sentada ); ?>
                                            </td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_endireitamento_bipede = array(
                                                            'name' => 'mrp_reacoes_endireitamento_bipede',
                                                            'id' => 'mrp_reacoes_endireitamento_bipede',
                                                            'value' => $prontuario->get_mrp_reacoes_endireitamento_bipede(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_endireitamento_bipede ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33.333333333%" style="text-align:center; vertical-align: middle;">Reações de Equilibrio (A, I, C)</td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_equilibrio_postura_sentada = array(
                                                            'name' => 'mrp_reacoes_equilibrio_postura_sentada',
                                                            'id' => 'mrp_reacoes_equilibrio_postura_sentada',
                                                            'value' => $prontuario->get_mrp_reacoes_equilibrio_postura_sentada(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_equilibrio_postura_sentada ); ?>
                                            </td>
                                            <td width="33.333333333%">
                                                <?php $mrp_reacoes_equilibrio_bipede = array(
                                                        'name' => 'mrp_reacoes_equilibrio_bipede',
                                                        'id' => 'mrp_reacoes_equilibrio_bipede',
                                                        'value' => $prontuario->get_mrp_reacoes_equilibrio_bipede(),
                                                        'rows' => 3,
                                                        'class' => 'form-control'
                                                    );
                                                    echo form_textarea( $mrp_reacoes_equilibrio_bipede ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33.333333333%" style="text-align:center; vertical-align: middle;">Reações de Proteção (A, P)</td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_protecao_postura_sentada = array(
                                                            'name' => 'mrp_reacoes_protecao_postura_sentada',
                                                            'id' => 'mrp_reacoes_protecao_postura_sentada',
                                                            'value' => $prontuario->get_mrp_reacoes_protecao_postura_sentada(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_protecao_postura_sentada ); ?>
                                            </td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_protecao_bipede = array(
                                                            'name' => 'mrp_reacoes_protecao_bipede',
                                                            'id' => 'mrp_reacoes_protecao_bipede',
                                                            'value' => $prontuario->get_mrp_reacoes_protecao_bipede(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_protecao_bipede ); ?>
                                            </td>
                                        </tr>
                                    </table>

                                <?php endif; ?>






                            </div> <!-- col-md-8 -->
                            
                        </div> <!-- row -->

                    </section> <!-- ortostatismo -->

                <?php endforeach; ?>

            </form> <!-- end form -->

        </div> <!-- container -->
    </section> <!-- prontuario-conteudo -->

</div>

<?php $this->load->view('fixos/rodape'); ?>