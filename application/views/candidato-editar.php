<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="candidato">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url('candidatos'); ?>">Candidatos</a>	
                        </li>
                        <li class="active text-capitalize"><?php echo $candidato->get_nome_completo(); ?></li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        
                        <!-- Editar -->
                        <button class="btn btn-style06" id="salvarCandidato">Salvar</button>
                        <!-- Editar -->

                        <!-- Voltar -->
                        <a class="btn btn-style06" href="<?php echo base_url('candidatos/consultar/' . $candidato->get_id_paciente() ); ?>">Voltar</a>
                        <!-- Voltar -->

                    </div> <!-- btn-group -->
                </div> <!-- cold-md-3 -->
                
            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <section class="padrao-info">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                   
                    <ul class="list-inline lista-abas" role="tablist" id="abas_padrao">
                        <li role="presentation" class="active">
                            <a href="#" aria-controls="informacoes" role="tab" data-toggle="tab">Informações</a>
                        </li>
                    </ul> <!-- lista-abas -->


                </div> <!-- col-md-6 -->

                <div class="col-md-6">
                    <div class="prontuario-criado-por pull-right">
                        <p>Última Modificação: <?php echo $candidato->get_data_atualizacao(); ?></p>
                    </div>
                </div> <!-- col-md-6 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-info -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">

        <?php  echo form_open('candidatos/editar_form/' . $candidato->get_id_paciente() ,
               array( 'id' => 'candidato_form', 'name' => 'candidato_form' ) ); ?>

        <div class="candidato-ver">
            
            <div class="container">
                
                <div class="row">
                    
                    <div class="col-md-7">
                        <label>Nome Completo*</label>
                        <div class="form-group">
                            <input type="text" name="nome_completo" class="form-control" value="<?php echo $candidato->get_nome_completo(); ?>" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Idade</label>
                        <div class="form-group">
                            <input type="number" name="idade" class="form-control" value="<?php echo $candidato->calcularIdade(); ?>" data-toggle="tooltip" data-placement="bottom" title="Idade é calculada de acordo com a data de nascimento." disabled>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Data de Nascimento*</label>
                        <div class="form-group">
                            <input type="text" name="data_nascimento" class="form-control" id="candidato_data_nasc" value="<?= (new DateTime( $candidato->get_data_nascimento() ))->format("d/m/Y"); ?>" required>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <label>Sexo*</label>
                        <div class="form-group">
                            <select class="form-control" name="sexo" required>
                                <option value="m" <?php echo ( strtolower( $candidato->get_sexo() ) == 'm' ) ? 'selected' : ''; ?> >M</option>
                                <option value="f" <?php echo ( strtolower( $candidato->get_sexo() ) == 'f' ) ? 'selected' : ''; ?> >F</option>
                            </select>
                        </div>
                    </div>

                    
                </div> <!-- row -->

                <div class="row">
                    
                    <div class="col-md-12">
                        <label>Hipótese de Diagnóstico (HD)</label>
                        <div class="form-group">
                            <textarea name="hipotese_diagnostico" class="form-control" rows="2" style="resize: vertical; "><?php echo $candidato->get_hipotese_diagnostico(); ?></textarea>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>				

                <div class="row">
                    
                    <div class="col-md-6">
                        <label>Responsável*</label>
                        <div class="form-group">
                            <input type="text" name="nome_responsavel" class="form-control" value="<?php echo $candidato->get_nome_responsavel(); ?>" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Parentesco*</label>
                        <div class="form-group">
                            <input type="text" name="parentesco_resp" class="form-control" value="<?php echo $candidato->get_parentesco_resp(); ?>" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Telefone*</label>
                        <div class="form-group">
                            <input type="tel" name="telefone" class="form-control" value="<?php echo $candidato->get_telefone(); ?>" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Celular*</label>
                        <div class="form-group">
                            <input type="tel" name="celular" class="form-control" value="<?php echo $candidato->get_celular(); ?>" required>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">
                    
                    <div class="col-md-7">
                        <label>Endereço*</label>
                        <div class="form-group">
                            <input type="text" name="endereco" class="form-control" value="<?php echo $candidato->get_endereco(); ?>" required>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <label>Queixa Principal*</label>
                        <div class="form-group">
                            <input type="text" name="queixa_principal" class="form-control" value="<?php echo $candidato->get_queixa_principal(); ?>" required>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">
                    
                    <div class="col-md-12">
                        <label>Defict Funcional</label>
                        <div class="form-group">
                            <input type="text" name="deficit_funcional" class="form-control" value="<?php echo $candidato->get_deficit_funcional(); ?>">
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">
                    
                    <div class="col-md-6">
                        <label>Solicitação de acompanhamento de FT Motora - Médico(a) Resonsável</label>
                        <div class="form-group">
                            <input type="text" name="medico_resp" class="form-control" value="<?php echo $candidato->get_medico_resp(); ?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label>Hospital de procedência</label>
                        <div class="form-group">
                            <input type="text" name="hospital_procedencia" class="form-control" value="<?php echo $candidato->get_hospital_procedencia(); ?>">
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">

                    <div class="col-md-12">
                        <label>Tratamento prévios / cirurgias</label>
                        <div class="form-group">
                            <textarea name="tratamento_previo" class="form-control" rows="2" style="resize: vertical; "><?php echo $candidato->get_tratamento_previo(); ?></textarea>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">

                    <div class="col-md-2">
                        <label>ADNPM</label>
                        <div class="form-group">
                            <select class="form-control" name="adnpm">
                                <?php echo paciente_opcoes( $candidato->pegarOpcoesParaCamposDoPaciente('adnpm'), $candidato->get_adnpm() ); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-10">
                        <label>ADNPM Motivo</label>
                        <div class="form-group">
                            <input type="text" name="adnpm_motivo" class="form-control" value="<?php echo $candidato->get_adnpm_motivo(); ?>">
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">

                    <div class="col-md-2">
                        <label>Síndrome de Down</label>
                        <div class="form-group">
                            <select class="form-control" name="sindrome_de_down">
                                <?php echo paciente_opcoes( $candidato->pegarOpcoesParaCamposDoPaciente('sindrome_de_down'), $candidato->get_sindrome_de_down() ); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Mielomeningocele</label>
                        <div class="form-group">
                            <select class="form-control" name="mielo">
                                <?php echo paciente_opcoes( $candidato->pegarOpcoesParaCamposDoPaciente('mielo'), $candidato->get_mielo() ); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <label>Paralisia Braquial Congênita</label>
                        <div class="form-group">
                            <select class="form-control" name="paralisia_braquial">
                                <?php echo paciente_opcoes( $candidato->pegarOpcoesParaCamposDoPaciente('paralisia_braquial'), $candidato->get_paralisia_braquial() ); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <label>Outras Síndromes</label>
                        <div class="form-group">
                            <input type="text" name="outras_sindromes" class="form-control" value="<?php echo $candidato->get_outras_sindromes(); ?>">
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">

                    <div class="col-md-3">
                        <label class="texto-pequeno">Encefalopatia Crônica infantil não Progressiva</label>
                        <div class="form-group">
                            <select class="form-control" name="encefalopatia">
                                <?php echo paciente_opcoes( $candidato->pegarOpcoesParaCamposDoPaciente('encefalopatia'), $candidato->get_encefalopatia() ); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <label>Classificação Topográfica</label>
                        <div class="form-group">
                            <select class="form-control" name="class_topografia">
                                <?php echo paciente_opcoes( $candidato->pegarOpcoesParaCamposDoPaciente('class_topografia'), $candidato->get_class_topografia() ); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label>Classificação clínica</label>
                        <div class="form-group">
                            <input type="text" name="class_clinica" class="form-control" value="<?php echo $candidato->get_class_clinica(); ?>">
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">

                    <div class="col-md-2">
                        <label>Nivel</label>
                        <div class="form-group">
                            <select class="form-control" name="nivel">
                                <?php echo paciente_opcoes( $candidato->pegarOpcoesParaCamposDoPaciente('nivel'), $candidato->get_nivel() ); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>GMFCS nivel</label>
                        <div class="form-group">
                            <input type="text" name="gmfcs_nivel" class="form-control" value="<?php echo $candidato->get_gmfcs_nivel(); ?>">
                        </div>
                    </div>

                    <div class="col-md-8">
                        <label>CID</label><i class="fa fa-question-circle" style="cursor: pointer; font-size: 16px; margin-left: 5px; color: #646F80;" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?= erro_msg(87); ?>"><div></div></i>
                        <div class="form-group">
                        	<input type="text" id="cid_search" name="cid" class="form-control" placeholder="Procure por um CID ou por uma descrição do CID" value="<?php echo $candidato->get_cid(); ?>">
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">

                    <div class="col-md-4">
                        <label>História da Moléstia Atual/Pregressa: Intercorrência</label>
                        <div class="form-group">
                            <select class="form-control" name="historia_molestia">
                                <?php echo paciente_opcoes( $candidato->pegarOpcoesParaCamposDoPaciente('historia_molestia'), $candidato->get_historia_molestia() ); ?>
                            </select>
                        </div>
                    </div>

                    

                </div> <!-- row -->

                <div class="row">
                    <div class="col-md-12">
                        <label>Observações</label>
                        <div class="form-group">
                            <textarea name="historia_molestia_obs" class="form-control" rows="10" style="resize: vertical; "><?php echo $candidato->get_historia_molestia_obs(); ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="separador-style01"></div>

                <div class="row">

                    <div class="col-md-12">
                        <label>Patologias ou distúrbios associados</label>
                        <div class="form-group">
                            <textarea name="patol_disturbio_assoc" class="form-control" rows="5" style="resize: vertical; "><?php echo $candidato->get_patol_disturbio_assoc(); ?></textarea>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>
                
                <div class="row">

                    <div class="col-md-12">
                        <label>Medicamentos em uso / Motivo</label>
                        <div class="form-group">
                            <textarea name="medicamento_uso" class="form-control" rows="5" style="resize: vertical; "><?php echo $candidato->get_medicamento_uso(); ?></textarea>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>
                

                <div class="row">

                    <div class="col-md-12">
                        <label>Exames complementares</label>
                        <div class="form-group">
                            <textarea name="exames_complementares" class="form-control" rows="5" style="resize: vertical; "><?php echo $candidato->get_exames_complementares(); ?></textarea>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">

                    <div class="col-md-12">
                        <label>Órteses/Próteses e Adaptações</label>
                        <div class="form-group">
                            <textarea name="orteses_proteses_adaptacoes" class="form-control" rows="5" style="resize: vertical; "><?php echo $candidato->get_orteses_proteses_adaptacoes(); ?></textarea>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>
                
                <div class="row">

                    <div class="col-md-12">
                        <label>Características Sindrômicas</label>
                        <div class="form-group">
                            <textarea name="caracteristicas_sindromicas" class="form-control" rows="5" style="resize: vertical; "><?php echo $candidato->get_caracteristicas_sindromicas(); ?></textarea>
                        </div>
                    </div>

                </div> <!-- row -->


            </div> <!-- container -->
            
        </div> <!-- candidato-ver -->

        </form>

    </section> <!-- padrao-conteudo -->

</div> <!-- candidato -->

<?php $this->load->view('fixos/rodape'); ?>