<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="prontuario">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <?php if( session_visao( [1, 2, 4] ) ): ?>
                        <li>
                            <a href="<?php echo base_url('pacientes'); ?>">Pacientes</a>
                        </li>
                        <?php endif; ?>

                        <?php if( session_visao( [3] ) ): ?>
                        <li class="active">
                            Pacientes
                        </li>
                        <?php endif; ?>
                        <li>
                            <a href="<?php echo base_url('pacientes/consultar/' . $prontuario->get_paciente()->get_id_paciente() . '#prontuarios' ); ?>" class="text-capitalize">
                                <?php echo $prontuario->get_paciente()->get_nome_completo(); ?>
                            </a>
                        </li>
                        <li class="active">Prontuário</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3" id="opcoes_prontuario">
                    <div class="btn-group" role="group">
                        
                        <!-- Editar -->
                        <a class="btn btn-style06" href="<?php echo base_url('prontuarios/editar/' . $prontuario->get_id_prontuario() ); ?>">Editar</a>
                        <!-- Editar -->

                        <!-- Remover -->
                        <?php if( session_visao( [1,2] ) ): ?>
                        <button type="button" class="btn btn-style06" data-cancelar="#cancelar_prontuario_<?php echo $prontuario->get_id_prontuario(); ?>" data-container="#opcoes_prontuario" data-toggle="popover" data-placement="bottom" data-html="true" data-content='
                    
                            <span class="cor-texto-padrao-2">Você realmente deseja remover esse prontuario?</span> </br></br> 
                            <button class="btn btn-style06 pull-left" id="cancelar_prontuario_<?php echo $prontuario->get_id_prontuario(); ?>">Cancelar</button> 
                            <a class="btn btn-danger pull-right" href="<?php echo base_url('prontuarios/remover/' . $prontuario->get_id_prontuario() . '/' . $prontuario->get_paciente()->get_id_paciente() ); ?>">Remover</a>   
                            </br></br>

                        '>Remover</button>
                        <?php endif; ?>
                        <!-- Remover -->
                        
                        <!-- Voltar -->
                        <a class="btn btn-style06" href="<?php echo base_url('pacientes/consultar/' . $prontuario->get_paciente()->get_id_paciente() . '#prontuarios' ); ?>">Voltar</a>
                        <!-- Voltar -->
                        
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->

    </section> <!-- padrao-topo -->

    <section class="padrao-info">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="prontuario-criado-por">
                        <p>Vinculado ao Usuário: <span class="text-capitalize"><?php echo $prontuario->get_usuario()->get_nome_usuario(); ?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        <?php if($prontuario->get_bloqueado() == true): ?>
                            <div class="prontuario_bloqueado"><span class="label label-bloqueado">Bloqueado</span></div>
                        <?php else: ?>
                            <p><?php echo $prontuario->porcentoFeitoDoProntuario(); ?>% completado</p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- padrao-info -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo prontuario-consultar">
        <div class="container">

                <?php foreach($prontCampos['prontuario'] as $secao => $campos): ?>

                    <section class="grupo-pront">

                        <div class="row">

                            <div class="col-md-4">
                                <h4 class="title"><?= $secao; ?></h4>
                            </div> <!-- col-md-4 -->

                            <div class="col-md-8">

                                <?php foreach($campos as $campo => $specs): ?>

                                    <div class="form-group">
                                        <label><?= $campo; ?></label>
                                        <div class="form-group">
                                            <?php if( array_key_exists('opcao', $specs) ): ?>

                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <?php 
                                                            $opcoes = $prontuario->{'get_'.$specs['opcao']}();
                                                            $opcoes = ( !empty($opcoes) ) ? json_decode($opcoes, true) : [];
                                                            
                                                            if( isset($opcoes['obs']) ) {
                                                                $obs = $opcoes['obs'];
                                                                unset($opcoes['obs']);
                                                            } else {
                                                                $obs = null;
                                                            }
                                                            
                                                            if ( !empty($opcoes) ): ?>
                                                            <ul>
                                                                <?php foreach($opcoes as $index => $opcao): ?>
                                                                    <li><?= $opcao; ?></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                            
                                                        <?php endif; ?>
                                                    </div> <!-- panel-body -->
                                                    <?php if( $obs !== null ): ?>

                                                    <div class="panel-footer">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">Descrição</div>
                                                            <p class="form-control"><?= $obs; ?></p>
                                                        </div>
                                                    </div>
                                                    
                                                    <?php endif; ?>
                                                </div>

                                            <?php elseif( array_key_exists('texto', $specs) ): ?>

                                                <div class="panel panel-default">
                                                    <div class="panel-body sem-opcao">
                                                        <?php  echo $prontuario->{'get_'.$specs['texto']['nome']}(); ?>
                                                    </div>
                                                </div>

                                            <?php endif; ?>
                                        </div>
                                    </div> <!-- form-group -->

                                <?php endforeach; ?>




                                <?php if( $secao == 'Força Muscular' ): ?>

                                    <table class="table table-bordered">
                                        <thead style="text-align: center;">
                                            <tr>
                                                <td width="50%" colspan="2">Grupos Musculares</td>
                                                <td width="25%">D</td>
                                                <td width="25%">E</td>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td width="25%" style="text-align:center; vertical-align: middle;">MMSS</td>
                                            <td>
                                                <?php   $forca_muscular_mms_gm = array(
                                                            'name' => 'forca_muscular_mms_gm',
                                                            'id' => 'forca_muscular_mms_gm',
                                                            'value' => $prontuario->get_forca_muscular_mms_gm(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $forca_muscular_mms_gm ); ?>
                                            </td>
                                            <td>
                                                <?php   $forca_muscular_mms_d = array(
                                                            'name' => 'forca_muscular_mms_d',
                                                            'id' => 'forca_muscular_mms_d',
                                                            'value' => $prontuario->get_forca_muscular_mms_d(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $forca_muscular_mms_d ); ?>
                                            </td>
                                            <td>
                                                <?php   $forca_muscular_mms_e = array(
                                                            'name' => 'forca_muscular_mms_e',
                                                            'id' => 'forca_muscular_mms_e',
                                                            'value' => $prontuario->get_forca_muscular_mms_e(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $forca_muscular_mms_e ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="text-align:center; vertical-align: middle;">MMII</td>
                                            <td>
                                                <?php   $forca_muscular_mmii_gm = array(
                                                            'name' => 'forca_muscular_mmii_gm',
                                                            'id' => 'forca_muscular_mmii_gm',
                                                            'value' => $prontuario->get_forca_muscular_mmii_gm(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                    echo form_textarea( $forca_muscular_mmii_gm ); ?>
                                            </td>
                                            <td>
                                                <?php   $forca_muscular_mmii_d = array(
                                                            'name' => 'forca_muscular_mmii_d',
                                                            'id' => 'forca_muscular_mmii_d',
                                                            'value' => $prontuario->get_forca_muscular_mmii_d(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                    echo form_textarea( $forca_muscular_mmii_d ); ?>
                                            </td>
                                            <td>
                                                <?php   $forca_muscular_mmii_e = array(
                                                            'name' => 'forca_muscular_mmii_e',
                                                            'id' => 'forca_muscular_mmii_e',
                                                            'value' => $prontuario->get_forca_muscular_mmii_e(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $forca_muscular_mmii_e ); ?>
                                            </td>
                                        </tr>
                                    </table>





                                <?php elseif( $secao == 'Mecanismo Reflexo Postural' ): ?>




                                    <table class="table table-bordered">
                                        <thead style="text-align: center;">
                                            <tr>
                                                <td width="33.333333333%">Reações Posturais</td>
                                                <td width="33.333333333%">Postura Sentada</td>
                                                <td width="33.333333333%">Bípede</td>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td width="33.333333333%" style="font-size:15px; text-align:center; vertical-align: middle;">Reações de Endireitamento (A, I, C)</td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_endireitamento_postura_sentada = array(
                                                            'name' => 'mrp_reacoes_endireitamento_postura_sentada',
                                                            'id' => 'mrp_reacoes_endireitamento_postura_sentada',
                                                            'value' => $prontuario->get_mrp_reacoes_endireitamento_postura_sentada(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_endireitamento_postura_sentada ); ?>
                                            </td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_endireitamento_bipede = array(
                                                            'name' => 'mrp_reacoes_endireitamento_bipede',
                                                            'id' => 'mrp_reacoes_endireitamento_bipede',
                                                            'value' => $prontuario->get_mrp_reacoes_endireitamento_bipede(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_endireitamento_bipede ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33.333333333%" style="text-align:center; vertical-align: middle;">Reações de Equilibrio (A, I, C)</td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_equilibrio_postura_sentada = array(
                                                            'name' => 'mrp_reacoes_equilibrio_postura_sentada',
                                                            'id' => 'mrp_reacoes_equilibrio_postura_sentada',
                                                            'value' => $prontuario->get_mrp_reacoes_equilibrio_postura_sentada(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_equilibrio_postura_sentada ); ?>
                                            </td>
                                            <td width="33.333333333%">
                                                <?php $mrp_reacoes_equilibrio_bipede = array(
                                                        'name' => 'mrp_reacoes_equilibrio_bipede',
                                                        'id' => 'mrp_reacoes_equilibrio_bipede',
                                                        'value' => $prontuario->get_mrp_reacoes_equilibrio_bipede(),
                                                        'rows' => 3,
                                                        'class' => 'form-control'
                                                    );
                                                    echo form_textarea( $mrp_reacoes_equilibrio_bipede ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="33.333333333%" style="text-align:center; vertical-align: middle;">Reações de Proteção (A, P)</td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_protecao_postura_sentada = array(
                                                            'name' => 'mrp_reacoes_protecao_postura_sentada',
                                                            'id' => 'mrp_reacoes_protecao_postura_sentada',
                                                            'value' => $prontuario->get_mrp_reacoes_protecao_postura_sentada(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_protecao_postura_sentada ); ?>
                                            </td>
                                            <td width="33.333333333%">
                                                <?php   $mrp_reacoes_protecao_bipede = array(
                                                            'name' => 'mrp_reacoes_protecao_bipede',
                                                            'id' => 'mrp_reacoes_protecao_bipede',
                                                            'value' => $prontuario->get_mrp_reacoes_protecao_bipede(),
                                                            'rows' => 3,
                                                            'class' => 'form-control'
                                                        );
                                                        echo form_textarea( $mrp_reacoes_protecao_bipede ); ?>
                                            </td>
                                        </tr>
                                    </table>

                                <?php endif; ?>






                            </div> <!-- col-md-8 -->
                            
                        </div> <!-- row -->

                    </section> <!-- ortostatismo -->

                <?php endforeach; ?>

                <section class="grupo-pront">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="prontuario-metadata">
                                <ul class="datas">
                                    <li>Data de Criação: <?php echo $prontuario->get_data_criacao(); ?></li>
                                    <li> Ultima Modificação Feita: <?php echo $prontuario->get_data_atualizacao(); ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section> <!-- data de criacao e data de atualizacao -->

        </div> <!-- container -->
    </section> <!-- padrao-conteudo -->

</div> <!-- prontuario -->

<?php $this->load->view('fixos/rodape'); ?>