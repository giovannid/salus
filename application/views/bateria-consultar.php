<?php $this->load->view('fixos/cabecalho'); ?>  

<div class="estilo-padrao" id="bateria">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li class="active">Baterias</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        <a class="btn btn-style06" href="<?php echo base_url('baterias/cadastrar'); ?>">Nova Bateria</a>
                    </div>
                </div> <!-- cold-md-3 -->
                
            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>
    
    <section class="padrao-conteudo">
        
        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="padrao-lista" id="baterias-lista">

                        <?php if(!empty($baterias)): ?>

                            <div class="topo-lista-personalizada">
                                <span class="pull-left">Nome da Bateria</span>
                                <span class="pull-right">Ações</span>
                            </div>

                            <?php foreach($baterias as $bateria): ?>

                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div class="pull-left titulo-lista-personalizada">
                                            <span class="text-capitalize"><?php echo $bateria->get_nome_bateria();?></span>
                                            <div class="info-lista-personalizada">
                                                <ul>
                                                    <?php if( $bateria->get_bloqueado() == true ): ?>
                                                        <li>
                                                            <span class="label label-bloqueado">Finalizada</span>
                                                        </li>
                                                    <?php endif; ?>
                                                    <li>Data Início: <?= $bateria->get_data_inicio(); ?></li>
                                                    <li>Data Fim: <?= $bateria->get_data_fim(); ?></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="pull-right acoes-lista-personalizada">

                                            <div class="btn-group" role="group">
                                                <?php echo anchor(base_url('baterias/consultar').'/'.$bateria->get_id_bateria(), 'Ver', 'class="btn btn-style06"');?>
                                                <button type="button" 
                                                        class="btn btn-style06" 
                                                        data-cancelar="#cancelar_bateria_<?php echo $bateria->get_id_bateria(); ?>" 
                                                        data-container="#baterias-lista" 
                                                        data-toggle="popover" 
                                                        data-placement="bottom" 
                                                        data-html="true" 
                                                        data-content='

                                                        Você realmente deseja remover essa bateria? </br></br> 
                                                        
                                                        <button class="btn btn-style06 pull-left" 
                                                        id="cancelar_bateria_<?php echo $bateria->get_id_bateria(); ?>">Cancelar</button> 
                                                        
                                                        <a class="btn btn-danger pull-right" 
                                                        href="<?php echo base_url('baterias/remover/' . $bateria->get_id_bateria() ); ?>">Remover</a>   
                                                        </br></br>'>Remover
                                                </button>
                                            </div>
                                            
                                        </div> <!-- pull-right -->

                                    </div> <!-- panel-body -->
                                </div> <!-- panel-default -->

                            <?php endforeach ?>

                        <?php else: ?>

                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <span>Não existem baterias cadastradas.</span>

                                </div> <!-- panel-body -->
                            </div> <!-- panel-default -->

                        <?php endif; ?>

                    </div> <!-- baterias-lista -->

                </div> <!-- col-md-12 -->  

            </div> <!-- row -->
        </div> <!-- container -->

    </section> <!-- padrao-conteudo -->

</div> <!-- bateria -->

<?php $this->load->view('fixos/rodape'); ?>