<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="consulta">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url('consultas'); ?>">Consultas</a></li>
                        <li class="active">
                            Cadastrar Consulta
                        </li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        <a class="btn btn-style06" href="<?php echo base_url('consultas/index'); ?>">Voltar</a>
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

                <?php echo form_open('consultas/cadastrar',
                        array( 'id' => 'consultas_form_parte1', 'name' => 'consultas_form_parte1' ) ); ?>

                    <section class="grupo-pront consulta_horario">

                        <div class="row">
                            <div class="col-md-4">
                                <h4 class="title">Paciente</h4>
                                
                            </div>

                            <div class="col-md-8">

                                <div class="form-group">
                                    <label>Paciente</label>
                                        <select name="consulta_novo_paciente" class="form-control required" required>
                                            <option value="">Selecione um paciente</option>
                                            <?php foreach($pacientes as $index => $paciente): ?>
                                                <option value="<?= $paciente->get_id_paciente(); ?>"><?= $paciente->get_nome_completo(); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </select>
                                </div>

                                <button class="btn btn-style08" id="enviarCadastrarConsulta">Próximo</button>
                                
                            </div>
                        </div> <!-- row -->

                    </section>


                </form>

        </div> <!-- container -->
    </section> <!-- padrao-conteudo -->

</div> <!-- bateria -->

<?php $this->load->view('fixos/rodape'); ?>