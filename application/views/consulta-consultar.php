<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="consulta">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li class="active">Consultas</li>
                        <?php if(isset($paciente_nome) && isset($paciente_sel)): ?>
                        <li>
                            <a href="<?= base_url('pacientes/consultar/' . $paciente_sel . '#consultas' ); ?>"><?= $paciente_nome; ?></a>
                        </li>
                        <?php endif;?>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        <a class="btn btn-style06" href="<?php echo base_url('consultas/cadastrar'); ?>">Nova Consulta</a>
                    </div>
                </div> <!-- cold-md-3 -->
                
            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        
        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="filtrar-lista">

                        <div class="row">
                            <div class="col-md-3">
                            
                                <span class="titulo">Filtrar por Data</span>
                                <?= form_open('consultas/index'); ?>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="consulta_data" name="consulta_data" placeholder="<?= $dia_atual_data; ?>" value="<?= $dia_atual_data; ?>">
                                        <span class="input-group-btn">
                                            <input type="submit" name="consultas_data_submit" class="btn btn-style08" value="Ver" />
                                        </span>
                                    </div><!-- /input-group -->
                                </form>
                            </div> <!-- col-md-3 -->

                            <div class="col-md-3">
                            
                                <span class="titulo">Filtrar por Paciente</span>
                                <?= form_open('consultas/index'); ?>
                                    <div class="input-group">
                                        <select name="consulta_paciente" class="required form-control" required>

                                            <option value="">Selecione um paciente</option>
                                            <?php foreach($pacientes as $index => $paciente): ?>
                                                <option value="<?= $paciente->get_id_paciente(); ?>" <?= (isset($paciente_sel) && $paciente_sel == $paciente->get_id_paciente() ) ? 'selected=""' : ''; ?>>
                                                    <?= $paciente->get_nome_completo(); ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="input-group-btn">
                                            <input type="submit" name="consultas_data_submit" class="btn btn-style08" value="Ver" />
                                        </span>
                                    </div><!-- /input-group -->
                                </form>
                            </div> <!-- col-md-3 -->

                            <div class="col-md-2">
                                <a href="<?= base_url('consultas/index/0/0/todas'); ?>" class="btn btn-style08" style="margin-top: 32px;">Ver todas consultas</a>
                            </div>

                        </div> <!-- row --> 

                    </div> <!-- filtrar-lista -->

                    <div class="padrao-lista">

                        <?php if(!empty($consultas)): ?>

                            <?php foreach($consultas as $data => $consultas_itens): ?>

                                <div class="topo-lista-personalizada">
                                
                                    <div class="consulta-dia">
                                        <?php if( $dia_atual == ( new DateTime($data) ) ): ?>
                                            <span class="label label-consulta">Hoje</span>
                                        <?php endif; ?>
                                        <span><?= strftime('%e de %B', (new DateTime($data))->getTimestamp()); ?></span>
                                    </div>
                                        
                                </div>

                                <?php foreach($consultas_itens as $index => $consulta): ?>

                                    <div class="padrao-item">
                                        <div class="panel panel-default">
                                            <div class="panel-body">

                                                <div class="row">

                                                    <div class="col-md-5">

                                                        <div class="pull-left titulo-lista-personalizada">
                                                    
                                                            <div class="tipo_usuario">
                                                                <span class="label label-consulta">Consulta</span>
                                                            </div>
                                                            
                                                            <div class="nome_usuario">
                                                                <span class="text-capitalize"><?php echo $consulta->get_paciente()->get_nome_completo(); ?></span> 
                                                            </div>

                                                        </div> <!-- titulo-lista-personalizada -->

                                                        

                                                    </div> <!-- col-md-4 -->

                                                    <div class="col-md-5">

                                                        <div class="consulta-horas">

                                                            <span class="label label-hora">Início</span>
                                                            <span class="hora-inicio"><?= $consulta->get_hora_inicio(); ?></span>
                                                            <span class="glyphicon icone-direita glyphicon-arrow-right"></span>
                                                            <span class="hora-fim"><?= $consulta->get_hora_fim(); ?></span>
                                                            <span class="label label-hora">Fim</span>
                                                            
                                                        </div>

                                                    </div> <!-- col-md-6 -->

                                                    <div class="col-md-2">
                                                        <div class="pull-right acoes-lista-personalizada">

                                                            <div class="btn-group" role="group">
                                                                <a class="btn btn-style06" href="<?php echo base_url('/consultas/consultar/' . $consulta->get_id_consulta() ); ?>">Ver</a>
                                                                <a class="btn btn-style06" href="<?php echo base_url('/consultas/editar/' . $consulta->get_id_consulta() ); ?>">Editar</a>

                                                            </div> <!-- btn-group -->
                                                            
                                                        </div> <!-- acoes-lista-personalizada -->
                                                    </div> <!-- col-md-4 -->
                                                
                                                </div> <!-- row -->

                                            </div> <!-- panel-body -->

                                            <div class="panel-footer">
                                                <div class="padrao-lista-info">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            Consulta com: <span class="link-local"><?= $consulta->get_usuario()->get_nome_usuario(); ?></span>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php 
                                                            $bateria = $consulta->get_usuario()->get_bateria();
                                                            if($bateria !== null): ?>
                                                            Bateria: <span class="link-local text-capitalize"><?= $bateria->get_nome_bateria(); ?></span>
                                                            <?php endif; ?>

                                                        </div>
                                                    </div> <!-- row -->
                                                </div> <!-- candidato-info -->
                                            </div>

                                        </div> <!-- panel-default -->
                                    </div> <!-- padrao-item -->

                                <?php endforeach; ?>

                            <?php endforeach ?>

                        <?php else: ?>
                            
                                <div class="topo-lista-personalizada">
                                
                                    <div class="consulta-dia">
                                        <?php if( $dia_atual == $data_consulta ): ?>
                                            <span class="label label-consulta">Hoje</span>
                                        <?php endif; ?>
                                        <span><?= strftime('%e de %B', $data_consulta->getTimestamp()); ?></span>
                                    </div>
                                        
                                </div>

                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <span>Não existem consultas cadastrada para este dia ou para este paciente.</span>

                                </div> <!-- panel-body -->
                            </div> <!-- panel-default -->

                        <?php endif; ?>

                    </div> <!-- padrao-lista -->

                </div> <!-- col-md-12 -->  

            </div> <!-- row -->
        </div> <!-- container -->

    </section> <!-- padrao-conteudo -->

</div> <!-- consulta -->

<?php $this->load->view('fixos/rodape'); ?>