<section class="padrao-msg">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <?php if( isset($msg) && $msg == 'sucesso' ): ?>
                    <div class="alert alert-success alert-dismissible" role="alert" style="color: #3c763d;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo erro_msg($codigo); ?>
                    </div>
                <?php elseif( isset($msg) && $msg == 'erro' ): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert" style="color: #a94442; ">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo erro_msg($codigo); ?>
                    </div>
                <?php endif; ?>
            </div> <!-- col-md-12 -->

        </div> <!-- row -->
    </div> <!-- container -->
</section> <!-- padrao-msg -->