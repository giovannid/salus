<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/config.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/dynamic.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/dyform.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/bootstrap-datepicker.pt-BR.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.validate.msg.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery-ui.min.js"></script>

</body>
</html>