<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?= base_url(); ?>front/css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>front/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>front/css/base.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>front/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>front/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>front/css/jquery-ui.structure.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>front/css/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<!--[if IE]><link rel="shortcut icon" href="<?= base_url('favicon.ico'); ?>"><![endif]-->
<link rel="icon" href="<?= base_url('favicon.png'); ?>">
<link rel="apple-touch-icon-precomposed" href="<?= base_url('favicon-180x180.png'); ?>">

<title>Salus</title>
</head>
<body>
<div class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url('inicio'); ?>">
                <img src="<?= base_url('front/images/logo.png'); ?>" class="logo" width="50" height="39" alt="Salus">
                <span>Salus</span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?= base_url('inicio'); ?>" class="<?= pagina_ativa('inicio'); ?>">Início</a>
                </li>
                <?php if( session_visao( [1, 2, 4] ) ): ?>
                <li>
                    <a href="<?= base_url('candidatos');?>" class="<?= pagina_ativa('candidatos'); ?>">Candidatos</a>
                </li>
                <li>
                    <a href="<?= base_url('pacientes');?>" class="<?= pagina_ativa('pacientes'); ?>">Pacientes</a>
                </li>
                <li>
                    <a href="<?= base_url('consultas');?>" class="<?= pagina_ativa('consultas'); ?>">Consultas</a>
                </li>
                <?php endif; ?>
                <?php if( session_visao( [1, 2] ) ): ?>
                <li>
                    <a href="<?= base_url('baterias');?>" class="<?= pagina_ativa('baterias'); ?>">Baterias</a>
                </li>
                <?php endif; ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle <?= pagina_ativa('user'); ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <?= $this->session->usuario['nome_usuario']; ?> 
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php if( session_visao( [1, 2] ) ): ?>
                        <li><a href="<?= base_url('usuarios'); ?>">Usuários</a></li>
                        <li><a href="<?= base_url('configuracoes'); ?>">Configurações</a></li>
                        <li><a href="<?= base_url('configuracoes/relatorio'); ?>">Relatórios</a></li>
                        <li role="separator" class="divider"></li>
                        <?php endif; ?>
                        <?php if( session_visao( [4] ) ): ?>
                            <li><a href="<?= base_url('configuracoes/relatorio'); ?>">Relatórios</a></li>
                            <li role="separator" class="divider"></li>
                        <?php endif; ?>
                        <li><a href="<?= base_url('meu-perfil'); ?>">Meu Perfil</a></li>
                        <li><a href="<?= base_url('meu-perfil/trocar-senha'); ?>">Trocar Senha</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?= base_url('login/sair'); ?>">Sair</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>