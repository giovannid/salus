<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= base_url(); ?>front/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>front/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>front/css/base.css" rel="stylesheet" type="text/css">
    <!--[if IE]><link rel="shortcut icon" href="<?= base_url('favicon.ico'); ?>"><![endif]-->
    <link rel="icon" href="<?= base_url('favicon.png'); ?>">
    <link rel="apple-touch-icon-precomposed" href="<?= base_url('favicon-180x180.png'); ?>">
    <title>Salus</title>
</head>
<body>