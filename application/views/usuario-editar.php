<?php $this->load->view('fixos/cabecalho'); ?>

<div class="usuario estilo-padrao">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url('usuarios'); ?>">Usuários</a></li>
                        <li class="active text-capitalize">
                            <a href="<?php echo base_url('usuarios/consultar/' . $usuario->get_id_usuario() ); ?>"><?php echo $usuario->get_nome_usuario(); ?></a>
                        </li>
                        <li class="active">Editar Usuário</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">

                        <!-- Salvar -->
                        <button class="btn btn-style06" id="salvarUsuario">Salvar</button>
                        <!-- Salvar -->

                        <!-- Voltar -->
                        <a class="btn btn-style06" href="<?php echo base_url('usuarios/consultar/' . $usuario->get_id_usuario() ); ?>">Voltar</a>
                        <!-- Voltar -->
                        
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

            <?php  echo form_open('usuarios/editar_form/' . $usuario->get_id_usuario() ,
                    array( 'id' => 'usuarios_form', 'name' => 'usuarios_form' ) ); ?>

                <section class="grupo-pront">
                    <div class="row">

                        <div class="col-md-4">
                            <h4 class="title">Detalhes do Usuário</h4>
                        </div> <!-- col-md-4 -->

                        <div class="col-md-8">

                            <div class="form-group">
                                <label>Nome do Usuário</label>
                                <input type="text" name="editar_nome_usuario" class="form-control" value="<?php echo $usuario->get_nome_usuario(); ?>" required>
                            </div>

                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" name="editar_email" class="form-control" value="<?php echo $usuario->get_email(); ?>" required>
                            </div>

                            <div class="form-group">
                                <label>RA</label>
                                <input type="text" name="editar_ra" class="form-control" value="<?php echo $usuario->get_ra(); ?>" required>
                            </div>

                            <div class="form-group">
                                <label>Tipo usuário</label>
                                <select name="editar_tipo_usuario" class="form-control required" required>
                                    <option value="">Selecione uma opção</option>
                                    <option value="1" <?= ($usuario->get_tipo_usuario() == 1) ? 'selected' : ''; ?> >Administrador</option>
                                    <option value="2" <?= ($usuario->get_tipo_usuario() == 2) ? 'selected' : ''; ?> >Supervisor</option>
                                    <option value="3" <?= ($usuario->get_tipo_usuario() == 3) ? 'selected' : ''; ?> >Estagiário</option>
                                    <option value="4" <?= ($usuario->get_tipo_usuario() == 4) ? 'selected' : ''; ?> >Teclab</option>
                                    
                                </select>
                            </div>

                        </div> <!-- col-md-8 -->

                    </div> <!-- row -->
                </section> <!-- grupo-pront -->

            </form> <!-- usuarios_form -->

        </div> <!-- container -->
    </section> <!-- padrao-conteudo -->

</div> <!-- usuario -->

<?php $this->load->view('fixos/rodape'); ?>