<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="consulta"> 

    <section class="padrao-topo">
        <div class="container">
            <div class="row">
                
                <div class="col-md-9">
                    <ol class="breadcrumb">

                        <!-- Consultas -->
                        <?php if( session_visao( [1, 2, 4] ) ): ?>
                            <li><a href="<?= base_url('consultas'); ?>">Consultas</a></li>

                            <li>
                                <a href="<?= base_url('consultas/index/0/0/data/' . $consulta_data ); ?>"><?= strftime('%e de %B', (new DateTime($consulta_data))->getTimestamp()); ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if( session_visao( [3] ) ): ?>
                            <li><a href="<?= base_url('pacientes/consultar/' . $consulta->get_paciente()->get_id_paciente() . '#consultas' ); ?>">Consultas</a></li>
                            <li class="active"><?= strftime('%e de %B', (new DateTime($consulta_data))->getTimestamp()); ?></li>
                        <?php endif; ?>
                        
                        <li class="active text-capitalize">
                            <a href="<?= base_url('pacientes/consultar/' . $consulta->get_paciente()->get_id_paciente() . '#consultas' ); ?>"><?= $consulta->get_paciente()->get_nome_completo(); ?></a>
                        </li>
                    </ol>
                </div> <!-- cold-md-9 -->
                
                <div class="col-md-3" id="consulta">
                    <div class="btn-group" role="group">
                        <a class="btn btn-style06" href="<?= base_url( 'consultas/editar/' . $consulta->get_id_consulta() ); ?>">Editar</a>
                        <?php if( session_visao([1, 2, 4]) ): ?>
                        <button type="button" 
                                class="btn btn-style06" 
                                data-cancelar="#cancelar_consulta_<?php echo $consulta->get_id_consulta(); ?>" 
                                data-container="#consulta" 
                                data-toggle="popover" 
                                data-placement="bottom" 
                                data-html="true" 
                                data-content='
                                Você realmente deseja remover essa consulta? </br></br> 
                                
                                <button class="btn btn-style06 pull-left" 
                                id="cancelar_consulta_<?php echo $consulta->get_id_consulta(); ?>">Cancelar</button> 
                                
                                <a class="btn btn-danger pull-right" 
                                href="<?php echo base_url('consultas/remover/' . $consulta->get_id_consulta() ); ?>">Remover</a>   
                                </br></br>'>Remover
                        </button>
                        <?php endif; ?>

                         <!-- Botao voltar para admin, supervisor e teclab -->
                        <?php if( session_visao( [1, 2, 4] ) ): ?>
                            <a class="btn btn-style06" href="<?php echo base_url('consultas/index'); ?>">Voltar</a>
                        <?php endif; ?>

                        <!-- Botao voltar para estagiarios -->
                        <?php if( session_visao( [3] ) ): ?>
                            <a class="btn btn-style06" href="<?php echo base_url('pacientes/consultar/' . $consulta->get_paciente()->get_id_paciente() . '#consultas' ); ?>">Voltar</a>
                        <?php endif; ?>
                        
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container" >
        
            <section class="grupo-pront consulta_horario">

                <div class="row">
                    <div class="col-md-4">
                        <h4 class="title">Paciente</h4>
                        
                    </div>

                    <div class="col-md-8">

                        <div class="form-group">
                            <label>Paciente</label>
                                <div class="panel panel-default">
                                    <div class="panel-body text-capitalize">
                                        <?= $consulta->get_paciente()->get_nome_completo(); ?>
                                    </div> <!-- panel-body -->
                                </div>
                            </select>
                        </div>
                        
                    </div>
                </div> <!-- row -->

            </section>

            <section class="grupo-pront consulta_horario">

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="title">Detalhes da Consulta</h4>

                        
                        <div class="linha-form">
                            <div class="horario_consulta_topo">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label style="margin-left: -15px;">Data</label>
                                    </div>
                                    <div class="col-md-5">
                                        <label>Horário</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Consulta com</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="consulta_horario_item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <span class="table-content">
                                            <?= (new DateTime($consulta_data))->format("d/m/Y"); ?>
                                        </span>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="table-content">
                                            <span class="label label-hora">Início</span>
                                            <span class="hora-inicio"><?= $consulta->get_hora_inicio(); ?></span>
                                            <span class="glyphicon icone-direita glyphicon-arrow-right"></span>
                                            <span class="hora-fim"><?= $consulta->get_hora_fim(); ?></span>
                                            <span class="label label-hora">Fim</span>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        <span class="table-content"><?= $consulta->get_usuario()->get_nome_usuario(); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- linha-form -->
                        

                       <?php if(session_visao([1, 2, 3])): ?>
                        
                        <div class="row">

                            <div class="col-md-6">
                                <label>Observação da Consulta</label>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <?= $consulta->get_observacao_paciente(); ?>
                                    </div>
                                </div>
                            </div>

                                <?php if( session_visao([1,2]) ): ?>
                                    <div class="col-md-6">
                                        <label>Observação Supervisor</label>

                                         <div class="panel panel-default">
                                            <div class="panel-body">
                                                <?= $consulta->get_observacao_estagiario(); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                        </div> <!-- row --> 
                        
                        <div class="row">
                            <div class="col-md-6">
                                <label>Paciente Compareceu</label>

                                 <div class="panel panel-default">
                                    <div class="panel-body">
                                        <?php 
                                            if( $consulta->get_comparecimento_paciente() === null ) {
                                                echo 'Não Preenchido';
                                            } else {
                                                echo ( $consulta->get_comparecimento_paciente() == 1 ) ? 'Sim' : 'Não';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <?php if( session_visao([1,2]) ): ?>
                                    <label>Estágiario Compareceu</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <?php 
                                                if( $consulta->get_comparecimento_estagiario() === null ) {
                                                    echo 'Não Preenchido';
                                                } else {
                                                    echo ( $consulta->get_comparecimento_estagiario() == 1 ) ? 'Sim' : 'Não';
                                                }
                                            ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div> <!-- row -->

                        <div class="row">
                            <div class="col-md-6">
                                <?php if( $consulta->get_comparecimento_paciente() !== null && $consulta->get_comparecimento_paciente() == 0 ): ?>
                                <label>Justificação da Falta do Paciente</label>

                                 <div class="panel panel-default">
                                    <div class="panel-body">
                                        <?= $consulta->get_justi_paciente(); ?>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>

                            <div class="col-md-6">
                                <?php if( session_visao([1,2]) ): ?>
                                    <?php if( $consulta->get_comparecimento_estagiario() !== null && $consulta->get_comparecimento_estagiario() == 0 ): ?>
                                    <label>Justificação da Falta do Estágiario(a)</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <?= $consulta->get_justi_estagiario(); ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            
                        </div> <!-- row -->

                        <?php endif; ?>

                    </div>
                </div> <!-- row -->

            </section> <!-- consulta_horario -->
            
        </div> <!-- container -->
    </section> <!-- padrao-conteudo --> 

</div> <!-- bateria -->
<?php $this->load->view('fixos/rodape'); ?>