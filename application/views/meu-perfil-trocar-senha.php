<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="meu-perfil">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url('meu-perfil'); ?>">Meu Perfil</a>
                        </li>
                        <li class="active">Trocar Senha</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3" id="bateria">
                    <div class="btn-group" role="group">
                        <button class="btn btn-style06" id="salvarUsuario">Salvar</button>
                        <a class="btn btn-style06" href="<?php echo base_url('meu-perfil'); ?>">Voltar</a>
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

            <?php  echo form_open('meu-perfil/trocar-senha-form',
                    array( 'id' => 'usuarios_form', 'name' => 'usuarios_form' ) ); ?>

                <section class="grupo-pront">
                    <div class="row">

                        <div class="col-md-4">
                            <h4 class="title">Detalhes do Perfil</h4>
                        </div> <!-- col-md-4 -->

                        <div class="col-md-8">

                            <div class="form-group">
                                <label>Senha Atual</label>
                                <input type="password" name="senha-atual" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Nova Senha</label>
                                <input type="password" name="nova-senha" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Confirmar Nova Senha</label>
                                <input type="password" name="confirmar-nova-senha" class="form-control" required>
                            </div>

                        </div> <!-- col-md-8 -->

                    </div> <!-- row -->
                </section> <!-- grupo-pront -->

        </div> <!-- container -->
    </section> <!-- padrao-conteudo -->

</div> <!-- meu-perfil -->
<?php $this->load->view('fixos/rodape'); ?>