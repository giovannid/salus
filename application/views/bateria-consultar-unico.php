<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="bateria">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">
                
                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url('baterias'); ?>">Baterias</a></li>
                        <li class="active text-capitalize"><?php echo $bateria->get_nome_bateria(); ?></li>
                    </ol>
                </div> <!-- cold-md-9 -->
                
                <div class="col-md-3" id="bateria">
                    <div class="btn-group" role="group">

                        <!-- Editar -->
                        <a href="<?php echo base_url('baterias/editar/' . $bateria->get_id_bateria() ); ?>" class="btn btn-style06">Editar</a>

                        <!-- Remover -->
                        <button type="button" 
                                class="btn btn-style06" 
                                data-cancelar="#cancelar_bateria_<?php echo $bateria->get_id_bateria(); ?>" 
                                data-container="#bateria" 
                                data-toggle="popover" 
                                data-placement="bottom" 
                                data-html="true" 
                                data-content='
                                Você realmente deseja remover essa bateria? </br></br> 
                                
                                <button class="btn btn-style06 pull-left" 
                                id="cancelar_bateria_<?php echo $bateria->get_id_bateria(); ?>">Cancelar</button> 
                                
                                <a class="btn btn-danger pull-right" 
                                href="<?php echo base_url('baterias/remover/' . $bateria->get_id_bateria() ); ?>">Remover</a>   
                                </br></br>'>Remover
                        </button>

                        <!-- Voltar -->
                        <a class="btn btn-style06" href="<?php echo base_url('baterias/index'); ?>">Voltar</a>
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <section class="padrao-conteudo">
        <div class="container" >
           
            <section class="grupo-pront bateria-ver">

                <div class="row">
                    <div class="col-md-4">
                        <h4 class="title">Detalhes da Bateria</h4>
                    </div>

                    <div class="col-md-8">

                        <div class="form-group">
                            <label>Nome da Bateria:</label>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?php echo $bateria->get_nome_bateria(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Data Início</label>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?= $bateria->get_data_inicio(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Data Fim</label>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?= $bateria->get_data_fim(); ?>
                                </div>
                            </div>
                        </div>

                        <?php if( $bateria->get_bloqueado() == true ): ?>
                            <div class="form-group">
                                <label>Estado da Bateria</label>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <span class="label label-bloqueado">Finalizada</span>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <label>Usuários nesta Bateria</label>
                            <div class="panel panel-default users-bateria">
                                <div class="panel-body">
                                    <?php if(!empty($usuarios)): ?>
                                    <ul>
                                        <?php foreach($usuarios as $index => $usuario): ?>
                                            <li><?= $usuario->get_nome_usuario(); ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <?php else: ?>
                                        <span>Nenhum</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="info-lista-personalizada">
							<ul>
								<li>Data de Criação: <?php echo $bateria->get_data_criacao(); ?></li>
								<li> Ultima Modificação Feita: <?php echo $bateria->get_data_atualizacao(); ?></li>
							</ul>
						</div>

                    </div>
                </div> <!-- row -->

            </section> <!--bateria-ver -->
                
        </div> <!-- contianer -->
    </section> <!-- padrao-conteudo --> 

</div> <!-- bateria -->
<?php $this->load->view('fixos/rodape'); ?>