<?php $this->load->view('fixos/cabecalho'); ?>

<div class="usuario estilo-padrao" id="usuario">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url('usuarios'); ?>">Usuários</a></li>
                        <li class="active text-capitalize"><?php echo $usuario->get_nome_usuario(); ?></li>
                    </ol>
                </div> <!-- cold-md-9 -->
                
                <div class="col-md-3" id="usuario">
                    <div class="btn-group" role="group">
                        
                        <!-- Editar -->
                        <a href="<?php echo base_url('usuarios/editar/' . $usuario->get_id_usuario() ); ?>" class="btn btn-style06">Editar</a>
                        <!-- Editar -->

                        <!-- Remover -->
                        <button type="button" 
                                class="btn btn-style06" 
                                data-cancelar="#cancelar_usuario_<?php echo $usuario->get_id_usuario(); ?>" 
                                data-container="#usuario" 
                                data-toggle="popover" 
                                data-placement="bottom" 
                                data-html="true" 
                                data-content='
                                Você realmente deseja remover esse usuário? </br></br> 
                                
                                <button class="btn btn-style06 pull-left" 
                                id="cancelar_usuario_<?php echo $usuario->get_id_usuario(); ?>">Cancelar</button> 
                                
                                <a class="btn btn-danger pull-right" 
                                href="<?php echo base_url('usuarios/remover/' . $usuario->get_id_usuario() ); ?>">Remover</a>   
                                </br></br>'>Remover
                        </button>
                        <!-- Remover -->
                        
                        <!-- Voltar -->
                        <a class="btn btn-style06" href="<?php echo base_url('usuarios/index'); ?>">Voltar</a>
                        <!-- Voltar -->

                    </div> <!-- btn-group -->
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <section class="padrao-info">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                   
                    <ul class="list-inline lista-abas" role="tablist" id="abas_padrao">
                        <li role="presentation" class="active">
                            <a href="#informacoes" role="tab" data-toggle="tab">Informações</a>
                        </li>
                        <?php if( !in_array( $usuario->get_tipo_usuario(), [0, 4, 1] ) ): ?>

                            <?php if( $usuario->get_tipo_usuario() != 2 ): ?>
                            <li role="presentation">
                                <a href="#trocar-bateria" role="tab" data-toggle="tab">Trocar Bateria</a>
                            </li>
                            <?php endif; ?>
                        
                        <li role="presentation">
                            <a href="#vincular-paciente" role="tab" data-toggle="tab">Vincular Paciente(s)</a>
                        </li>
                        <?php endif; ?>
                    </ul> <!-- lista-abas -->

                </div> <!-- col-md-12 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-info -->

    <section class="padrao-msg">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <?php if( isset($msg) && $msg == 'sucesso' ): ?>
                        <div class="alert alert-success alert-dismissible" role="alert" style="color: #3c763d; margin-top: 10px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo erro_msg($codigo, $replace); ?>
                        </div>
                    <?php elseif( isset($msg) && $msg == 'erro' ): ?>
                        <div class="alert alert-danger alert-dismissible" role="alert" style="color: #a94442; margin-top: 10px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo erro_msg($codigo, $replace); ?>
                        </div>
                    <?php endif; ?>
                </div> <!-- col-md-12 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-msg -->

    <section class="padrao-conteudo">
        <div class="container">

        <div class="tab-content">

            <section role="tabpanel" class="tab-pane active grupo-pront"  id="informacoes">
                <div class="row">

                    <div class="col-md-4">
                        <h4 class="title">Detalhes do Usuário</h4>
                    </div> <!-- col-md-4 -->

                    <div class="col-md-8">

                        <div class="form-group">
                            <label>Nome do Usuário</label>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?php echo $usuario->get_nome_usuario(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>E-mail</label>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?php echo $usuario->get_email(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>RA</label>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?php echo $usuario->get_ra(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tipo de Usuário</label>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <span class="text-capitalize"><?php echo $usuario->nomeTipoUsuario(); ?></span>
                                </div>
                            </div>
                        </div>

                        <?php if( in_array( $usuario->get_tipo_usuario(), [3] ) ): ?>
                            <div class="form-group" id="registrado_bateria">
                                <label>Registrado na Bateria</label>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="info-lista-personalizada">
                                            <?php if( $usuario->get_bateria() != null ): ?>
                                            <ul>
                                                <li class="text-capitalize">Nome: <?php echo $usuario->get_bateria()->get_nome_bateria(); ?></li>
                                                <li>Data Início: <?php echo $usuario->get_bateria()->get_data_inicio(); ?></li>
                                                <li>Data Fim: <?php echo $usuario->get_bateria()->get_data_fim(); ?></li>
                                            </ul>
                                            <?php else: ?>
                                                Nenhuma
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <a href="<?= base_url('usuarios/trocar-senha/' . $usuario->get_id_usuario() ); ?>" class="btn btn-style06">Nova Senha Provisória</a>
                            <a href="#" data-toggle="modal" data-target="#faltasUsuario" class="btn btn-style06" style="margin-left: 10px;">Ver Faltas</a>
                        </div>

                        <div class="modal fade falta_modal" id="faltasUsuario" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Faltas</h4>
                                    </div>
                                    <div class="modal-body">
                                        <label>Usuário:</label>
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <?= $usuario->get_nome_usuario(); ?>
                                            </div>
                                        </div>

                                        <label>Quantidade de faltas: <?= count($faltas); ?></label>
                                        <?php if(!empty($faltas)): ?>
                                            <?php foreach($faltas as $index => $consulta): ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <b>Data:</b> <?= (new DateTime( $consulta->get_data() ))->format('d/m/Y'); ?>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <b>Paciente:</b> <?= $consulta->get_paciente()->get_nome_completo(); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-sm-10">
                                                                <div class="texto">
                                                                     <b>Justificativa:</b> <?= $consulta->get_justi_estagiario(); ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2 text-right">
                                                                <a href="<?= base_url('consultas/consultar/' . $consulta->get_id_consulta() ); ?>" class="btn btn-style06">Ver</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    Nenhuma
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                       
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-style08" data-dismiss="modal">Fechar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="info-lista-personalizada">
                            <ul>
                                <li>Data de Criação: <?php echo $usuario->get_data_criacao(); ?></li>
                                <li> Ultima Modificação Feita: <?php echo $usuario->get_data_atualizacao(); ?></li>
                            </ul>
                        </div>

                    </div> <!-- cold-md-8 -->

                </div> <!-- row -->
            </section> <!-- informacoes -->

            <?php if( !in_array( $usuario->get_tipo_usuario(), [4, 1] ) ): ?>

            <section role="tabpanel" class="tab-pane grupo-pront" id="trocar-bateria">

                <div class="row">

                    <div class="col-md-4">
                        <h4 class="title">Detalhes da Bateria</h4>
                    </div> <!-- col-md-4 -->

                    <div class="col-md-8">

                        <?php  echo form_open('usuarios/trocar-bateria/' . $usuario->get_id_usuario() ,
                        array( 'id' => 'usuarios_form', 'name' => 'usuarios_form') ); ?>

                            <div class="form-group">
                                <label>Baterias</label>

                                <div class="row">

                                    <div class="col-md-10">
                                        <select name="editar_id_bateria"  class="form-control">
                                            <optgroup label="Nenhuma">
                                                <option value="0">Nenhuma</option>
                                            </optgroup>

                                            <?php foreach( $baterias as $bateria ): ?>

                                                <optgroup label="Data Inicio: <?php echo $bateria->get_data_inicio(); ?> - Data Fim: <?php echo $bateria->get_data_fim(); ?>">
                                                    <option value="<?php echo $bateria->get_id_bateria(); ?>" <?= ( $usuario->get_bateria() instanceof Bateria && $usuario->get_bateria()->get_id_bateria() == $bateria->get_id_bateria() ) ? 'selected' : ''; ?> >
                                                        <?php echo ucwords( $bateria->get_nome_bateria() ); ?>
                                                    </option>
                                                </optgroup>

                                            <?php endforeach; ?>
                                        </select>
                                    </div> <!-- col-md-10 -->

                                    <div class="col-md-2">
                                        <button class="btn btn-style06" type="submit" id="salvarUsuario">Salvar</button>
                                    </div> <!-- col-md-2 -->
                                
                                </div> <!-- row -->
                            </div> <!-- form-group -->

                        </form> <!-- form -->

                    </div> <!-- cold-md-8 -->

                </div> <!-- row -->

            </section> <!-- troca-bateria -->

            <section role="tabpanel" class="tab-pane grupo-pront" id="vincular-paciente">

                <div class="row">

                    <div class="col-md-4">
                        <h4 class="title">Pacientes Vinculados</h4>
                    </div> <!-- col-md-4 -->

                    <div class="col-md-8">

                        <?php if( $usuario->get_bateria() != NULL || in_array( $usuario->get_tipo_usuario(), [2] ) ): ?>

                        <?php  echo form_open('usuarios/trocar-paciente/' . $usuario->get_id_usuario() ,
                        array( 'id' => 'usuario_pacientes_form', 'name' => 'usuario_pacientes_form') ); ?>

                            <div class="form-group">
                                <label>Pacientes</label>

                                <div class="row">
                                    <div class="col-md-10">
                                        <ul class="list-group" id="pacientes_vinculados">
                                            <?php if( !empty($prontuarios) ): ?>

                                                <?php foreach($prontuarios as $pront): ?>

                                                    <li class="list-group-item btn-style06 text-capitalize" data-idp="<?= $pront->get_paciente()->get_id_paciente(); ?>">
                                                        <?php echo $pront->get_paciente()->get_nome_completo(); ?>
                                                        <span class="botao_rm">Remover</span>
                                                    </li>

                                                    <input name="pacientes[]" value="<?= $pront->get_paciente()->get_id_paciente(); ?>" class="hidden" type="hidden">

                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <li class="list-group-item nenhum">Nenhum paciente vinculado</li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-10">

                                        <div class="form-group">

                                            <select name="lista_pacientes" id="lista_pacientes" class="form-control">
                                                <?php foreach( $pacientes as $paciente ): ?>

                                                    <option value="<?php echo $paciente->get_id_paciente(); ?>" >
                                                        <?php echo ucwords( $paciente->get_nome_completo() ); ?>
                                                    </option>

                                                <?php endforeach; ?>
                                            </select>

                                        </div>
                                    </div> <!-- col-md-10 -->

                                    <div class="col-md-2">
                                        <button class="btn btn-style06" id="adicionar_paciente">Adicionar</button>
                                    </div> <!-- col-md-2 -->
                                
                                </div> <!-- row -->

                            </div> <!-- form-group -->

                            <div class="form-group">
                                <button class="btn btn-style06" type="submit" id="salvarUsuarioPaciente">Salvar</button>
                            </div>

                        </form> <!-- form -->

                        <?php else: ?>

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?php echo erro_msg(34); ?>
                                </div>
                            </div>

                        <?php endif; ?>

                    </div> <!-- cold-md-8 -->

                </div> <!-- row -->
                
            </section> <!-- vincular-paciente -->

            <?php endif; ?>

        </div>
                
        </div> <!-- container -->
    </section> <!-- padrao-conteudo --> 

</div> <!-- usuario -->

<?php $this->load->view('fixos/rodape'); ?>