<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="paciente">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <?php if( session_visao( [1, 2, 4] ) ): ?>
                        <li>
                            <a href="<?php echo base_url('pacientes'); ?>">Pacientes</a>
                        </li>
                        <?php endif; ?>

                        <?php if( session_visao( [3] ) ): ?>
                        <li class="active">
                            Pacientes
                        </li>
                        <?php endif; ?>
                        <li class="active text-capitalize"><?php echo $paciente->get_nome_completo(); ?></li>
                    </ol>
                </div> <!-- cold-md-9 -->

                 <div class="col-md-3" id="opcoes_prontuario">
                    <div class="btn-group" role="group">

                        <?php if( session_visao( [1, 2, 4] ) ): ?>
                            <a class="btn btn-style06" href="<?php echo base_url('pacientes/editar/' . $paciente->get_id_paciente() ); ?>">Editar</a>

                            <button type="button" class="btn btn-style06" data-cancelar="#cancelar_paciente_<?php echo $paciente->get_id_paciente(); ?>" data-container="#opcoes_prontuario" data-toggle="popover" data-placement="bottom" data-html="true" data-content='

                                <span class="cor-texto-padrao-2">Você realmente deseja remover esse paciente?</span></br></br> 
                                <button class="btn btn-style06 pull-left" id="cancelar_paciente_<?php echo $paciente->get_id_paciente(); ?>">Cancelar</button> 
                                <a class="btn btn-danger pull-right" href="<?php echo base_url('pacientes/remover/' . $paciente->get_id_paciente() ); ?>">Remover</a>   
                                </br></br>

                            '>Remover</button>
                        <?php endif; ?>

                        <!-- Botao voltar para admin, supervisor e teclab -->
                        <?php if( session_visao( [1, 2, 4] ) ): ?>
                        <a class="btn btn-style06" href="<?php echo base_url('pacientes' ); ?>">Voltar</a>
                        <?php endif; ?>

                        <!-- Botao voltar para estagiarios -->
                        <?php if( session_visao( [3] ) ): ?>
                        <a class="btn btn-style06" href="<?php echo base_url('inicio' ); ?>">Voltar</a>
                        <?php endif; ?>

                    </div>
                </div> <!-- cold-md-3 -->
                
            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <section class="padrao-info">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                   
                    <ul class="list-inline lista-abas" role="tablist" id="abas_padrao">
                        <li role="presentation" class="active">
                            <a href="#informacoes" aria-controls="informacoes" role="tab" data-toggle="tab">Informações</a>
                        </li>
                        <li role="presentation">
                            <a href="#consultas" aria-controls="consultas" role="tab" data-toggle="tab">Consultas</a>
                        </li>
                        <?php if( session_visao( [1, 2, 3] ) ): ?>
                        <li role="presentation">
                            <a href="#prontuarios" aria-controls="prontuarios" role="tab" data-toggle="tab">Prontuários</a>
                        </li>
                        <?php endif; ?>
                    </ul> <!-- lista-abas -->

                </div> <!-- col-md-6 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-info -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">

        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="tab-content">

                        <section role="tabpanel" class="tab-pane padrao-conteudo-section" id="consultas">
                            
                            <div class="padrao-lista">

                                <?php if( session_visao([1,2,4]) ): ?> 
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="opcoes_topo">
                                            <span class="texto-style10">Opções</span>
                                            <div class="form-group">
                                                <a href="<?= base_url('consultas/cadastrar/0/0/' . $paciente->get_id_paciente() ); ?>" class="btn btn-style08">Nova Consulta</a>
                                                <a href="#" data-toggle="modal" data-target="#faltasPaciente" style="margin-left: 10px;" class="btn btn-style08">Ver Faltas</a>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- row -->

                                <div class="modal fade falta_modal" id="faltasPaciente" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Faltas</h4>
                                            </div>
                                            <div class="modal-body">
                                                <label>Paciente:</label>
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <?= $paciente->get_nome_completo(); ?>
                                                    </div>
                                                </div>

                                                <label>Quantidade de faltas: <?= count($faltas); ?></label>
                                                <?php if(!empty($faltas)): ?>
                                                    <?php foreach($faltas as $index => $consulta): ?>
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <b>Data:</b> <?= (new DateTime( $consulta->get_data() ))->format('d/m/Y'); ?>
                                                                    </div>
                                                                    <div class="col-sm-8">
                                                                        <b>Consulta com:</b> <?= $consulta->get_usuario()->get_nome_usuario(); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel-footer">
                                                                <div class="row">
                                                                    <div class="col-sm-10">
                                                                        <div class="texto">
                                                                            <b>Justificativa:</b> <?= $consulta->get_justi_paciente(); ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-2 text-right">
                                                                        <a href="<?= base_url('consultas/consultar/' . $consulta->get_id_consulta() ); ?>" class="btn btn-style06">Ver</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                        
                                                    <?php endforeach; ?>
                                                <?php else: ?>
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            Nenhuma
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-style08" data-dismiss="modal">Fechar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end-modal -->

                                <?php endif; ?>

                                <div class="row">
                                    <div class="col-md-12">

                                        <?php if(!empty($consultas)): ?>

                                            <?php foreach($consultas as $data => $consultas_itens): ?>

                                                <div class="topo-lista-personalizada">
                                                
                                                    <div class="consulta-dia">
                                                        <?php if( $dia_atual == ( new DateTime($data) ) ): ?>
                                                            <span class="label label-consulta">Hoje</span>
                                                        <?php endif; ?>
                                                        <span><?= strftime('%e de %B', (new DateTime($data))->getTimestamp()); ?></span>
                                                    </div>
                                                        
                                                </div>

                                                <?php foreach($consultas_itens as $index => $consulta): ?>

                                                    <div class="padrao-item">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">

                                                                <div class="row">

                                                                    <div class="col-md-5">

                                                                        <div class="pull-left titulo-lista-personalizada">
                                                                    
                                                                            <div class="tipo_usuario">
                                                                                <span class="label label-consulta">Consulta</span>
                                                                            </div>
                                                                            
                                                                            <div class="nome_usuario">
                                                                                <span class="text-capitalize"><?php echo $consulta->get_paciente()->get_nome_completo(); ?></span> 
                                                                            </div>

                                                                        </div> <!-- titulo-lista-personalizada -->

                                                                        

                                                                    </div> <!-- col-md-4 -->

                                                                    <div class="col-md-5">

                                                                        <div class="consulta-horas">

                                                                            <span class="label label-hora">Início</span>
                                                                            <span class="hora-inicio"><?= $consulta->get_hora_inicio(); ?></span>
                                                                            <span class="glyphicon icone-direita glyphicon-arrow-right"></span>
                                                                            <span class="hora-fim"><?= $consulta->get_hora_fim(); ?></span>
                                                                            <span class="label label-hora">Fim</span>
                                                                            
                                                                        </div>

                                                                    </div> <!-- col-md-6 -->

                                                                    <div class="col-md-2">
                                                                        <div class="pull-right acoes-lista-personalizada">

                                                                            <div class="btn-group" role="group">
                                                                                <a class="btn btn-style06" href="<?php echo base_url('/consultas/consultar/' . $consulta->get_id_consulta() ); ?>">Ver</a>
                                                                                <a class="btn btn-style06" href="<?php echo base_url('/consultas/editar/' . $consulta->get_id_consulta() ); ?>">Editar</a>

                                                                            </div> <!-- btn-group -->
                                                                            
                                                                        </div> <!-- acoes-lista-personalizada -->
                                                                    </div> <!-- col-md-4 -->
                                                                
                                                                </div> <!-- row -->

                                                            </div> <!-- panel-body -->

                                                            <div class="panel-footer">
                                                                <div class="padrao-lista-info">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            Consulta com: <span class="link-local"><?= $consulta->get_usuario()->get_nome_usuario(); ?></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <?php 
                                                                            $bateria = $consulta->get_usuario()->get_bateria();
                                                                            if($bateria !== null): ?>
                                                                            Bateria: <span class="link-local text-capitalize"><?= $bateria->get_nome_bateria(); ?></span>
                                                                            <?php endif; ?>

                                                                        </div>
                                                                    </div> <!-- row -->
                                                                </div> <!-- candidato-info -->
                                                            </div>

                                                        </div> <!-- panel-default -->
                                                    </div> <!-- padrao-item -->

                                                <?php endforeach; ?>

                                            <?php endforeach; ?>

                                        <?php else: ?>

                                            <div class="panel panel-default">
                                                <div class="panel-body">

                                                    <span>Não existem consultas cadastradas para este dia ou para este paciente.</span>

                                                </div> <!-- panel-body -->
                                            </div> <!-- panel-default -->

                                        <?php endif; ?>

                                    </div> <!-- col-md-12 -->
                                </div> <!-- row -->

                            </div> <!-- padrao-lista -->

                        </section> <!-- consultas -->

                        <section role="tabpanel" class="tab-pane padrao-conteudo-section" id="prontuarios">

                            <?php if( !empty($prontuarios) ): ?>

                                <div class="topo-lista-personalizada">
                                    <span class="pull-left">Nome do usuário</span>
                                    <span class="pull-right">Ações</span>
                                </div> <!-- topo-lista-personalizada -->

                                <div class="padrao-lista">

                                    <?php foreach($prontuarios as $prontuario): ?>

                                        <div class="padrao-item">
                                            <div class="panel panel-default">

                                                <div class="panel-body">

                                                    <div class="row">

                                                        <div class="col-xs-12 col-sm-7">
                                                            <div class="pull-left titulo-lista-personalizada">
                                                                <?php $diminuir_nome = 30; ?>
                                                                <?php if( $prontuario->get_bloqueado() == true ): ?>
                                                                    <div class="identificador_unico">
                                                                        <span class="label label-bloqueado">Bloqueado</span>
                                                                    </div>
                                                                    <?php $diminuir_nome = 21; ?>
                                                                <?php endif; ?>
                                                                <div class="identificador_unico">
                                                                    <span class="label label-id-pront">Prontuário ID <?php echo $prontuario->get_id_prontuario(); ?></span>
                                                                </div>
                                                                <div class="tipo_usuario">
                                                                    <?php echo $prontuario->get_usuario()->nomeTipoUsuario(); ?>
                                                                </div>
                                                                <div class="nome_usuario">
                                                                    <?php echo ( strlen($prontuario->get_usuario()->get_nome_usuario()) > 30 ) ? substr( $prontuario->get_usuario()->get_nome_usuario(), 0, $diminuir_nome ) . '...'  : $prontuario->get_usuario()->get_nome_usuario();  ?> 
                                                                </div>
                                                            </div> <!-- titulo-lista-personalizada -->
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2">
                                                            <div class="por-cento">
                                                                <?php echo $prontuario->porcentoFeitoDoProntuario(); ?>% completado
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-3">
                                                            <div class="pull-right acoes-lista-personalizada">

                                                                <div class="btn-group" role="group">

                                                                    <!-- Ver -->
                                                                    <a class="btn btn-style06" href="<?php echo base_url('/prontuarios/consultar/' . $prontuario->get_id_prontuario() ); ?>">Ver</a>
                                                                    <!-- Ver -->

                                                                    <!-- Remover -->
                                                                    <?php if( session_visao( [1,2] ) ): ?>
                                                                    <button type="button" class="btn btn-style06" data-cancelar="#cancelar_prontuario_<?php echo $prontuario->get_id_prontuario(); ?>" data-container="#prontuarios" data-toggle="popover" data-placement="bottom" data-html="true" data-content='
                                            
                                                                        <span class="cor-texto-padrao-2">Você realmente deseja remover esse prontuario?</span> </br></br> 
                                                                        <button class="btn btn-style06 pull-left" id="cancelar_prontuario_<?php echo $prontuario->get_id_prontuario(); ?>">Cancelar</button> 
                                                                        <a class="btn btn-danger pull-right" style="font-weight: 500;" href="<?php echo base_url('prontuarios/remover/' . $prontuario->get_id_prontuario() . '/' . $prontuario->get_paciente()->get_id_paciente() ); ?>">Remover</a>   
                                                                        </br></br>

                                                                    '>Remover</button>
                                                                    <?php endif; ?>
                                                                    <!-- Remover -->
                                                                    
                                                                </div> <!-- btn-group -->
                                                                
                                                            </div> <!-- acoes-lista-personalizada -->
                                                        </div>
                                                    
                                                    </div> <!-- row -->

                                                </div> <!-- panel-body -->

                                                
                                            </div> <!-- panel-default -->
                                        </div> <!-- padrao-item -->

                                    <?php endforeach; ?>

                                </div> <!-- padrao-lista -->

                            <?php else: ?>

                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <span>Não há prontuários cadastrados. Adicione um usuário a este paciente para que o prontuário seja criado. Você pode fazer isso indo na página de Usuários.</span>

                                    </div> <!-- panel-body -->
                                </div> <!-- panel-default -->

                            <?php endif; ?>

                        </section> <!-- prontuarios -->

                        <section role="tabpanel" class="tab-pane active padrao-conteudo-section" id="informacoes">

                            <div class="paciente-ver">
                                
                            <?php if( $paciente->get_desabilitado() == true ): ?>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-warning"><strong>Atenção!</strong> Paciente com alta</div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Alta: Observações</label>
                                        <div class="panel panel-default">
                                            <div class="panel-body text-capitalize">
                                                <?php echo $paciente->get_desabilitado_motivo(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="separador-style01"></div>				
                            
                            <?php endif; ?>
                            
                            <div class="row">
                    
                                <div class="col-md-7">
                                    <label>Nome Completo</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_nome_completo(); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <label>Idade</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?php echo $paciente->calcularIdade(); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <label>Data de Nascimento</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?= (new DateTime( $paciente->get_data_nascimento() ))->format("d/m/Y"); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <label>Sexo</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?php echo $paciente->get_sexo(); ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div> <!-- row -->

                            <div class="row">
                                
                                <div class="col-md-12">
                                    <label>Hipótese de Diagnóstico (HD)</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_hipotese_diagnostico(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>				

                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <label>Responsável 1</label>
                                        <div class="panel panel-default">
                                            <div class="panel-body text-capitalize">
                                                <?php echo $paciente->get_nome_responsavel(); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <label>Parentesco</label>
                                        <div class="panel panel-default">
                                            <div class="panel-body text-capitalize text-center">
                                                <?php echo $paciente->get_parentesco_resp(); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <label>Telefone</label>
                                        <div class="panel panel-default">
                                            <div class="panel-body text-capitalize">
                                                <?php echo $paciente->get_telefone(); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <label>Celular</label>
                                        <div class="panel panel-default">
                                            <div class="panel-body text-capitalize">
                                                <?php echo $paciente->get_celular(); ?>
                                            </div>
                                        </div>
                                    </div>

                                </div> <!-- row -->

                            <div class="separador-style01"></div>

                            <div class="row">
                                
                                <div class="col-md-7">
                                    <label>Endereço</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_endereco(); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <label>Queixa Principal</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_queixa_principal(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>

                            <div class="row">
                                
                                <div class="col-md-12">
                                    <label>Defict Funcional</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_deficit_funcional(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="row">
                                
                                <div class="col-md-6">
                                    <label>Solicitação de acompanhamento de FT Motora - Médico(a) Resonsável</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_medico_resp(); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Hospital de procedência</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_hospital_procedencia(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="row">

                                <div class="col-md-12">
                                    <label>Tratamento prévios / cirurgias</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_tratamento_previo(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>

                            <div class="row">

                                <div class="col-md-1">
                                    <label>ADNPM</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?php echo $paciente->pegarOpcaoUnica( $paciente->get_adnpm() ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-11">
                                    <label>ADNPM Motivo</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_adnpm_motivo(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="row">

                                <div class="col-md-2">
                                    <label>Síndrome de Down</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?php echo $paciente->pegarOpcaoUnica( $paciente->get_sindrome_de_down() ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <label>Mielomeningocele</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?php echo $paciente->pegarOpcaoUnica( $paciente->get_paralisia_braquial() ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label>Paralisia Braquial Congênita</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?php echo $paciente->pegarOpcaoUnica( $paciente->get_mielo() ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <label>Outras Síndromes</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_outras_sindromes(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="row">

                                <div class="col-md-3">
                                    <label class="texto-pequeno">Encefalopatia Crônica infantil não Progressiva</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?php echo $paciente->pegarOpcaoUnica( $paciente->get_encefalopatia() ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label>Classificação Topográfica</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize text-center">
                                            <?php echo $paciente->pegarOpcaoUnica( $paciente->get_class_topografia() ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Classificação clínica</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_class_clinica(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="row">

                                <div class="col-md-2">
                                    <label>Nivel</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->pegarOpcaoUnica( $paciente->get_nivel() ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <label>GMFCS nivel</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_gmfcs_nivel(); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <label>CID</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_cid(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>

                            <div class="row">

                                <div class="col-md-3">
                                    <label>História da Moléstia Atual/Pregressa</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->pegarOpcaoUnica( $paciente->get_historia_molestia() ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <label>Observações</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_historia_molestia_obs(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>

                            <div class="row">

                                <div class="col-md-12">
                                    <label>Patologias ou distúrbios associados</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_patol_disturbio_assoc(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>
                            
                            <div class="row">

                                <div class="col-md-12">
                                    <label>Medicamentos em uso / Motivo</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_medicamento_uso(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>
                            

                            <div class="row">

                                <div class="col-md-12">
                                    <label>Exames complementares</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_exames_complementares(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>

                            <div class="row">

                                <div class="col-md-12">
                                    <label>Órteses/Próteses e Adaptações</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_orteses_proteses_adaptacoes(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            <div class="separador-style01"></div>
                            
                            <div class="row">

                                <div class="col-md-12">
                                    <label>Características Sindrômicas</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?php echo $paciente->get_caracteristicas_sindromicas(); ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- row -->

                            </div> <!-- paciente-ver -->

                        </section> <!-- informacoes -->

                    </div> <!-- tab-content -->

                </div> <!-- col-md-12 -->

            </div> <!-- row -->
        </div> <!-- container -->

    </section> <!-- padrao-conteudo -->

</div> <!-- paciente -->

<?php $this->load->view('fixos/rodape'); ?>
