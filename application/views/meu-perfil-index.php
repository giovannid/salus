<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="meu-perfil">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li class="active">Meu Perfil</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3" id="bateria">
                    <div class="btn-group" role="group">
                        <a class="btn btn-style06" href="<?php echo base_url('meu-perfil/editar'); ?>">Editar</a>
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <section class="padrao-conteudo">
		<div class="container">
		   
			<section class="grupo-pront">
				<div class="row">

					<div class="col-md-4">
						<h4 class="title">Detalhes do Perfil</h4>
					</div> <!-- col-md-4 -->

					<div class="col-md-8">

						<div class="form-group">
							<label>Nome do Usuário</label>
							<div class="panel panel-default">
								<div class="panel-body">
									<?php echo $usuario->get_nome_usuario(); ?>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>E-mail</label>
							<div class="panel panel-default">
								<div class="panel-body">
									<?php echo $usuario->get_email(); ?>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>RA</label>
							<div class="panel panel-default">
								<div class="panel-body">
									<?php echo $usuario->get_ra(); ?>
								</div>
							</div>
						</div>

						<?php if( !in_array( $usuario->get_tipo_usuario(), [0, 4, 1] ) ): ?>

						<div class="form-group">
							<label>Registrado na Bateria</label>
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="info-lista-personalizada">
										<?php if( $usuario->get_bateria() != null ): ?>
										<ul>
											<li class="text-capitalize">Nome: <?php echo $usuario->get_bateria()->get_nome_bateria(); ?></li>
											<li>Data Início: <?php echo $usuario->get_bateria()->get_data_inicio(); ?></li>
											<li>Data Fim: <?php echo $usuario->get_bateria()->get_data_fim(); ?></li>
										</ul>
										<?php else: ?>
											N/A
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>

						<?php endif; ?>

						<div class="form-group">
							<a 	href="<?php echo base_url('meu-perfil/trocar-senha'); ?>" class="btn btn-style06" >Trocar Senha</a>
						</div>

					</div> <!-- cold-md-8 -->

				</div> <!-- row -->
			</section> <!-- grupo-pront -->
				
		</div> <!-- contianer -->
	</section> <!-- padrao-conteudo --> 

</div> <!-- meu-perfil -->

<?php $this->load->view('fixos/rodape'); ?>