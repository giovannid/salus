<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="candidato">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url('candidatos'); ?>">Candidatos</a>	
                        </li>
                        <li class="active text-capitalize"><?php echo $candidato->get_nome_completo(); ?></li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        
                        <!-- Editar -->
                        <a href="<?php echo base_url('candidatos/editar/' . $candidato->get_id_paciente() ); ?>" class="btn btn-style06">Editar</a>
                        <!-- Editar -->

                        <!-- Remover -->
                        <button type="button" 
                                class="btn btn-style06" 
                                data-cancelar="#cancelar_usuario_<?php echo $candidato->get_id_paciente(); ?>" 
                                data-container="#candidato" 
                                data-toggle="popover" 
                                data-placement="bottom" 
                                data-html="true" 
                                data-content='
                                Você realmente deseja remover esse candidato? </br></br> 
                                
                                <button class="btn btn-style06 pull-left" 
                                id="cancelar_usuario_<?php echo $candidato->get_id_paciente(); ?>">Cancelar</button> 
                                
                                <a class="btn btn-danger pull-right" 
                                href="<?php echo base_url('candidatos/remover/' . $candidato->get_id_paciente() ); ?>">Remover</a>   
                                </br></br>'>Remover
                        </button>
                        <!-- Remover -->
                        
                        <!-- Voltar -->
                        <a class="btn btn-style06" href="<?php echo base_url('candidatos/index'); ?>">Voltar</a>
                        <!-- Voltar -->

                    </div> <!-- btn-group -->
                </div> <!-- cold-md-3 -->
                
            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <section class="padrao-info">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                   
                    <ul class="list-inline lista-abas" role="tablist" id="abas_padrao">
                        <li role="presentation" class="active">
                            <a href="#" aria-controls="informacoes" role="tab" data-toggle="tab">Informações</a>
                        </li>
                    </ul> <!-- lista-abas -->

                </div> <!-- col-md-12 -->

            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-info -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">

        <div class="container">
        
            <div class="candidato-ver">
                
                <div class="row">
                    
                    <div class="col-md-7">
                        <label>Nome Completo</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_nome_completo(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Idade</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize text-center">
                                <?php echo $candidato->calcularIdade(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Data de Nascimento</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize text-center">
                                <?= (new DateTime( $candidato->get_data_nascimento() ))->format("d/m/Y"); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <label>Sexo</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize text-center">
                                <?php echo $candidato->get_sexo(); ?>
                            </div>
                        </div>
                    </div>

                    
                </div> <!-- row -->

                <div class="row">
                    
                    <div class="col-md-12">
                        <label>Hipótese de Diagnóstico (HD)</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_hipotese_diagnostico(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>				

                    <div class="row">
                        
                        <div class="col-md-6">
                            <label>Responsável 1</label>
                            <div class="panel panel-default">
                                <div class="panel-body text-capitalize">
                                    <?php echo $candidato->get_nome_responsavel(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label>Parentesco</label>
                            <div class="panel panel-default">
                                <div class="panel-body text-capitalize text-center">
                                    <?php echo $candidato->get_parentesco_resp(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label>Telefone</label>
                            <div class="panel panel-default">
                                <div class="panel-body text-capitalize">
                                    <?php echo $candidato->get_telefone(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label>Celular</label>
                            <div class="panel panel-default">
                                <div class="panel-body text-capitalize">
                                    <?php echo $candidato->get_celular(); ?>
                                </div>
                            </div>
                        </div>

                    </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">
                    
                    <div class="col-md-7">
                        <label>Endereço</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_endereco(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <label>Queixa Principal</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_queixa_principal(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">
                    
                    <div class="col-md-12">
                        <label>Defict Funcional</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_deficit_funcional(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">
                    
                    <div class="col-md-6">
                        <label>Solicitação de acompanhamento de FT Motora - Médico(a) Resonsável</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_medico_resp(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label>Hospital de procedência</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_hospital_procedencia(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">

                    <div class="col-md-12">
                        <label>Tratamento prévios / cirurgias</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_tratamento_previo(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">

                    <div class="col-md-2">
                        <label>ADNPM</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize text-center">
                                <?php echo $candidato->pegarOpcaoUnica( $candidato->get_adnpm() ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-10">
                        <label>ADNPM Motivo</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_adnpm_motivo(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">

                    <div class="col-md-2">
                        <label>Síndrome de Down</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize text-center">
                                <?php echo $candidato->pegarOpcaoUnica( $candidato->get_sindrome_de_down() ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>Mielomeningocele</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize text-center">
                                <?php echo $candidato->pegarOpcaoUnica( $candidato->get_paralisia_braquial() ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <label>Paralisia Braquial Congênita</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize text-center">
                                <?php echo $candidato->pegarOpcaoUnica( $candidato->get_mielo() ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <label>Outras Síndromes</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize text-center">
                                <?php echo $candidato->get_outras_sindromes(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">

                    <div class="col-md-3">
                        <label class="texto-pequeno">Encefalopatia Crônica infantil não Progressiva</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->pegarOpcaoUnica( $candidato->get_encefalopatia() ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <label>Classificação Topográfica</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->pegarOpcaoUnica( $candidato->get_class_topografia() ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label>Classificação clínica</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_class_clinica(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="row">

                    <div class="col-md-2">
                        <label>Nivel</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->pegarOpcaoUnica( $candidato->get_nivel() ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>GMFCS nivel</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_gmfcs_nivel(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <label>CID</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_cid(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">

                    <div class="col-md-3">
                        <label>História da Moléstia Atual/Pregressa</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->pegarOpcaoUnica( $candidato->get_historia_molestia() ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9">
                        <label>Observações</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_historia_molestia_obs(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">

                    <div class="col-md-12">
                        <label>Patologias ou distúrbios associados</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_patol_disturbio_assoc(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>
                
                <div class="row">

                    <div class="col-md-12">
                        <label>Medicamentos em uso / Motivo</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_medicamento_uso(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>
                

                <div class="row">

                    <div class="col-md-12">
                        <label>Exames complementares</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_exames_complementares(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>

                <div class="row">

                    <div class="col-md-12">
                        <label>Órteses/Próteses e Adaptações</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_orteses_proteses_adaptacoes(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

                <div class="separador-style01"></div>
                
                <div class="row">

                    <div class="col-md-12">
                        <label>Características Sindrômicas</label>
                        <div class="panel panel-default">
                            <div class="panel-body text-capitalize">
                                <?php echo $candidato->get_caracteristicas_sindromicas(); ?>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->
            
            </div> <!-- candidato-ver -->

            <section class="grupo-pront informacao-de-datas">
                <div class="row">
                    <div class="col-md-12">
                        <div class="info-lista-personalizada">
                            <ul>
                                <li>Data de Criação: <?php echo $candidato->get_data_criacao(); ?></li>
                                <li> Ultima Modificação Feita: <?php echo $candidato->get_data_atualizacao(); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section> <!-- data de criacao e data de atualizacao -->

        </div> <!-- container -->

    </section> <!-- padrao-conteudo -->

</div>

<?php $this->load->view('fixos/rodape'); ?>