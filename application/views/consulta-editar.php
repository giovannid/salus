<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="consulta">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <!-- Consultas -->
						<?php if( session_visao( [1, 2, 4] ) ): ?>
							<li><a href="<?= base_url('consultas'); ?>">Consultas</a></li>

							<li>
								<a href="<?= base_url('consultas/index/0/0/data/' . $consulta_data ); ?>"><?= strftime('%e de %B', (new DateTime($consulta_data))->getTimestamp()); ?></a>
							</li>
						<?php endif; ?>
                        <?php if( session_visao( [3] ) ): ?>
							<li><a href="<?= base_url('pacientes/consultar/' . $consulta->get_paciente()->get_id_paciente() . '#consultas' ); ?>">Consultas</a></li>
							<li class="active"><?= strftime('%e de %B', (new DateTime($consulta_data))->getTimestamp()); ?></li>
						<?php endif; ?>

						<li class="text-capitalize">
                            <a href="<?= base_url('pacientes/consultar/' . $consulta->get_paciente()->get_id_paciente() . '#consultas' ); ?>"><?= $consulta->get_paciente()->get_nome_completo(); ?></li></a>
                        <li class="active">
                            Editar Consulta
                        </li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3" id="consulta">
                    <div class="btn-group" role="group">
                        <button class="btn btn-style06" id="salvarConsulta">Salvar</button>
                        <a class="btn btn-style06" href="<?php echo base_url('consultas/consultar/' . $consulta->get_id_consulta() ); ?>">Voltar</a>
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

            <?php echo form_open('consultas/editar_form/' . $consulta->get_id_consulta(),
                    array( 'id' => 'consultas_form', 'name' => 'consultas_form' ) ); ?>

                    <section class="grupo-pront consulta_horario">

                        <div class="row">
                            <div class="col-md-4">
                                <h4 class="title">Paciente</h4>
                                
                            </div>

                            <div class="col-md-8">

                                <div class="form-group">
                                    <label>Paciente</label>
                                    <div class="panel panel-default">
                                        <div class="panel-body text-capitalize">
                                            <?= $consulta->get_paciente()->get_nome_completo(); ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div> <!-- col-md-8 -->
                        </div> <!-- row -->

                    </section>

                    <section class="grupo-pront consulta_horario">

                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="title">Detalhes da Consulta</h4>

                                
                                <div class="linha-form">
                                    <div class="horario_consulta_topo">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label style="margin-left: -15px;">Data</label>
                                            </div>
                                            <div class="col-md-5">
                                                <label>Horário</label>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Estágiario/Supervisor</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="consulta_horario_item">
                                        <div class="row">
                                            <div class="col-md-4">

                                                <?php if( $consulta_passada == false && session_visao([1, 2, 4]) ): ?>
                                                    <input required type="text" name="consulta_editar_data" class="form-control consulta_horario_campo_data" placeholder="Data" value="<?= (new DateTime($consulta->get_data()))->format("d/m/Y"); ?>">
                                                <?php else: ?>
                                                    <span class="table-content"><?= (new DateTime($consulta->get_data()))->format("d/m/Y"); ?></span>
                                                <?php endif; ?>

                                            </div>
                                            <div class="col-md-4">

                                                <?php if( $consulta_passada == false && session_visao([1, 2, 4]) ): ?>

                                                    <select name="consulta_editar_horario" class="form-control required" required>
                                                        <option value="">Selecione um horário</option>
                                                        <?php $contador = 1; foreach($horarios as $id => $hora): ?>
                                                            <optgroup label="Horario <?= $contador; ?>">    
                                                                <option value="<?= $id; ?>" <?= ( $id == $this->consulta->pegarIdHorario( $consulta->get_hora_inicio(), $consulta->get_hora_fim() ) ) ? 'selected=""' : ''; ?>>Hora Inicio: <?= $hora[0]; ?> - Hora Fim: <?= $hora[1]; ?></option>
                                                            </optgroup>
                                                        <?php $contador++; endforeach; ?>
                                                    </select>

                                                <?php else: ?>

                                                        <div class="table-content">
                                                            <span class="label label-hora">Início</span>
                                                            <span class="hora-inicio"><?= $consulta->get_hora_inicio(); ?></span>
                                                            <span class="glyphicon icone-direita glyphicon-arrow-right"></span>
                                                            <span class="hora-fim"><?= $consulta->get_hora_fim(); ?></span>
                                                            <span class="label label-hora">Fim</span>
                                                        </div>
                                                        
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-3">

                                                <?php if($consulta_passada == false && session_visao([1, 2, 4]) ): ?>

                                                    <select name="consulta_editar_usuario" class="form-control required" required>
                                                        <option value="">Selecione um usuário</option>
                                                        <?php foreach($usuarios as $index => $usuario): ?>
                                                            <option value="<?= $usuario->get_id_usuario(); ?>" <?= ( $usuario->get_id_usuario() == $consulta->get_usuario()->get_id_usuario() ) ? 'selected=""' : ''; ?>><?= $usuario->get_nome_usuario(); ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                
                                                 <?php else: ?>
                                                 
                                                    <span class="table-content"><?= $consulta->get_usuario()->get_nome_usuario(); ?></span>

                                                 <?php endif; ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- linha-form -->

                                <?php if(session_visao([1, 2, 3])): ?>

                                <div class="row">

                                    <div class="col-md-6">
                                        <label>Observação da Consulta</label>
                                        <?= form_textarea(['name' => 'consulta_editar_obs_paciente'], $consulta->get_observacao_paciente(), 'class="form-control"'); ?>
                                    </div>

                                    <?php if( session_visao([1,2]) ): ?>
                                        <div class="col-md-6">
                                            <label>Observação Supervisor</label>
                                            <?= form_textarea(['name' => 'consulta_editar_user_paciente'], $consulta->get_observacao_estagiario(), 'class="form-control"'); ?>
                                        </div>
                                    <?php endif; ?>

                                </div> <!-- row --> 
                                </br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Paciente Compareceu</label>
                                        <?= form_dropdown('consulta_editar_paciente_compareceu', [1 => 'Sim',  0 => 'Não' ], $consulta->get_comparecimento_paciente(), 'id="consulta_editar_paciente_compareceu" class="form-control"'); ?>

                                        <br>
                                        <div id="consulta_editar_justificacao_paciente" style="display:<?= ($consulta->get_comparecimento_paciente() == '0') ? 'unset' : 'none'; ?>;">
                                            <label>Justificação da Falta do Paciente</label>
                                            <input type="text" name="consulta_editar_justificacao_paciente" value="<?= $consulta->get_justi_paciente(); ?>" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <?php if( session_visao([1,2]) ): ?>
                                            <label>Estágiario(a) Compareceu</label>
                                            <?= form_dropdown('consulta_editar_usuario_compareceu', [1 => 'Sim', 0 => 'Não'], $consulta->get_comparecimento_estagiario(), 'id="consulta_editar_usuario_compareceu" class="form-control"'); ?>

                                            <br>
                                            <div id="consulta_editar_justificacao_usuario" style="display:<?= ($consulta->get_comparecimento_estagiario() == '0') ? 'unset' : 'none'; ?>;">
                                                <label>Justificação da Falta do Estágiario(a)</label>
                                                <input type="text" name="consulta_editar_justificacao_usuario" value="<?= $consulta->get_justi_estagiario(); ?>" class="form-control">
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div> <!-- row -->

                                <?php endif; ?>

                            </div>
                        </div> <!-- row -->

                    </section> <!-- consulta_horario -->


            </form>

        </div> <!-- container -->
    </section> <!-- padrao-conteudo -->

</div> <!-- bateria -->

<?php $this->load->view('fixos/rodape'); ?>