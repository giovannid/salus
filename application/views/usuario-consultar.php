<?php $this->load->view('fixos/cabecalho'); ?>

<div class="usuario estilo-padrao" id="usuario">

	<section class="padrao-topo">
		<div class="container">
			<div class="row">

				<div class="col-md-9">
					<ol class="breadcrumb">
						<li class="active">Usuários</li>
					</ol>
				</div> <!-- cold-md-9 -->

				<div class="col-md-3">
					<div class="btn-group" role="group">
						<a class="btn btn-style06" href="<?php echo base_url('usuarios/cadastrar'); ?>">Novo Usuário</a>
					</div>
				</div> <!-- cold-md-3 -->
				
			</div> <!-- row -->
		</div> <!-- container -->
	</section> <!-- padrao-topo -->

	<?php $this->load->view('fixos/mensagem'); ?>

	<section class="padrao-conteudo">

		<div class="container">
			<div class="row">

				<div class="col-md-12">

					<div class="row">
                        
                        <div class="col-md-12">
                            <div class="buscar-usuario">
                                <?= form_open('usuarios/buscar', ['name' => 'buscar_usuario']); ?>
                                    <div class="form-group">
                                        <span class="texto-style10">Buscar</span>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="info"  placeholder="Digite o nome de um usuário ou um email" <?= (isset($buscando_por)) ? 'value="' . $buscando_por . '"' : ''; ?>>
                                            <span class="input-group-btn">
                                                <input class="btn btn-style08" type="submit" value="Buscar">
                                            </span>
                                        </div><!-- /input-group -->
                                        
                                    </div>
                                </form>
                            </div>
                        </div> <!-- col-md-9 -->

                    </div> <!-- row -->

					<div class="padrao-lista" id="usuarios-lista">

						<?php if( !empty($usuarios) || isset($buscando_por) ): ?>
                        
                            <?php if(isset($buscando_por)): ?>
                            <div class="buscando_por">
                                <span>Buscando por: <?= $buscando_por; ?></span>

                                <?php if(empty($usuarios)): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            Não foi encontrado nenhum usuário
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <?php else: ?>
                            
                            <div class="topo-lista-personalizada">
								<span class="pull-left">Nome do usuário</span>
								<span class="pull-right">Ações</span>
							</div>

                            <?php endif; ?>

						<?php endif; ?>

						<?php foreach($usuarios as $usuario): ?>

							<div class="padrao-item">
								<div class="panel panel-default">
									<div class="panel-body">

										<div class="pull-left titulo-lista-personalizada">
											
											<div class="tipo_usuario">
												<?php echo $usuario['info']->nomeTipoUsuario(); ?>
											</div>
											
											<div class="nome_usuario">
												<span class="text-capitalize"><?php echo $usuario['info']->get_nome_usuario(); ?></span> 
											</div>

										</div> <!-- titulo-lista-personalizada -->

										<div class="pull-right acoes-lista-personalizada">

											<div class="btn-group" role="group">
												<a class="btn btn-style06" href="<?php echo base_url('/usuarios/consultar/' . $usuario['info']->get_id_usuario() ); ?>">Ver</a>
												<button type="button" class="btn btn-style06" data-cancelar="#cancelar_usuario_<?php echo $usuario['info']->get_id_usuario(); ?>" data-container="#usuarios-lista" data-toggle="popover" data-placement="bottom" data-html="true" data-content='
						
													<span class="cor-texto-padrao-2">Você realmente deseja remover esse usuário?</span> </br></br> 
													<button class="btn btn-style06 pull-left" id="cancelar_usuario_<?php echo $usuario['info']->get_id_usuario(); ?>">Cancelar</button> 
													<a class="btn btn-danger pull-right" style="font-weight: 500;" href="<?php echo base_url('usuarios/remover/' . $usuario['info']->get_id_usuario() ); ?>">Remover</a>   
													</br></br>

												'>Remover</button>
											</div> <!-- btn-group -->
											
										</div> <!-- acoes-lista-personalizada -->

									</div> <!-- panel-body -->

									<?php if( in_array( $usuario['info']->get_tipo_usuario(), [2, 3] ) ): ?>

										<div class="panel-footer">
												
											<div class="padrao-lista-info">
												<div class="row">
													<div class="col-md-4">
														Bateria: 
														<?php if( $usuario['info']->get_bateria() !== null && $usuario['info']->get_tipo_usuario() == 3 ): ?>
															<a  class="link-local text-capitalize" 
																tabindex="0" 
																role="button" 
																data-toggle="popover" 
																data-html="true"
																data-trigger="focus"
																data-placement="bottom" 
																data-content="<strong>Data Inicio:</strong> <?php echo $usuario['info']->get_bateria()->get_data_inicio(); ?> </br> <strong>Data Fim:</strong> <?php echo $usuario['info']->get_bateria()->get_data_fim(); ?>">
																
																<?php echo $usuario['info']->get_bateria()->get_nome_bateria(); ?>

															</a>
														<?php elseif( $usuario['info']->get_tipo_usuario() == 2 ): ?>
															Desabilitado
														<?php else: ?>
															Nenhuma
														<?php endif; ?>
													</div>
													<div class="col-md-4">
														Paciente(s): 
														<?php if( $usuario['prontuario'] == true ): ?> 
															<a href="<?php echo base_url ('usuarios/consultar/' . $usuario['info']->get_id_usuario() ); ?>#vincular-paciente" class="link-local text-capitalize">Alterar</a>
														<?php else: ?>
															Nenhum
														<?php endif; ?> 
													</div>
													<div class="col-md-4">
														<?php if( $usuario['info']->get_tipo_usuario() == 3 ): ?>
															Faltas: 
															<a class="link-local" href="<?= base_url('usuarios/consultar/' . $usuario['info']->get_id_usuario() . '#faltasUsuario' ); ?>"><?= count($this->consulta->pegarFaltas($usuario['info']->get_id_usuario())); ?></a>
														<?php endif; ?> 
													</div>
												</div> <!-- row -->
											</div> <!-- usuario-info -->

										</div> <!-- panel-footer -->

									<?php endif; ?>

								</div> <!-- panel-default -->
							</div>

						<?php endforeach; ?>

					</div> <!-- usuarios-lista -->

				</div> <!-- col-md-12 -->

			</div> <!-- row -->
		</div> <!-- container -->

	</section> <!-- padrao-conteudo -->

</div> <!-- usuario -->

<?php $this->load->view('fixos/rodape'); ?>