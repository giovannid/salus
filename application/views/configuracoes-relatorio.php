<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="meu-perfil">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li class="active">Relatórios</li>
                    </ol>
                </div> <!-- cold-md-9 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <section class="padrao-conteudo">
		<div class="container">
		   
			<section class="grupo-pront">
				<div class="row">

					<div class="col-md-4">
						<h4 class="title">Gerar Relatórios</h4>
					</div> <!-- col-md-4 -->

					<div class="col-md-8">
						<div class="form-group">
							<label>Selecione uma opção</label> <br>
							<div class="btn-group" role="group">
								<a href="<?= base_url('configuracoes/gerar_relatorio/consulta'); ?>" class="btn btn-style06">Consultas</a>
								<a href="<?= base_url('configuracoes/gerar_relatorio/paciente'); ?>" class="btn btn-style06">Pacientes</a>
								<a href="<?= base_url('configuracoes/gerar_relatorio/candidato'); ?>" class="btn btn-style06">Candidatos</a>
								<a href="<?= base_url('configuracoes/gerar_relatorio/prontuario'); ?>" class="btn btn-style06">Prontuários</a>
								<a href="<?= base_url('configuracoes/gerar_relatorio/bateria'); ?>" class="btn btn-style06">Baterias</a>
								<a href="<?= base_url('configuracoes/gerar_relatorio/estatisticas'); ?>" class="btn btn-style06">Estatisticas</a>
							</div>
						</div>
						
					</div> <!-- cold-md-8 -->

				</div> <!-- row -->
			</section> <!-- grupo-pront -->
				
		</div> <!-- contianer -->
	</section> <!-- padrao-conteudo --> 

</div> <!-- meu-perfil -->

<?php $this->load->view('fixos/rodape'); ?>