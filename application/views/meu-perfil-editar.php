<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="meu-perfil">

    <section class="padrao-topo">

        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url('meu-perfil'); ?>">Meu Perfil</a>
                        </li>
                        <li class="active">Editar Perfil</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3" id="bateria">
                    <div class="btn-group" role="group">
                        <button class="btn btn-style06" id="salvarUsuario">Salvar</button>
                        <a class="btn btn-style06" href="<?php echo base_url('meu-perfil'); ?>">Voltar</a>
                    </div>
                </div> <!-- cold-md-3 -->

            </div> <!-- row -->
        </div> <!-- container -->
        
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">
        <div class="container">

            <?php  echo form_open('meu-perfil/editar-form',
                    array( 'id' => 'usuarios_form', 'name' => 'usuarios_form' ) ); ?>

                <section class="grupo-pront">
                    <div class="row">

                        <div class="col-md-4">
                            <h4 class="title">Detalhes do Perfil</h4>
                        </div> <!-- col-md-4 -->

                        <div class="col-md-8">

                            <div class="form-group">
                                <label>Nome do Usuário</label>
                                <input type="text" name="editar_nome_usuario" class="form-control" value="<?php echo $usuario->get_nome_usuario(); ?>" required>
                            </div>

                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" name="editar_email" class="form-control" value="<?php echo $usuario->get_email(); ?>" required>
                            </div>

                            <div class="form-group">
                                <label>RA</label>
                                <input type="text" name="editar_ra" class="form-control" value="<?php echo $usuario->get_ra(); ?>" required>
                            </div>

                        </div> <!-- col-md-8 -->

                    </div> <!-- row -->
                </section> <!-- grupo-pront -->

        </div> <!-- container -->
    </section> <!-- padrao-conteudo -->

</div> <!-- meu-perfil -->
<?php $this->load->view('fixos/rodape'); ?>