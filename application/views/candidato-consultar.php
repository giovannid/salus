<?php $this->load->view('fixos/cabecalho'); ?>

<div class="estilo-padrao" id="candidato">

    <section class="padrao-topo">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <ol class="breadcrumb">
                        <li class="active">Candidatos</li>
                    </ol>
                </div> <!-- cold-md-9 -->

                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        <a class="btn btn-style06" href="<?php echo base_url('candidatos/cadastrar'); ?>">Novo Candidato</a>
                    </div>
                </div> <!-- cold-md-3 -->
                
            </div> <!-- row -->
        </div> <!-- container -->
    </section> <!-- padrao-topo -->

    <?php $this->load->view('fixos/mensagem'); ?>

    <section class="padrao-conteudo">

        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="row">
                        
                        <div class="col-md-3">
                            <div class="filtrar-candidato">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="texto-style10">Filtrar</span>
                                        <div class="input-group">
                                            <span class="input-group-addon">Filtrar por:</span>
                                            <span class="input-group-btn">
                                                <a class="btn <?= (isset($por_nome) && $por_nome == true ) ? 'btn-style06 btn-fix' : 'btn-style08'; ?>" href="<?php echo base_url('candidatos/index/0/0/filtrar/nome'); ?>">Nome</a>
                                                <a class="btn <?= (isset($por_idade) && $por_idade == true ) ? 'btn-style06 btn-fix' : 'btn-style08'; ?>" href="<?php echo base_url('candidatos/index/0/0/filtrar/idade'); ?>">Idade</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- filtrar-paciente -->
                        </div> <!-- col-md-3 -->

                        <div class="col-md-9">
                            <div class="buscar-candidato">
                                <?= form_open('candidatos/buscar', ['name' => 'buscar_candidato']); ?>
                                    <div class="form-group">
                                        <span class="texto-style10">Buscar</span>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="info"  placeholder="Digite um nome, nome do responsável ou queixa principal" <?= (isset($buscando_por)) ? 'value="' . $buscando_por . '"' : ''; ?>>
                                            <span class="input-group-btn">
                                                <input class="btn btn-style08" type="submit" value="Buscar">
                                            </span>
                                        </div><!-- /input-group -->
                                        
                                    </div>
                                </form>
                            </div>
                        </div> <!-- col-md-9 -->

                    </div> <!-- row -->

                    <div class="padrao-lista" id="candidato-lista">

                        <?php if( !empty($candidatos) || !empty($buscando_por) ): ?>
                        
                            <?php if(isset($buscando_por)): ?>
                            <div class="buscando_por">
                                <span>Buscando por: <?= $buscando_por; ?></span>

                                <?php if(empty($candidatos)): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            Não foi encontrado nenhum candidato
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <?php else: ?>
                            
                            <div class="topo-lista-personalizada">
                                <span class="pull-left">Nome do Candidato</span>
                                <span class="pull-right">Ações</span>
                            </div>

                            <?php endif; ?>

                        <?php foreach($candidatos as $candidato): ?>

                            <div class="padrao-item">
                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div class="pull-left titulo-lista-personalizada">
                                            
                                            <div class="tipo_usuario">
                                                <span class="label label-primary">Candidato</span>
                                            </div>
                                            
                                            <div class="nome_usuario">
                                                <span class="text-capitalize"><?php echo $candidato->get_nome_completo(); ?></span> 
                                            </div>

                                        </div> <!-- titulo-lista-personalizada -->

                                        <div class="pull-right acoes-lista-personalizada">

                                            <div class="btn-group" role="group">
                                                <a class="btn btn-style06" href="<?php echo base_url('/candidatos/consultar/' . $candidato->get_id_paciente() ); ?>">Ver</a>
                                                
                                                <?php if( session_visao([1,2]) ): ?>
                                                    <button type="button" class="btn btn-style06 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="<?php echo base_url('/candidatos/mover-para-paciente/' . $candidato->get_id_paciente() ); ?>">Mover Para Paciente</a>
                                                        </li>
                                                    </ul>
                                                <?php endif; ?>
                                                
                                            </div> <!-- btn-group -->
                                            
                                        </div> <!-- acoes-lista-personalizada -->

                                    </div> <!-- panel-body -->

                                    <div class="panel-footer">
                                        <div class="padrao-lista-info">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    Idade: <span class="link-local"><?php echo $candidato->calcularIdade(); ?></span>
                                                </div>
                                                <div class="col-md-6">
                                                    Queixa Principal: <span class="link-local text-capitalize"><?php echo ( $candidato->get_queixa_principal() ) ? $candidato->get_queixa_principal() : 'Nenhuma'; ?></span>
                                                </div>
                                            </div> <!-- row -->
                                        </div> <!-- candidato-info -->
                                    </div>

                                </div> <!-- panel-default -->
                            </div>

                        <?php endforeach; ?>

                        <?php else: ?>

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    Não existem candidatos cadastrados.
                                </div>
                            </div>

                        <?php endif; ?>

                    </div> <!-- usuarios-lista -->

                </div> <!-- col-md-12 -->

            </div> <!-- row -->
        </div> <!-- container -->

    </section> <!-- padrao-conteudo -->

</div> <!-- candidato -->

<?php $this->load->view('fixos/rodape'); ?>