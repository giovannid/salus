<?php $this->load->view('fixos/cabecalho-login'); ?>

<div class="login">

	<div class="container">
		<div class="row">

			<div class="col-md-4 col-md-offset-4">

					<img src="<?= base_url('front/images/logo-white.png'); ?>" width="200" class="logo" alt="Salus">
					<span class="logo-nome">Salus</span>

					<section class="login-form">

						<?php if( isset($msg) && $msg == 'erro' ): ?>
							<div class="alert alert-danger alert-dismissible" style="color: #a94442; " role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<?php echo erro_msg($codigo); ?>
							</div>
						<?php endif; ?>

						<?php echo form_open('login/trocar_senha_form'); ?>

							<div class="form-group">
								<label for="senha-atual">Senha atual</label>
								<input type="password" class="form-control" name="senha-atual" id="senha-atual">
							</div>
							<div class="form-group">
								<label for="nova-senha">Nova Senha</label>
								<input type="password" class="form-control" name="nova-senha" id="nova-senha">
							</div>
							<div class="form-group">
								<label for="nova-senha-novamente">Confirme Nova Senha</label>
								<input type="password" class="form-control" name="nova-senha-novamente" id="nova-senha-novamente">
							</div>
							<button type="submit" class="btn btn-style06 center-block">Trocar Senha</button>

						</form>

					</section> <!-- login-form -->

			</div> <!-- col-md-4 -->

		</div> <!-- row -->
	</div> <!-- container -->

</div> <!-- login -->

<?php $this->load->view('fixos/rodape'); ?>