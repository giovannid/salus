<?php $this->load->view('fixos/cabecalho'); ?>

<div class="usuario estilo-padrao">

	<section class="padrao-topo">

		<div class="container">
			<div class="row">

				<div class="col-md-9">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url('usuarios'); ?>">Usuários</a></li>
						<li class="active">
							Novo Usuário
						</li>
					</ol>
				</div> <!-- cold-md-9 -->

				<div class="col-md-3" id="bateria">
					<div class="btn-group" role="group">
						<button class="btn btn-style06" id="salvarUsuario">Salvar</button>
						<a class="btn btn-style06" href="<?php echo base_url('usuarios/index'); ?>">Voltar</a>
					</div>
				</div> <!-- cold-md-3 -->

			</div> <!-- row -->
		</div> <!-- container -->
		
	</section> <!-- padrao-topo -->

	<section class="padrao-conteudo">
		<div class="container">

			<?php  echo form_open('usuarios/cadastrar_form',
					array( 'id' => 'usuarios_form', 'name' => 'usuarios_form' ) ); ?>

				<section class="grupo-pront">

					<div class="row">
						<div class="col-md-4">
							<h4 class="title">Detalhes do Usuário</h4>
						</div>

						<div class="col-md-8">

							<div class="form-group">
								<label>Nome do Usuário</label>
								<input type="text" name="cadastrar_nome_usuario" class="form-control" required>
							</div>

							<div class="form-group">
								<label>E-mail</label>
								<input type="email" name="cadastrar_email" class="form-control" required>
							</div>

							<div class="form-group">
								<label>Senha Provisória</label>
								<input type="password" name="cadastrar_senha" class="form-control" required>
							</div>

							<div class="form-group">
								<label>RA</label>
								<input type="text" name="cadastrar_ra" class="form-control" required>
							</div>

							<div class="form-group">
								<label>Tipo usuário</label>
								<select name="cadastrar_tipo_usuario" class="form-control required" required>
									<option value="">Selecione uma opção</option>
									<option value="1">Administrador</option>
									<option value="2">Supervisor</option>
									<option value="3">Estagiário</option>
									<option value="4">Teclab</option>
								</select>
							</div>

							<div class="form-group">
								<label>Atrelado a Bateria</label>
								<select name="cadastrar_id_bateria"  class="form-control required" required>
									<option value="">Selecione uma opção</option>
									<optgroup label="Nenhuma">
										<option value="0">Nenhuma</option>
									</optgroup>

									<?php foreach( $baterias as $bateria ): ?>

										<optgroup label="Data Inicio: <?php echo $bateria->get_data_inicio(); ?> - Data Fim: <?php echo $bateria->get_data_fim(); ?>">
											<option value="<?php echo $bateria->get_id_bateria(); ?>">
												<?php echo ucwords( $bateria->get_nome_bateria() ); ?>
											</option>
										</optgroup>

									<?php endforeach; ?>
								</select>
							</div>

							<small>No primeiro login do usuário será requisitado a troca da senha.</small>

						</div>
					</div> <!-- row -->

				</section> <!--bateria-ver -->

		</div> <!-- container -->
	</section> <!-- padrao-conteudo -->

</div> <!-- usuario -->
<?php $this->load->view('fixos/rodape'); ?>