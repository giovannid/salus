<?php $this->load->view('fixos/cabecalho'); ?>

<?php $this->load->view('fixos/mensagem'); ?>

<div class="estilo-padrao" id="inicio">
   
    <div class="container">
        <div class="row">

                <div class="col-md-4">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="data-hoje">
                                <div class="panel panel-default">
                                    <div class="dia-da-semana text-capitalize"><?= strftime( '%A', $dia_atual->getTimestamp() ); ?></div>
                                    <div class="numero-dia"><?=  $dia_atual->format('d'); ?></div>
                                    <div class="mes-com-ano text-capitalize"><?= strftime('%B', $dia_atual->getTimestamp()); ?>, <?= $dia_atual->format('Y'); ?></div>
                                </div>
                            </div>

                        </div>
                    </div> <!-- first-row -->

                    <?php if( !empty($bateria) ): ?>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="inicio-bateria">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <p class="titulo">Bateria</p>
                                        <div class="nome-bateria"><?= $bateria->get_nome_bateria(); ?></div>
                                        <?php if( $bateria->get_bloqueado() ): ?>
                                            <div class="bloqueada-bateria">
                                                <span class="label label-bloqueado">Finalizada</span>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="titulo">Inicio</div>
                                                <div class="bateria-data"><?= $bateria->get_data_inicio(); ?></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="titulo">Fim</div>
                                                <div class="bateria-data"><?= $bateria->get_data_fim(); ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div> <!-- second-row -->

                    <?php endif; ?>

                </div>

                <div class="col-md-8">
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="lista-padrao lista-consultas">
                                <div class="topo-lista">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <?php if(isset($todas_consultas) && $todas_consultas == true): ?>
                                                <div class="titulo">Todas Consultas</div>
                                            <?php else: ?>
                                                <div class="titulo">Próximas Consultas</div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-2">
                                            <?php if( session_visao( [1, 2, 4] ) ): ?>
                                                <a href="<?= base_url('consultas'); ?>" class="btn btn-style06">Ver tudo</a>
                                            <?php elseif( session_visao([3]) && $todas_consultas == false ): ?>
                                                <a href="<?= base_url('inicio/index/0/0/todas'); ?>" class="btn btn-style06">Ver tudo</a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <?php if(!empty($consultas)): ?>

                                    <?php foreach($consultas as $data => $consultas_itens): ?>
                                        <?php foreach($consultas_itens as $index => $consulta): ?>

                                            <div class="lista-item">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="nome">
                                                                    <a href="<?= base_url('pacientes/consultar/' . $consulta->get_paciente()->get_id_paciente() ); ?>"><?= $consulta->get_paciente()->get_nome_completo(); ?></a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="data">
                                                                    <?php if( new DateTime($consulta->get_data()) == $dia_atual ): ?>
                                                                        <span class="label label-hoje">Hoje</span>
                                                                    <?php elseif( new DateTime($consulta->get_data()) == $dia_seguinte ): ?>
                                                                        <span class="label label-amanha">Amanha</span>
                                                                    <?php endif; ?>
                                                                    <span class="data-numeros">
                                                                        <?= (new DateTime($consulta->get_data()))->format('d/m/Y'); ?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="horas">
                                                                    <span class="label label-hora">Início</span>
                                                                    <span class="hora-inicio"><?= $consulta->get_hora_inicio(); ?></span>
                                                                    <span class="glyphicon icone-direita glyphicon-arrow-right"></span>
                                                                    <span class="hora-fim"><?= $consulta->get_hora_fim(); ?></span>
                                                                    <span class="label label-hora">Fim</span>
                                                                </div>

                                                                <a href="<?= base_url('consultas/consultar/' . $consulta->get_id_consulta() );?>" class="btn btn-style06 botao">Ver</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> <!-- lista-item -->

                                        <?php endforeach; ?>
                                    <?php endforeach; ?>

                                <?php else: ?>

                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <?= erro_msg(81); ?>
                                        </div>
                                    </div>

                                <?php endif; ?>

                            </div> <!-- lista-padrao -->
                        </div>

                    </div> <!-- first-row -->

                    <?php if( session_visao([1,2,3]) ): ?>

                    <div class="row"> <!-- second-row -->
                        <div class="col-md-12">
                            
                            <div class="lista-padrao lista-prontuarios">
                                <div class="topo-lista">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="titulo">Meus Prontuários</div>
                                        </div>
                                    </div>
                                </div>

                                <?php if(!empty($prontuarios)): ?> 
                                
                                    <?php foreach($prontuarios as $index => $prontuario): ?>

                                        <div class="lista-item">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="nome">
                                                                <a href="<?= base_url('pacientes/consultar/' . $prontuario->get_paciente()->get_id_paciente() ); ?>"><?= $prontuario->get_paciente()->get_nome_completo(); ?></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="completado">
                                                                <?php if( $prontuario->get_bloqueado() == true ): ?>
                                                                    <span class="label label-bloqueado">Bloqueado</span>
                                                                <?php endif; ?>
                                                                <span class="completado-numeros"><?= $prontuario->porcentoFeitoDoProntuario(); ?>% completado</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="btn-group btns" role="group">
                                                                <a class="btn btn-style06" href="<?= base_url('prontuarios/consultar/' . $prontuario->get_id_prontuario() ); ?>">Ver</a>
                                                                <a class="btn btn-style06" href="<?= base_url('prontuarios/editar/' . $prontuario->get_id_prontuario() ); ?>">Editar</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- lista-item -->

                                    <?php endforeach; ?>

                                <?php else: ?>

                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            Você não está vinculado a nenhum prontuário.
                                        </div>
                                    </div>

                                <?php endif; ?>

                            </div> <!-- lista-padrao -->
                        </div>

                    </div> <!-- second-row -->

                    <?php endif; ?>

                </div> <!-- cold-md-8 -->

        </div> <!-- row ->
    </div>

</div> <!-- inicio -->

<?php $this->load->view('fixos/rodape'); ?>