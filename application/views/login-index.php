<?php $this->load->view('fixos/cabecalho-login'); ?>

<div class="login">

	<div class="container">
		<div class="row">

			<div class="col-md-4 col-md-offset-4">

					<img src="<?= base_url('front/images/logo-white.png'); ?>" width="150" class="logo" alt="Salus">
					<span class="logo-nome">Salus</span>
				
					<section class="login-form">

						<?php if( isset($msg) && $msg == 'erro' ): ?>
							<div class="alert alert-danger alert-dismissible" style="color: #a94442; " role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<?php echo erro_msg($codigo); ?>
							</div>
						<?php elseif( isset($msg) && $msg == 'sucesso' ): ?>
							<div class="alert alert-success alert-dismissible" role="alert" style="color: #3c763d; margin-top: 10px;">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<?php echo erro_msg($codigo); ?>
							</div>
						<?php endif; ?>

						<?php echo form_open('login/entrar'); ?>

							<div class="form-group">
								<label for="login-email">E-mail</label>
								<input type="email" class="form-control" name="login-email" id="login-email">
							</div>
							<div class="form-group">
								<label for="login-senha">Senha</label>
								<input type="password" class="form-control" name="login-senha" id="login-senha">
							</div>
							<button type="submit" class="btn btn-style06 center-block">Entrar</button>

						</form>

					</section> <!-- login-form -->

			</div> <!-- col-md-4 -->

		</div> <!-- row -->
	</div> <!-- container -->

</div> <!-- login -->

<?php $this->load->view('fixos/rodape'); ?>