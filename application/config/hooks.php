<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['post_controller_constructor'][] = array(
        'class'    => 'Bateria',
        'function' => 'checarValidadeBaterias',
        'filename' => 'Bateria.php',
        'filepath' => 'models'
);

$hook['post_controller_constructor'][] = array(
        'class'    => null,
        'function' => 'controle_de_acesso_global',
        'filename' => 'controle_de_acesso_global.php',
        'filepath' => 'hooks'
);