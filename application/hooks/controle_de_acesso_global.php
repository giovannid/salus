<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function controle_de_acesso_global()
{
    $paginas = array(
        'candidatos'                     => [1, 2, 4], 
        'candidatos/index'               => [1, 2, 4],
        'candidatos/consultar'           => [1, 2, 4],
        'candidatos/cadastrar'           => [1, 2, 4],
        'candidatos/cadastrar-form'      => [1, 2, 4],
        'candidatos/editar'              => [1, 2, 4],
        'candidatos/editar-form'         => [1, 2, 4],
        'candidatos/remover'             => [1, 2],
        'candidatos/mover-para-paciente' => [1, 2],

        'pacientes'                      => [1, 2, 4],
        'pacientes/index'                => [1, 2, 4],
        'pacientes/consultar'            => [1, 2, 3, 4],
        'pacientes/editar'               => [1, 2],
        'pacientes/editar-form'          => [1, 2],
        'pacientes/remover'              => [1, 2],
        'pacientes/dar_alta'             => [1, 2],
        'pacientes/mover-para-candidato' => [1, 2],
        
        'prontuarios/consultar'          => [1, 2, 3],
        'prontuarios/editar'             => [1, 2, 3],
        'prontuarios/editar-form'        => [1, 2, 3],
        'prontuarios/remover'            => [1, 2],

        'baterias'                       => [1, 2],
        'baterias/index'                 => [1, 2],
        'baterias/cadastrar'             => [1, 2],
        'baterias/cadastrar-form'        => [1, 2],
        'baterias/consultar'             => [1, 2],
        'baterias/editar'                => [1, 2],
        'baterias/editar-form'           => [1, 2],
        'baterias/remover'               => [1, 2],

        'usuarios'                       => [1, 2],
        'usuarios/index'                 => [1, 2],
        'usuarios/cadastrar'             => [1, 2],
        'usuarios/cadastrar-form'        => [1, 2],
        'usuarios/consultar'             => [1, 2],
        'usuarios/editar'                => [1, 2],
        'usuarios/editar-form'           => [1, 2],
        'usuarios/remover'               => [1, 2],
        'usuarios/trocar_bateria'        => [1, 2],
        'usuarios/trocar_paciente'       => [1, 2],
        'usuarios/trocar_senha'          => [1, 2],
        'usuarios/trocar_senha_form'     => [1, 2],

        'consultas'                      => [1, 2, 4],
        'consultas/index'                => [1, 2, 4],
        'consultas/consultar'            => [1, 2, 3, 4],
        'consultas/cadastrar'            => [1, 2, 4],
        'consultas/cadastrar-form'       => [1, 2, 4],
        'consultas/editar'               => [1, 2, 3, 4],
        'consultas/editar-form'          => [1, 2, 3, 4],
        'consultas/remover'              => [1, 2, 4],

        'configuracoes'                    => [1, 2],
        'configuracoes/index'              => [1, 2],
        'configuracoes/opcoes_prontuarios' => [1, 2],
        'configuracoes/horario_permitidos' => [1, 2],
        'configuracoes/gerar_relatorio'    => [1, 2, 4],
        'configuracoes/relatorio'          => [1, 2, 4]
    );

    $ci = &get_instance();
    $acesso = ( $ci->uri->segment(2) !== null ) ? $ci->uri->segment(1) . '/' . $ci->uri->segment(2) : $ci->uri->segment(1);
    $tipo_usuario = $ci->session->usuario['tipo_usuario'];

    if( $ci->uri->segment(1) !== 'login' && $ci->session->usuario == null ) {
        redirect('login');
    } else if( $ci->uri->segment(1) !== 'login' &&  $ci->session->usuario['forcar_troca_senha'] == 1 ) {
        redirect('login/trocar_senha');
    } else if( array_key_exists($acesso, $paginas) ) {
        if( !in_array( $tipo_usuario, $paginas[$acesso] ) ) {
            redirect('inicio/index/erro/51');
        }
    }
}