<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consulta extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    private $id_consulta;
    private $id_usuario;
    private $id_paciente;
    private $data;
    private $hora_inicio;
    private $hora_fim;
    private $comparecimento_paciente;
    private $justi_paciente;
    private $comparecimento_estagiario;
    private $justi_estagiario;
    private $observacao_paciente;
    private $observacao_estagiario;
    private $data_criacao;
    private $data_atualizacao;

    public function get_id_consulta() {
        return $this->id_consulta;
    }
    public function set_id_consulta($id_consulta) {
        $this->id_consulta = $id_consulta;
    }
    public function get_usuario() {
        return $this->id_usuario;
    }
    public function set_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }
    public function get_paciente() {
        return $this->id_paciente;
    }
    public function set_paciente($id_paciente) {
        $this->id_paciente = $id_paciente;
    }
    public function get_data() {
        return $this->data;
    }
    public function set_data($data) {
        $this->data = $data;
    }
    public function get_hora_inicio() {
        return $this->hora_inicio;
    }
    public function set_hora_inicio($hora_inicio) {
        $this->hora_inicio = $hora_inicio;
    }
    public function get_hora_fim() {
        return $this->hora_fim;
    }
    public function set_hora_fim($hora_fim) {
        $this->hora_fim = $hora_fim;
    }
    public function get_comparecimento_paciente() {
        return $this->comparecimento_paciente;
    }
    public function set_comparecimento_paciente($comparecimento_paciente) {
        $this->comparecimento_paciente = $comparecimento_paciente;
    }
    public function get_justi_paciente() {
        return $this->justi_paciente;
    }
    public function set_justi_paciente($justi_paciente) {
        $this->justi_paciente = $justi_paciente;
    }
    public function get_comparecimento_estagiario() {
        return $this->comparecimento_estagiario;
    }
    public function set_comparecimento_estagiario($comparecimento_estagiario) {
        $this->comparecimento_estagiario = $comparecimento_estagiario;
    }
    public function get_justi_estagiario() {
        return $this->justi_estagiario;
    }
    public function set_justi_estagiario($justi_estagiario) {
        $this->justi_estagiario = $justi_estagiario;
    }
    public function get_observacao_paciente() {
        return ($this->observacao_paciente === null) ? 'Não Preenchido' : $this->observacao_paciente;
    }
    public function set_observacao_paciente($observacao_paciente) {
        $this->observacao_paciente = $observacao_paciente;
    }
    public function get_observacao_estagiario() {
        return ($this->observacao_estagiario === null) ? 'Não Preenchido' : $this->observacao_estagiario;
    }
    public function set_observacao_estagiario($observacao_estagiario) {
        $this->observacao_estagiario = $observacao_estagiario;
    }
    public function get_data_atualizacao()
    {
        return ( new DateTime($this->data_atualizacao) )->format("d/m/Y H:i");
    }
    public function set_data_atualizacao($data_atualizacao = null)
    {
        $data = ( $data_atualizacao != null) ? $data_atualizacao : date('Y-m-d H:i:s'); 
        $this->data_atualizacao = ( new DateTime($data) )->format("Y-m-d H:i:s");
    }
    public function get_data_criacao()
    {
        return ( new DateTime($this->data_criacao) )->format("d/m/Y H:i");
    }
    public function set_data_criacao($data_criacao = null)
    {
        $data = ( $data_criacao != null) ? $data_criacao : date('Y-m-d H:i:s'); 
        $this->data_criacao = ( new DateTime($data) )->format("Y-m-d H:i:s");
    }
    
    // conjunto = pesquisar data/hora_inicio/hora_fim
    public function consultar($id_consulta = null, $data_consulta = null, $conjunto = array(), $id_paciente = null, $id_usuario = null , $limit = false)
    {
        if( $limit !== false ) $this->db->limit( $limit );

        if( $id_consulta !== null ) {
            $resultado = $this->db->get_where( 'consulta', array( 'id_consulta' => $id_consulta) )->custom_result_object( 'Consulta' );
        } else if( !empty($conjunto) ) {
            $resultado = $this->db->get_where( 'consulta', array( 'data' => $conjunto[0]->format('Y-m-d'), 'hora_inicio' => $conjunto[1], 'hora_fim' => $conjunto[2], 'id_paciente' => $conjunto[3] ) )->custom_result_object( 'Consulta' );
        } else if( $data_consulta !== null && $id_usuario !== null ) {
            $resultado = $this->db->get_where( 'consulta', array( 'data' => $data_consulta->format('Y-m-d'), 'id_usuario' => $id_usuario ) )->custom_result_object( 'Consulta' );
        } else if( $id_paciente !== null && $id_usuario !== null ){ 
            $resultado = $this->db->get_where( 'consulta', array( 'id_paciente' => $id_paciente, 'id_usuario' => $id_usuario ) )->custom_result_object( 'Consulta' );
        } else if( $data_consulta !== null ){ 
            $resultado = $this->db->get_where( 'consulta', array( 'data' => $data_consulta->format('Y-m-d') ) )->custom_result_object( 'Consulta' );
        } else if( $id_paciente !== null ) {
            $this->db->order_by('data', 'DESC');
            $resultado = $this->db->get_where( 'consulta', array( 'id_paciente' => $id_paciente ) )->custom_result_object( 'Consulta' );
        } else if( $id_usuario !== null ) {
            $this->db->order_by('data', 'DESC');
            $resultado = $this->db->get_where( 'consulta', array( 'id_usuario' => $id_usuario ) )->custom_result_object( 'Consulta' ); 
        } else {
            $this->db->order_by('data', 'DESC');
            $resultado = $this->db->get('consulta')->custom_result_object( 'Consulta' );
        }

        $resultado_filtrado = array();
        
        if( !empty( $resultado ) )
        {
            for ($i=0; $i < count( $resultado ); $i++)
            {

                $consultarPaciente = $this->paciente->consultar( $resultado[$i]->get_paciente() );
                $resultado[$i]->set_paciente( $consultarPaciente );
        
                $consultarUsuario = $this->usuario->consultar( $resultado[$i]->get_usuario() );
                $resultado[$i]->set_usuario($consultarUsuario);

                $data_inicial = new DateTime($resultado[$i]->get_hora_inicio());
                $resultado[$i]->set_hora_inicio( $data_inicial->format('H:i') );

                $data_final = new DateTime($resultado[$i]->get_hora_fim());
                $resultado[$i]->set_hora_fim( $data_final->format('H:i') );

                if( array_key_exists($resultado[$i]->get_data(), $resultado_filtrado) ) {
                    $resultado_filtrado[$resultado[$i]->get_data()][] = $resultado[$i];
                } else {
                    $resultado_filtrado[$resultado[$i]->get_data()] = array($resultado[$i]);
                }
            }
        }
        return $resultado_filtrado;
    }

    public function pegarHorarios($id_horario = null)
    {
        $horarios = [];
        if( $id_horario !== null ) {
            $resultado = $this->db->get_where('opcoes', array('id_opcoes' => $id_horario, 'campo' => 'horario'))->result_array();
            if(!empty($resultado)) $horarios[$resultado[0]['id_opcoes']] = json_decode($resultado[0]['opcao']);
        } else {
            $this->db->order_by('id_opcoes', 'ASC');
            $resultado = $this->db->get_where('opcoes', array('campo' => 'horario') )->result();
            $horarios = [];
            foreach($resultado as $row){
                $horarios[$row->id_opcoes] = json_decode($row->opcao);
            }
        }
        return $horarios;
    }

    public function pegarIdHorario($hora_inicio = null, $hora_fim = null)
    {
        if( $hora_inicio !== null && $hora_fim !== null ) {
            $resultado = null;
            $horarios = $this->db->get_where('opcoes', array('campo' => 'horario') )->result();

            foreach($horarios as $row) {
                $extrair_horarios = json_decode($row->opcao);
                if($extrair_horarios[0] == $hora_inicio && $extrair_horarios[1] == $hora_fim) {
                    $resultado = $row->id_opcoes;
                    break;
                }
            }
            return $resultado;
        }
    }

    public function cadastrar() 
    {
        $this->set_data_atualizacao();
        $this->set_data_criacao();
        $data = array(  'id_usuario' => $this->id_usuario,
                        'id_paciente' => $this->id_paciente,
                        'data' => $this->data,
                        'hora_inicio' => $this->hora_inicio,
                        'hora_fim' => $this->hora_fim,
                        'data_atualizacao' => $this->data_atualizacao,
                        'data_criacao' => $this->data_criacao,
        );
        return $this->db->insert('consulta', $data);
    }

    
    public function consultar_ativos()
    {
        $resultado[] = $this->db->get_where( 'usuarios', array( 'login_desativado' => 'n') )->custom_result_object( 'Usuario' );
        $resultado[] = $this->db->get_where( 'paciente', array( 'paciente_ativo' => '1') )->custom_result_object( 'Paciente' );

        return $resultado;
    }

    public function remover($id_consulta) 
    {
        return $this->db->delete('consulta', array('id_consulta' => $id_consulta));
    }
    
    public function editar() 
    {
        $this->set_data_atualizacao();
        $data = array( 'data_atualizacao' => $this->data_atualizacao );

        if( $this->id_usuario   !== null ) $data['id_usuario']  = $this->id_usuario; 
        if( $this->id_paciente  !== null ) $data['id_paciente'] = $this->id_paciente; 
        if( $this->data         !== null ) $data['data']        = $this->data; 
        if( $this->hora_inicio  !== null ) $data['hora_inicio'] = $this->hora_inicio; 
        if( $this->hora_fim     !== null ) $data['hora_fim']    = $this->hora_fim;

        if( $this->comparecimento_paciente   !== null ) $data['comparecimento_paciente']   = $this->comparecimento_paciente;
        if( $this->justi_paciente            !== null ) $data['justi_paciente']            = $this->justi_paciente;
        if( $this->comparecimento_estagiario !== null ) $data['comparecimento_estagiario'] = $this->comparecimento_estagiario;
        if( $this->justi_estagiario          !== null ) $data['justi_estagiario']          = $this->justi_estagiario;
        if( $this->observacao_paciente       !== null ) $data['observacao_paciente']       = $this->observacao_paciente;
        if( $this->observacao_estagiario     !== null ) $data['observacao_estagiario']     = $this->observacao_estagiario;
        
        return $this->db->update('consulta', $data, array('id_consulta' => $this->id_consulta));
    }

    public function removerConsultaPorPaciente($id_paciente)
    {
        $consultas = $this->db->get_where( 'consulta', array('id_paciente' => $id_paciente) )->result();
        $data = array();
        foreach($consultas as $row) {
            $data[] = $row->id_consulta;
        }
        if( !empty($data) ) {
            $this->db->where_in('id_consulta', $data);
            return $this->db->delete('consulta');
        }
    }

    public function pegarFaltas($id_usuario = null, $id_paciente = null)
    {
        if( $id_usuario !== null ) {
            $consulta = $this->db->get_where('consulta', array('id_usuario' => $id_usuario, 'comparecimento_estagiario' => 0) )->custom_result_object('Consulta');
        } else if ( $id_paciente !== null ) {
            $consulta = $this->db->get_where('consulta', array('id_paciente' => $id_paciente, 'comparecimento_paciente' => 0) )->custom_result_object('Consulta');
        } else {
            return false;
        }
        if( !empty( $consulta ) )
        {
            for ($i=0; $i < count( $consulta ); $i++)
            {
                $consultarPaciente = $this->paciente->consultar( $consulta[$i]->get_paciente() );
                $consulta[$i]->set_paciente( $consultarPaciente );

                $consultarUsuario = $this->usuario->consultar( $consulta[$i]->get_usuario() );
                $consulta[$i]->set_usuario($consultarUsuario);
            }
        }

        return $consulta;
    }

}