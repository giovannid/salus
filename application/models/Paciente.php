<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paciente extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	private $id_paciente;
	private $candidato;
	private $desabilitado;
	private $desabilitado_motivo;
	private $nome_completo;
	private $data_nascimento;
	private $sexo;
	private $hipotese_diagnostico;
	private $cid;
	private $nome_responsavel;
	private $parentesco_resp;
	private $endereco;
	private $telefone;
	private $celular;
	private $queixa_principal;
	private $deficit_funcional;
	private $medico_resp;
	private $hospital_procedencia;
	private $tratamento_previo;
	private $adnpm;
	private $adnpm_motivo;
	private $sindrome_down;
	private $paralisia_braquial_congenita;
	private $mielomeningocele;
	private $outras_sindromes;
	private $encefalopatia_cronica;
	private $classificacao_topografia;
	private $classficacao_clinica;
	private $nivel;
	private $gmfcs_nivel;
	private $historia_molestia_intercorrencia;
	private $historia_molestia_observacao;
	private $patologia_disturbio_associados;
	private $medicamento_uso;
	private $exames_complementares;
	private $orteses_proteses_adaptacoes;
	private $caracteristicas_sindromicas;
	private $data_atualizacao;
	private $data_criacao;

	public function get_id_paciente() {
		return $this->id_paciente;
	}
	public function set_id_paciente($id_paciente) {
		$this->id_paciente = $id_paciente;
	}
	public function get_candidato() {
		return $this->candidato;
	}
	public function set_candidato($candidato) {
		$this->candidato = $candidato;
	}
	public function get_nome_completo() {
		return $this->nome_completo;
	}
	public function set_nome_completo($nome_completo) {
		$this->nome_completo = $nome_completo;
	}
	public function get_data_nascimento() {
		$date_ob = new DateTime($this->data_nascimento);
		return $date_ob->format("d-m-Y");
	}
	public function set_data_nascimento($data_nascimento) {
		$this->data_nascimento = ( new DateTime($data_nascimento) )->format("Y-m-d H:i:s");
	}
	public function get_sexo() {
		return $this->sexo;
	}
	public function set_sexo($sexo) {
		$this->sexo = $sexo;
	}
	public function get_hipotese_diagnostico() {
		return $this->hipotese_diagnostico;
	}
	public function set_hipotese_diagnostico($hipotese_diagnostico) {
		$this->hipotese_diagnostico = $hipotese_diagnostico;
	}
	public function get_cid() {
		return $this->cid;
	}
	public function set_cid($cid) {
		$this->cid = $cid;
	}
	public function get_nome_responsavel() {
		return $this->nome_responsavel;
	}
	public function set_nome_responsavel($nome_responsavel) {
		$this->nome_responsavel = $nome_responsavel;
	}
	public function get_parentesco_resp() {
		return $this->parentesco_resp;
	}
	public function set_parentesco_resp($parentesco_resp) {
		$this->parentesco_resp = $parentesco_resp;
	}
	public function get_endereco() {
		return $this->endereco;
	}
	public function set_endereco($endereco) {
		$this->endereco = $endereco;
	}
	public function get_telefone() {
		return $this->telefone;
	}
	public function set_telefone($telefone) {
		$this->telefone = $telefone;
	}
	public function get_celular() {
		return $this->celular;
	}
	public function set_celular($celular) {
		$this->celular = $celular;
	}
	public function get_queixa_principal() {
		return $this->queixa_principal;
	}
	public function set_queixa_principal($queixa_principal) {
		$this->queixa_principal = $queixa_principal;
	}
	public function get_deficit_funcional() {
		return $this->deficit_funcional;
	}
	public function set_deficit_funcional($deficit_funcional) {
		$this->deficit_funcional = $deficit_funcional;
	}
	public function get_medico_resp() {
		return $this->medico_resp;
	}
	public function set_medico_resp($medico_resp) {
		$this->medico_resp = $medico_resp;
	}
	public function get_hospital_procedencia() {
		return $this->hospital_procedencia;
	}
	public function set_hospital_procedencia($hospital_procedencia) {
		$this->hospital_procedencia = $hospital_procedencia;
	}
	public function get_tratamento_previo() {
		return $this->tratamento_previo;
	}
	public function set_tratamento_previo($tratamento_previo) {
		$this->tratamento_previo = $tratamento_previo;
	}
	public function get_adnpm() {
		return $this->adnpm;
	}
	public function set_adnpm($adnpm) {
		$this->adnpm = $adnpm;
	}
	public function get_adnpm_motivo() {
		return $this->adnpm_motivo;
	}
	public function set_adnpm_motivo($adnpm_motivo) {
		$this->adnpm_motivo = $adnpm_motivo;
	}
	public function get_sindrome_de_down() {
		return $this->sindrome_de_down;
	}
	public function set_sindrome_de_down($sindrome_de_down) {
		$this->sindrome_de_down = $sindrome_de_down;
	}
	public function get_paralisia_braquial() {
		return $this->paralisia_braquial;
	}
	public function set_paralisia_braquial($paralisia_braquial) {
		$this->paralisia_braquial = $paralisia_braquial;
	}
	public function get_mielo() {
		return $this->mielo;
	}
	public function set_mielo($mielo) {
		$this->mielo = $mielo;
	}
	public function get_outras_sindromes() {
		return $this->outras_sindromes;
	}
	public function set_outras_sindromes($outras_sindromes) {
		$this->outras_sindromes = $outras_sindromes;
	}
	public function get_encefalopatia() {
		return $this->encefalopatia;
	}
	public function set_encefalopatia($encefalopatia) {
		$this->encefalopatia = $encefalopatia;
	}
	public function get_class_topografia() {
		return $this->class_topografia;
	}
	public function set_class_topografia($class_topografia) {
		$this->class_topografia = $class_topografia;
	}
	public function get_class_clinica() {
		return $this->class_clinica;
	}
	public function set_class_clinica($class_clinica) {
		$this->class_clinica = $class_clinica;
	}
	public function get_nivel() {
		return $this->nivel;
	}
	public function set_nivel($nivel) {
		$this->nivel = $nivel;
	}
	public function get_gmfcs_nivel() {
		return $this->gmfcs_nivel;
	}
	public function set_gmfcs_nivel($gmfcs_nivel) {
		$this->gmfcs_nivel = $gmfcs_nivel;
	}
	public function get_historia_molestia() {
		return $this->historia_molestia;
	}
	public function set_historia_molestia($historia_molestia) {
		$this->historia_molestia = $historia_molestia;
	}
	public function get_historia_molestia_obs() {
		return $this->historia_molestia_obs;
	}
	public function set_historia_molestia_obs($historia_molestia_obs) {
		$this->historia_molestia_obs = $historia_molestia_obs;
	}
	public function get_patol_disturbio_assoc() {
		return $this->patol_disturbio_assoc;
	}
	public function set_patol_disturbio_assoc($patol_disturbio_assoc) {
		$this->patol_disturbio_assoc = $patol_disturbio_assoc;
	}
	public function get_medicamento_uso() {
		return $this->medicamento_uso;
	}
	public function set_medicamento_uso($medicamento_uso) {
		$this->medicamento_uso = $medicamento_uso;
	}
	public function get_exames_complementares() {
		return $this->exames_complementares;
	}
	public function set_exames_complementares($exames_complementares) {
		$this->exames_complementares = $exames_complementares;
	}
	public function get_orteses_proteses_adaptacoes() {
		return $this->orteses_proteses_adaptacoes;
	}
	public function set_orteses_proteses_adaptacoes($orteses_proteses_adaptacoes) {
		$this->orteses_proteses_adaptacoes = $orteses_proteses_adaptacoes;
	}
	public function get_caracteristicas_sindromicas() {
		return $this->caracteristicas_sindromicas;
	}
	public function set_caracteristicas_sindromicas($caracteristicas_sindromicas) {
		$this->caracteristicas_sindromicas = $caracteristicas_sindromicas;
	}
	public function get_desabilitado() {
		return $this->desabilitado;
	}
	public function set_desabilitado($desabilitado) {
		$this->desabilitado = $desabilitado;
	}
	public function get_desabilitado_motivo() {
		return $this->desabilitado_motivo;
	}
	public function set_desabilitado_motivo($desabilitado_motivo) {
		$this->desabilitado_motivo = $desabilitado_motivo;
	}
	public function get_data_atualizacao()
	{
		return ( new DateTime($this->data_atualizacao) )->format("d/m/Y H:i");
	}

	public function set_data_atualizacao($data_atualizacao = null)
	{
		$data = ( $data_atualizacao != null) ? $data_atualizacao : date('Y-m-d H:i:s'); 
		$this->data_atualizacao = ( new DateTime($data) )->format("Y-m-d H:i:s");
	}

	public function get_data_criacao()
	{
		return ( new DateTime($this->data_criacao) )->format("d/m/Y H:i");
	}

	public function set_data_criacao($data_criacao = null)
	{
		$data = ( $data_criacao != null) ? $data_criacao : date('Y-m-d H:i:s'); 
		$this->data_criacao = ( new DateTime($data) )->format("Y-m-d H:i:s");
	}

	public function consultar($id_paciente = null, $candidato = null, $orderby = null, $asc = 'ASC', $desabilitado = false)
	{
		$orderby = ($orderby !== null ) ? $orderby : 'id_paciente';
		$this->db->order_by( $orderby, $asc );

		if ($id_paciente !== null) {
			$resultado = $this->db->get_where('paciente', array('id_paciente' => $id_paciente))->custom_result_object('Paciente');
		} else if( $candidato == true ) {
			$resultado = $this->db->get_where('paciente', array('candidato' => 1, 'desabilitado' => 0))->custom_result_object('Paciente');
		} else if($desabilitado == true) {
			$resultado = $this->db->get_where('paciente', array('candidato' => 0, 'desabilitado' => 1))->custom_result_object('Paciente');
		} else {
			$resultado = $this->db->get_where('paciente', array('candidato' => 0, 'desabilitado' => 0))->custom_result_object('Paciente');
		}
		return ( !empty($resultado) && $id_paciente != null ) ? $resultado[0] : $resultado;
	}

	public function buscar($info, $candidato = 0)
	{
		$resultado  = $this->db->select('*')->from('paciente')
			->where('candidato', $candidato)
			->group_start()
				->or_like('nome_completo', $info)
				->or_like('nome_responsavel', $info)
				->or_like('queixa_principal', $info)
			->group_end()->get()->custom_result_object('Paciente');

		return $resultado;
	}

	public function editar()
	{
		$fields = $this->input->post();
		$data = array();

		foreach($fields as $field => $value) {
			if( method_exists('Paciente', 'set_' . $field) ) {
				$data[$field] = $value;
			}
		}
		
		try {
			$data['data_nascimento'] = ( new DateTime( str_replace('/', '-', $data['data_nascimento']) ) )->format('Y-m-d'); 
		} catch (Exception $e) {
			log_message('error', '[Classe] Paciente [Metodo] editar -> Não foi possivel editar a data de nascimento do paciente/candidato');
			return false;
		}
		$data['data_atualizacao'] = date('Y-m-d H:i:s');

		$this->db->where('id_paciente', $this->id_paciente);
		return $this->db->update('paciente', $data);
	}

	public function cadastrar()
	{
		$fields = $this->input->post();
		$data = array();

		foreach($fields as $field => $value) {
			if( method_exists('Paciente', 'set_' . $field) ) {
				$data[$field] = $value;
			}
		}
		try {
			$data['data_nascimento'] = ( new DateTime( str_replace('/', '-', $data['data_nascimento']) ) )->format('Y-m-d'); 
		} catch(Exception $e) {
			log_message('error', '[Classe] Paciente [Metodo] cadastrar -> Não foi possivel editar a data de nascimento do paciente/candidato');
			return false;
		}
		$data['data_atualizacao'] = date('Y-m-d H:i:s');
		$data['data_criacao'] = date('Y-m-d H:i:s');

		if( $this->db->insert('paciente', $data) ){
			return $this->db->insert_id();
		}
		return false;
	}

	public function desabilitar($motivo = null) {
		if ( !empty($this->id_paciente && $motivo !== null) ) {
			$this->set_data_atualizacao();
			$data = array(
				'desabilitado' => 1,
				'desabilitado_motivo' => $motivo,
				'data_atualizacao' => $this->data_atualizacao 
			);
			$this->db->where('id_paciente', $this->id_paciente);
			return $this->db->update('paciente', $data);
		}
		return false;
	}

	public function remover() {
		if ( !empty($this->id_paciente) ) {
			return $this->db->delete( 'paciente', array('id_paciente' => $this->id_paciente) );
		}
		return false;
	}

	public function checarSeExiste($pacientes = array()) {
		$saida = array();
		if( !empty($pacientes) ) {
			foreach($pacientes as $key => $id_paciente) {
				if ( !empty($this->consultar($id_paciente)) ) {
					$saida[] = $id_paciente;
				}
			}
		}
		return $saida;
	}

	public function calcularIdade()
	{
		$datetime = new DateTime( $this->data_nascimento );
		$idade_anos = $datetime->diff( new DateTime("now") )->y;
		$idade_meses = $datetime->diff( new DateTime("now") )->m;
		if( $idade_anos == 0 ) {
			return ( $idade_meses == 1 ) ? $idade_meses . ' mês' : $idade_meses . ' meses';
		} else {
			return ( $idade_anos == 1 ) ? $idade_anos . ' ano' : $idade_anos . ' anos';
		}
	}

	public function pegarOpcoesParaCamposDoPaciente($campo = null) 
	{
		if( $campo != null ) {
			$resultado = $this->db->get_where('opcoes', array('campo' => $campo))->result_array();
			return $resultado;
		}
		return array();
	}

	public function pegarOpcaoUnica($id_opcoes) 
	{
		$resultado = $this->db->get_where('opcoes', array('id_opcoes' => $id_opcoes))->result_array();
		return ( !empty($resultado) ) ? $resultado[0]['opcao'] : '';
	}

	public function moverParaCandidato($opcao = null)
	{
		if ( !empty($this->id_paciente) && $opcao !== null ) {
			$this->set_data_atualizacao();
			$data = array(
				'candidato' => $opcao,
				'data_atualizacao' => $this->data_atualizacao 
			);
			$this->db->where('id_paciente', $this->id_paciente);
			return $this->db->update('paciente', $data);
		}
		return false;
	}
	
}
