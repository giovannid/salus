<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bateria extends CI_Model
{
    private $id_bateria;
    private $nome_bateria;
    private $data_inicio;
    private $data_fim;
    private $data_criacao;
    private $data_atualizacao;
    private $bloqueado;
    private $desabilitado;

    public function get_id_bateria()
    {
        return $this->id_bateria;
    }

    public function set_id_bateria($id_bateria)
    {
        $this->id_bateria = $id_bateria;
    }

    public function get_nome_bateria()
    {
        return $this->nome_bateria;
    }

    public function set_nome_bateria($nome_bateria)
    {
        $this->nome_bateria = $nome_bateria;
    }

    public function get_data_inicio() {
        $date_ob = new DateTime($this->data_inicio);
        return $date_ob->format("d/m/Y");
    }

    public function set_data_inicio($data_inicio) {
        $this->data_inicio = $data_inicio;
    }

    public function get_data_fim() {
        $date_ob = new DateTime($this->data_fim);
        return $date_ob->format("d/m/Y");
    }

    public function set_data_fim($data_fim) {
        $this->data_fim = $data_fim;
    }

    public function get_data_atualizacao()
    {
        return ( new DateTime($this->data_atualizacao) )->format("d/m/Y H:i");
    }

    public function set_data_atualizacao($data_atualizacao = null)
    {
        $data = ( $data_atualizacao != null) ? $data_atualizacao : date('Y-m-d H:i:s'); 
        $this->data_atualizacao = ( new DateTime($data) )->format("Y-m-d H:i:s");
    }

    public function get_data_criacao()
    {
        return ( new DateTime($this->data_criacao) )->format("d/m/Y H:i");
    }

    public function set_data_criacao($data_criacao = null)
    {
        $data = ( $data_criacao != null) ? $data_criacao : date('Y-m-d H:i:s'); 
        $this->data_criacao = ( new DateTime($data) )->format("Y-m-d H:i:s");
    }

    public function get_desabilitado()
    {
        return $this->desabilitado;
    }

    public function set_desabilitado($desabilitado)
    {
        $this->desabilitado = $desabilitado;
    }

    public function get_bloqueado()
    {
        return $this->bloqueado;
    }

    public function set_bloqueado($bloqueado)
    {
        $this->bloqueado = $bloqueado;
    }

    public function consultar( $id_bateria = null, $mostrar_tudo = null )
    {
        if ( $id_bateria != null ) {
            $resultado = $this->db->get_where( 'bateria', array('id_bateria' => $id_bateria) )->custom_result_object('Bateria');
        } else {
            $opcoes = array('bloqueado' => 0 );
            if($mostrar_tudo === true) {
                unset($opcoes['bloqueado']);
            }
            $resultado = $this->db->get_where( 'bateria', $opcoes )->custom_result_object('Bateria');
        }
        return ( !empty($resultado) && $id_bateria != null ) ? $resultado[0] : $resultado;
    }

    public function cadastrar()
    {
        $this->set_data_criacao();
        $this->set_data_atualizacao();
        $data = array(
                'nome_bateria' 	=> $this->nome_bateria,
                'data_inicio' 	=> $this->data_inicio,
                'data_fim' 		=> $this->data_fim,
                'data_criacao' 	=> $this->data_criacao,
                'data_atualizacao' => $this->data_atualizacao
        );
        return $this->db->insert('bateria', $data);
    }

    public function editar()
    {
        $this->set_data_atualizacao();
        $data = array(
                'nome_bateria' 	=> $this->nome_bateria,
                'data_inicio' 	=> $this->data_inicio,
                'data_fim' 		=> $this->data_fim,
                'data_atualizacao' => $this->data_atualizacao
        );
        return $this->db->update( 'bateria', $data, array('id_bateria' => $this->id_bateria) );
    }

    public function bloquear($id_bateria)
    {
        $this->set_data_atualizacao();
        $data = array(
                'bloqueado'			=> true,
                'data_atualizacao' 	=> $this->data_atualizacao
        );
        $this->db->where('id_bateria', $id_bateria);
        return $this->db->update( 'bateria', $data );
    }

    public function remover() 
    {
        return $this->db->delete( 'bateria', array('id_bateria' => $this->id_bateria) );
    }

    public function checarValidadeBaterias()
    {
        $data_hoje = new DateTime();
        $baterias = $this->consultar();
        $usuarios = $this->usuario->consultar(null, null, true);

        foreach($baterias as $key => $bateria)
        {   
            $data_fim_bateria = new DateTime( str_replace('/', '-', $bateria->get_data_fim()) . '23:59:59' );

            if( $data_fim_bateria <= $data_hoje ) {
                if( $bateria->get_bloqueado() == false ) {
                    if( !$bateria->bloquear( $bateria->get_id_bateria() ) ) {
                        log_message('error', 'Metodo [checarValidadeBaterias] Não foi possivel bloquear a bateria id: ' . $bateria->get_id_bateria() );
                    }

                    // bloquear todos os prontuarios de todos os usuarios do tipo estagiario que estiverem nessa bateria
                    foreach($usuarios as $key2 => $usuario) {
                        if( $usuario->get_bateria() !== null && $usuario->get_tipo_usuario() != 2 && $usuario->get_bateria()->get_id_bateria() == $bateria->get_id_bateria() ){
                            
                            $prontuarios = $this->prontuario->consultar(null, null, $usuario);
                            if( !empty($prontuarios) ) {
                                foreach($prontuarios as $key3 => $prontuario) {
                                    $prontuario->bloquear( $prontuario->get_id_prontuario() );
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}