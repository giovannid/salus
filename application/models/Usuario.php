<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Model {

    private $id_usuario;
    private $nome_usuario;
    private $email;
    private $senha;
    private $tipo_usuario;
    private $ra;
    private $id_bateria;
    private $desabilitado;
    private $data_criacao;
    private $data_atualizacao;
    private $forcar_troca_senha;

    public function get_id_usuario() {
        return $this->id_usuario;
    }

    public function set_id_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function get_nome_usuario() {
        return $this->nome_usuario;
    }

    public function set_nome_usuario($nome_usuario) {
        $this->nome_usuario = $nome_usuario;
    }

    public function get_email() {
        return $this->email;
    }

    public function set_email($email) {
        $this->email = $email;
    }

    public function get_senha() {
        return $this->senha;
    }

    public function set_senha($senha) {
        $this->senha = $senha;
    }

    public function get_tipo_usuario() {
        return $this->tipo_usuario;
    }

    public function set_tipo_usuario($tipo_usuario) {
        $this->tipo_usuario = $tipo_usuario;
    }

    public function get_ra() {
        return $this->ra;
    }

    public function set_ra($ra) {
        $this->ra = $ra;
    }

    public function get_bateria() {
        return $this->id_bateria;
    }

    public function set_bateria($id_bateria) {
        $this->id_bateria = $id_bateria;
    }

    public function get_desabilitado() {
        return $this->desabilitado;
    }

    public function set_desabilitado($desabilitado) {
        $this->desabilitado = $desabilitado;
    }

    public function get_data_atualizacao()
    {
        return ( new DateTime($this->data_atualizacao) )->format("d/m/Y H:i");
    }

    public function set_data_atualizacao($data_atualizacao = null)
    {
        $data = ( $data_atualizacao != null) ? $data_atualizacao : date('Y-m-d H:i:s'); 
        $this->data_atualizacao = ( new DateTime($data) )->format("Y-m-d H:i:s");
    }

    public function get_data_criacao()
    {
        return ( new DateTime($this->data_criacao) )->format("d/m/Y H:i");
    }

    public function set_data_criacao($data_criacao = null)
    {
        $data = ( $data_criacao != null) ? $data_criacao : date('Y-m-d H:i:s'); 
        $this->data_criacao = ( new DateTime($data) )->format("Y-m-d H:i:s");
    }

    public function get_forcar_troca_senha() {
        return $this->forcar_troca_senha;
    }

    public function set_forcar_troca_senha($forcar_troca_senha) {
        $this->forcar_troca_senha = $forcar_troca_senha;
    }

    public function criptografar_senha( $senha )
    {
        $options = [
            'cost' => 12,
        ];
        return password_hash($senha, PASSWORD_BCRYPT, $options);
    }

    public function cadastrar()
    {
        $this->set_data_atualizacao();
        $this->set_data_criacao();
        $data = array(
            'nome_usuario' => $this->nome_usuario,
            'email' => $this->email,
            'ra' => $this->ra,
            'senha' => $this->criptografar_senha( $this->senha ),
            'tipo_usuario' => $this->tipo_usuario,
            'id_bateria' => $this->id_bateria,
            'data_atualizacao' => $this->data_atualizacao,
            'data_criacao' => $this->data_criacao,
            'forcar_troca_senha' => $this->forcar_troca_senha
        );

        return $this->db->insert('usuario', $data);
    }

    public function consultar($id_usuario = null, $email = null, $somente_estagio = false, $tipo = array()) 
    {
        if( $id_usuario != null ) {
            $resultado = $this->db->get_where( 'usuario', array('id_usuario' => $id_usuario ) )->custom_result_object('Usuario');
        } else if($email != null) {
            $resultado = $this->db->get_where( 'usuario', array('email' => $email ) )->custom_result_object('Usuario');
        } else if( $somente_estagio == true ) {
            $resultado = $this->db->get_where( 'usuario', array('tipo_usuario' => 3, 'desabilitado' => 0 ) )->custom_result_object('Usuario');
        } else if( !empty($tipo) ) {
            $this->db->where_in('tipo_usuario', $tipo);
            $resultado = $this->db->get_where( 'usuario', array( 'desabilitado' => 0 ) )->custom_result_object('Usuario');
        } else {
            $resultado = $this->db->get_where( 'usuario', array( 'desabilitado' => 0 ) )->custom_result_object('Usuario');
        }

        if( !empty( $resultado ) ) {
            for ($i=0; $i < count( $resultado ); $i++) {
                $bateria = $this->bateria->consultar( $resultado[$i]->get_bateria() );
                $resultado[$i]->set_bateria( ( !empty($bateria) ? $bateria : null ) );
            }
        }
        return ( !empty($resultado) && ( $id_usuario != null || $email != null ) ) ? $resultado[0] : $resultado;
    }

    public function buscar($info)
	{
        if( empty($info) ) return array();
		$resultado  = $this->db->select('*')->from('usuario')
			->group_start()
				->or_like('nome_usuario', $info)
				->or_like('email', $info)
			->group_end()->get()->custom_result_object('Usuario');

		return $resultado;
	}

    public function consultarUsuarioPorBateria($id_bateria = null)
    {
        if( $id_bateria !== null ) {
            return $this->db->get_where( 'usuario', array('id_bateria' => $id_bateria) )->custom_result_object('Usuario');
        }
        return [];
    }

    public function editar()
    {
        $this->set_data_atualizacao();
        $data = array(
            'nome_usuario' => $this->nome_usuario,
            'email' => $this->email,
            'ra' => $this->ra,
            'tipo_usuario' => $this->tipo_usuario,
            'data_atualizacao' => $this->data_atualizacao
        );

        $this->db->where('id_usuario', $this->id_usuario);
        return $this->db->update('usuario', $data);
    }

    public function trocarBateria()
    {
        if( $this->id_usuario != null && $this->id_bateria != null ) {
            
            $this->set_data_atualizacao();
            $data = array(
                'id_bateria' => $this->id_bateria,
                'data_atualizacao' => $this->data_atualizacao
            );
            $this->db->where('id_usuario', $this->id_usuario);
            return $this->db->update('usuario', $data);
        }
        return false;
    }

    public function trocarSenha()
    {
        if( $this->id_usuario != null && $this->senha != null ) {
            
            $this->set_data_atualizacao();
            $data = array(
                'senha' => $this->criptografar_senha( $this->senha ),
                'forcar_troca_senha' => $this->forcar_troca_senha,
                'data_atualizacao' => $this->data_atualizacao
            );
            $this->db->where('id_usuario', $this->id_usuario);
            return $this->db->update('usuario', $data);
        }
        return false;
    }


    public function remover()
    {
        $this->db->where('id_usuario', $this->id_usuario);
        return $this->db->update('usuario', array('desabilitado' => 1) );
    }

    public static function autenticar($email, $senha)
    {
        $usuario = (new Usuario)->consultar( null, $email );
        if( count($usuario) == 1 && $usuario->get_desabilitado() != true ) {

            if( password_verify( $senha, $usuario->get_senha() ) ) {
                return $usuario;
            }
        }
        return false;
    }

    public function nomeTipoUsuario() 
    {
        switch ($this->tipo_usuario) {
            case 1:
                return '<span class="label label-danger">Administrador</span>';
                break;
            case 2:
                return '<span class="label label-style10">Supervisor</span>';
                break;
            case 3:
                return '<span class="label label-warning">Estagiário(a)</span>';
                break;
            case 4:
                return '<span class="label label-info">Teclab</span>';
                break;
            default:
                return '<span class="label label-default">Padrão</span>';
                break;
        }
    }

    public function consultarBaterias()
    {
        return $this->bateria->consultar();
    }
}