<?php defined('BASEPATH') OR exit('No direct script access allowed');


if ( !function_exists('pagina_ativa') ) {

    function pagina_ativa( $menu_item ) {
        $uri = uri_string();
        if( empty($uri) && $menu_item == 'inicio' ){
            return 'ativo';
        }
        if( strpos($uri, $menu_item) !== false ) {
            return 'ativo';
        }
        if( $menu_item == 'user' && ( strpos($uri, 'usuarios') !== false || strpos($uri, 'configuracoes') !== false || strpos($uri, 'meu-perfil') !== false || strpos($uri, 'trocar-senha') !== false ) ) {
            return 'ativo';
        }
    }
    
}