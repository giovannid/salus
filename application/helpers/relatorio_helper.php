<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( !function_exists('exportaConsulta') ) {

    function exportaConsulta($nomeArquivo)
    {
        $ci = &get_instance();

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='. $nomeArquivo .'.csv');

        $output = fopen('php://output', 'w');

        $resultado = $ci->db->query('DESCRIBE consulta')->result_array();
        $nome_colunas = array();
        if( !empty($resultado) ) {
            foreach($resultado as $index => $info) {
                $nome_colunas[] = $info['Field'];
            }
        }

        fputcsv($output, $nome_colunas);

        $conteudo = $ci->db->query("select `id_consulta`, usuario.nome_usuario,  paciente.nome_completo , `data`, `hora_inicio`, `hora_fim`, `comparecimento_paciente`, `justi_paciente`, `comparecimento_estagiario`, `justi_estagiario`, `observacao_paciente`, `observacao_estagiario`, consulta.data_criacao, consulta.data_atualizacao from consulta JOIN usuario on consulta.id_usuario = usuario.id_usuario JOIN paciente on consulta.id_paciente = paciente.id_paciente");
        while ( $row = $conteudo->unbuffered_row('array') ) fputcsv($output, $row);
    }

}

if ( !function_exists('exportaBateria') ) {

    function exportaBateria($nomeArquivo)
    {
        $ci = &get_instance();

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='. $nomeArquivo .'.csv');

        $output = fopen('php://output', 'w');

        $resultado = $ci->db->query('DESCRIBE bateria')->result_array();
        $nome_colunas = array();
        if( !empty($resultado) ) {
            foreach($resultado as $index => $info) {
                if( $info['Field'] != 'desabilitado' ) $nome_colunas[] = $info['Field'];
            }
        }

        fputcsv($output, $nome_colunas);

        $conteudo = $ci->db->query("SELECT `id_bateria`, `nome_bateria`, `data_inicio`, `data_fim`, `data_criacao`, `data_atualizacao`, `bloqueado` FROM `bateria` ");
        while ( $row = $conteudo->unbuffered_row('array') ) fputcsv($output, $row);
    }

}

if ( !function_exists('exportaPaciente') ) {

    function exportaPaciente($nomeArquivo, $somente_candidato = false)
    {
        $ci = &get_instance();

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='. $nomeArquivo .'.csv');

        $output = fopen('php://output', 'w');

        $resultado = $ci->db->query('DESCRIBE paciente')->result_array();
        $nome_colunas = array();
        if( !empty($resultado) ) {
            foreach($resultado as $index => $info) {
                $nome_colunas[] = $info['Field'];
            }
        }

        fputcsv($output, $nome_colunas);

        $opcao = ($somente_candidato == false) ? 0 : 1;

        $conteudo = $ci->db->query("SELECT `id_paciente`, `candidato`, `desabilitado`, `desabilitado_motivo`, `nome_completo`, `data_nascimento`, `sexo`, `hipotese_diagnostico`, `cid`, `nome_responsavel`, `parentesco_resp`, `endereco`, `telefone`, `celular`, `queixa_principal`, `deficit_funcional`, `medico_resp`, `hospital_procedencia`, `tratamento_previo`, ( select opcao from opcoes where opcoes.id_opcoes = paciente.adnpm ), `adnpm_motivo`, ( select opcao from opcoes where opcoes.id_opcoes = paciente.sindrome_de_down ), ( select opcao from opcoes where opcoes.id_opcoes = paciente.paralisia_braquial ), ( select opcao from opcoes where opcoes.id_opcoes = paciente.mielo ), `outras_sindromes`, ( select opcao from opcoes where opcoes.id_opcoes = paciente.encefalopatia ), ( select opcao from opcoes where opcoes.id_opcoes = paciente.class_topografia ), `class_clinica`, ( select opcao from opcoes where opcoes.id_opcoes = paciente.nivel ), `gmfcs_nivel`, ( select opcao from opcoes where opcoes.id_opcoes = paciente.historia_molestia ), `historia_molestia_obs`, `patol_disturbio_assoc`, `medicamento_uso`, `exames_complementares`, `orteses_proteses_adaptacoes`, `caracteristicas_sindromicas`, `data_criacao`, `data_atualizacao` FROM `paciente`, opcoes WHERE candidato = " . $opcao . " GROUP by id_paciente");
        while ( $row = $conteudo->unbuffered_row('array') ) fputcsv($output, $row);
    }

}

if ( !function_exists('exportaProntuario') ) {

    function exportaProntuario($nomeArquivo)
    {
        $ci = &get_instance();

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='. $nomeArquivo .'.csv');

        $output = fopen('php://output', 'w');

        $resultado = $ci->db->query('DESCRIBE prontuario')->result_array();
        $nome_colunas = array();
        if( !empty($resultado) ) {
            foreach($resultado as $index => $info) {
                $nome_colunas[] = $info['Field'];
            }
        }

        fputcsv($output, $nome_colunas);

        $conteudo = $ci->db->query("SELECT `id_prontuario`, paciente.nome_completo , usuario.nome_usuario, `bloqueado`, `adm_visao`, `adm_audicao`, `adm_linguagem`, `adm_cognitivo`, `adm_reflexos_prim`, `supino_simetria`, `supino_alinhamento`, `supino_movimentacao_ativa`, `supino_obs`, `prono_controle_cervical`, `prono_controle_escapular`, `prono_simetria`, `prono_alinhamento`, `prono_movimentacao_ativa`, `prono_obs`, `rolar`, `sentado_controle_cervical`, `sentado_controle_tronco`, `sentando_simetria`, `sentado_alinhamento`, `sentado_movimentacao_ativa`, `sentado_obs`, `sentado_troca_postural`, `sentado_postura_quadril`, `sentado_deformidade_coluna`, `sentado_deformidade_quadril`, `engatinhar`, `engatinhar_obs`, `arrastar`, `arrastar_obs`, `ortostatismo`, `marcha`, `marcha_obs`, `observacao`, `tonus_base_hipertonia_elastica`, `tonus_base_hipertonia_elastica_sinais_clinicos`, `tonus_base_asworth`, `tonus_base_hipertonia_plastica`, `tonus_base_hipertonia_plastica_sinais_clinicos`, `tonus_base_discinesias`, `tonus_base_hipotonia`, `tonus_base_incordenacao_movimentos`, `tonus_dinamico`, `encurtamento_musculares_deformidades`, `forca_muscular_mms_gm`, `forca_muscular_mms_d`, `forca_muscular_mms_e`, `forca_muscular_mmii_gm`, `forca_muscular_mmii_d`, `forca_muscular_mmii_e`, `mrp_reacoes_endireitamento_postura_sentada`, `mrp_reacoes_endireitamento_bipede`, `mrp_reacoes_equilibrio_postura_sentada`, `mrp_reacoes_equilibrio_bipede`, `mrp_reacoes_protecao_postura_sentada`, `mrp_reacoes_protecao_bipede`, `atividades_vida_diaria_alimentacao`, `atividades_vida_diaria_alimentacao_obs`, `atividades_vida_diaria_higiene`, `atividades_vida_diaria_higiene_obs`, `atividades_vida_diaria_vestuario`, `atividades_vida_diaria_vestuario_obs`, `atividades_vida_diaria_locomocao`, `atividades_vida_diaria_locomocao_obs`, `sistema_respiratorio`, `objetivos`, `condutas`, `evolucao_periodo`, prontuario.`data_atualizacao`, prontuario.`data_criacao` FROM `prontuario` JOIN paciente on prontuario.id_paciente = paciente.id_paciente JOIN usuario on prontuario.id_usuario = usuario.id_usuario");
        while ( $row = $conteudo->unbuffered_row('array') ) {
            
            $row_filtrada = array();

            foreach($row as $campo => $valor) {
                if( strpos($valor, '{') !== false ) {
                    $temp = json_decode($valor, true);
                    $temp = implode('/', $temp);
                    $row_filtrada[] = $temp;
                } else {
                    $row_filtrada[] = $valor;
                }
            }
            fputcsv($output, $row_filtrada);
        }
    }

}

if ( !function_exists('exportaEstatisticas') ) {

    function exportaEstatisticas($nomeArquivo)
    {
        $ci = &get_instance();

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='. $nomeArquivo .'.csv');

        $output = fopen('php://output', 'w');

        $meses = array(
            1 => 'Janeiro',
            'Fevereiro',
            'Março',
            'Abril',
            'Maio',
            'Junho',
            'Julho',
            'Agosto',
            'Setembro',
            'Outubro',
            'Novembro',
            'Dezembro'
        );

        // pegar todos os pacientes cadastrados
        $pacientes = $ci->db->get('paciente')->result_array();
        
        // [ANO] => [MES] = QNT CADASTRADOS
        $paciente_datas = array();
        
        foreach($pacientes as $index => $info) {
            $data_criacao = new DateTime($info['data_criacao']);
            // criar ano se nao existir
            if( !array_key_exists( $data_criacao->format('Y'), $paciente_datas ) ) $paciente_datas[ $data_criacao->format('Y') ] = array();
            // criar mes se nao existir
            if( !array_key_exists( $data_criacao->format('m'), $paciente_datas[ $data_criacao->format('Y') ] ) ) $paciente_datas[ $data_criacao->format('Y') ][ $data_criacao->format('n') ] = 0;
            // adicionar na lista
            $paciente_datas[ $data_criacao->format('Y') ][ $data_criacao->format('n') ] += 1;
        }

        // pegar todos as consultas cadastrados
        $consultas = $ci->db->get('consulta')->result_array();
        
        // [ANO] => [MES] = QNT CADASTRADOS
        $consulta_datas = array();
        
        foreach($consultas as $index => $info) {
            $data_criacao = new DateTime($info['data_criacao']);
            // criar ano se nao existir
            if( !array_key_exists( $data_criacao->format('Y'), $consulta_datas ) ) $consulta_datas[ $data_criacao->format('Y') ] = array();
            // criar mes se nao existir
            if( !array_key_exists( $data_criacao->format('m'), $consulta_datas[ $data_criacao->format('Y') ] ) ) $consulta_datas[ $data_criacao->format('Y') ][ $data_criacao->format('n') ] = 0;
            // adicionar na lista
            $consulta_datas[ $data_criacao->format('Y') ][ $data_criacao->format('n') ] += 1;
        }

        function contarValor($ano, $arr){
            $resultado = 0;
            if( isset($arr[$ano]) ) {
                foreach($arr[$ano] as $mes => $qnt) {
                    $resultado += $qnt;
                }
            }
            return $resultado;
        }

        $mesclar_todos_anos = array_unique( array_merge( array_keys($paciente_datas), array_keys($consulta_datas) ) );

        $nome_colunas = array('ANO', 'MES', 'QUANTIDADE DE CONSULTAS NO MES', 'QUANTIDADE DE PACIENTES CADASTRADOS NO MES');
        fputcsv($output, $nome_colunas);
        
        foreach($mesclar_todos_anos as $index => $ano) {
            
            foreach($meses as $num => $nome) {
                $linha = array($ano, $nome, ( isset($consulta_datas[$ano][$num]) ) ? $consulta_datas[$ano][$num] : 0, ( isset($paciente_datas[$ano][$num]) ) ? $paciente_datas[$ano][$num] : 0 );
                fputcsv($output, $linha);
            }
            fputcsv($output, [ 'TOTAL', NULL , contarValor($ano, $consulta_datas), contarValor($ano, $paciente_datas) ]);

            fputcsv($output, []);
            fputcsv($output, []);
            fputcsv($output, []);
        }

        fputcsv($output, ['TOTAL DE CADA ANO']);
        fputcsv($output, ['ANO', 'QUANTIDADE DE CONSULTAS NO MES', 'QUANTIDADE DE PACIENTES CADASTRADOS NO MES']);
        foreach($mesclar_todos_anos as $index => $ano) {
            fputcsv($output, [ $ano, contarValor($ano, $consulta_datas), contarValor($ano, $paciente_datas) ]);
        }
    }

}