<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('pront_radio_input'))
{
    function pront_radio_input($fields = array(), $selected = null)
    {
        $html = '';
        $contem_obs = 0;
        $conteudo_obs = '';

        $selecionados = ( $selected !== null ) ? json_decode($selected, true) : [];
        if( isset($selecionados['obs']) ) {
            $conteudo_obs = $selecionados['obs'];
            unset($selecionados['obs']);
        }
        
        foreach($fields as $field) {

            $selected_input = ( is_array($selecionados) && in_array( $field['opcao'] , $selecionados ) ) ? 'checked=""' : '';

            if( $field['obs'] == 's' ) {
                $contem_obs++;
                $obs = 'data-obs="'. $field['campo'] .'"';
            } else {
                $obs = '';
            }
            
            $html .= '<label class="checkbox-inline pront_radio_input">';
            $html .= '<input type="checkbox" name="' . $field['campo'] . '[]" value="' . $field['opcao'] . '"' . $selected_input . ' ' . $obs . '> ' . $field['opcao'];
            $html .= '</label>';

        }
        
        if( $contem_obs !== 0 ) {
            $html .= '<div class="panel-footer hidden" data-obs-parent="'.$fields[0]['campo'].'">';
            $html .=    '<div class="input-group">';
            $html .=        '<div class="input-group-addon">Descrição</div>';
            $html .=        '<input type="text" class="form-control" id="'. $fields[0]['campo'] .'" name="'. $fields[0]['campo'] .'[obs]" disabled="" value="'. $conteudo_obs .'">';
            $html .=    '</div>';
            $html .= '</div>';
        }

        return $html;
    }
}

if ( ! function_exists('paciente_opcoes'))
{
    function paciente_opcoes($fields = array(), $selected = null)
    {
        $html = '';
        $html = '<option value="0">Nenhum</option>';

        foreach($fields as $field) {
            $selected_input = ( $selected == $field['id_opcoes'] ) ? 'selected' : '';
            $html .= '<option value="' . $field['id_opcoes'] . '"' . $selected_input . ' > ' . $field['opcao'] . '</option>';
        }
        return $html;
    }
}