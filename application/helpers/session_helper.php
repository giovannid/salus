<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( !function_exists('session_visao') ) {

    function session_visao( $nivel_de_accesso ) {
        $ci = &get_instance();
                
        if( in_array( $ci->session->usuario['tipo_usuario'], $nivel_de_accesso ) ) {
            return true;
        } 
        return false;
    }

}