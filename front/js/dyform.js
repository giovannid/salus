'use strict';

/**
 * DynamicForm
 * 
 * Copyright 2016 Giovanni D.
 */
(function ($) {

    $.fn.dyform = function (options) {

        var form = this;
        var name = this.prop('id');

        var settings = $.extend({
            saveButton: '#' + name + '_save'
        }, options);

        $(document.body).on('click', settings.saveButton, function (e) {
            e.preventDefault();
            form.submit();
        });
    };
})(jQuery);