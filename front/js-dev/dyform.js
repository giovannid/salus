/**
 * DynamicForm
 * 
 * Copyright 2016 Giovanni D.
 */
(function($){

    $.fn.dyform = function (options){

        let form = this;
        let name = this.prop('id');

        let settings = $.extend({
            saveButton: `#${name}_save`,
        }, options);

        $(document.body).on('click', settings.saveButton, function(e){
            e.preventDefault();
            form.submit();
        });

    }

}(jQuery));