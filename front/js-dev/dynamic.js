/**
 * Dynamic v1.0.0
 * 
 * Copyright 2016 Giovanni D.
 */
(function($){

    $.fn.dynamic = function (options){

        let form = this;
        let name = this.prop('id');
        let next_elem = -1;
        
        this.find('input').each(function(){
            if( $(this).data('name') === name ) {
                next_elem++;
            }
        });

        let settings = $.extend({
            template: ``,
            removeButton: `${name}_remove_current`,
            addButton: `${name}_add_new`,
            parentToAdd: `${name}_parent`,
            emptyMessage: ``
        }, options);
        
        $(`#${settings.addButton}`).on('click', function(){

            if( countInputs() === 0 ) {
                $(`#${settings.parentToAdd}`).html(''); 
            }

            let temp_template = settings.template;

            // update name
            temp_template = temp_template.split("{{name}}").join(name);
            
            // update remove
            temp_template = temp_template.split("{{remove}}").join(name + "_remove_current");
           
            // update index
            next_elem++;
            temp_template = temp_template.split("{{index}}").join(next_elem);

            // update id
            temp_template = temp_template.split("{{id}}").join(name + "_" + next_elem);

            $(`#${settings.parentToAdd}`).append(temp_template);
            
        });
        
        function countInputs(){
            let counter = 0;
            form.find('input').each(function(){
                if( $(this).data('name') === name ) {
                    counter++;
                }
            });
            return counter;
        }

        $(document.body).on('click', `#${settings.removeButton}`, function(){
            let fid = $(this).data('remove');
            $(`#${name}_${fid}`).remove();
            
            if ( countInputs() < 1 ) {
                let temp_emptyMessage = settings.emptyMessage;
                temp_emptyMessage = temp_emptyMessage.split("{{name}}").join(name);

                $(`#${settings.parentToAdd}`).append(temp_emptyMessage); 
            }
        });

    };

}(jQuery));