$('document').ready(function(){

/****************************************************************
* PADRAO
****************************************************************/

/**
 *  Inicializar o popover do bootstrap
 */
$('[data-toggle="popover"]').popover();

$('[data-toggle="popover"]').on('shown.bs.popover', function(e) {
    let botao_cancelar = $(e.target.dataset.cancelar);
    
    if(typeof botao_cancelar[0] !== undefined ) {
        botao_cancelar[0].addEventListener('click', function(e){
            $('[data-toggle="popover"]').popover('hide');
        });
    }
});

/**
 *  Inicializar o tooltip do bootstrap
 */
$('[data-toggle="tooltip"]').tooltip()



/**
 * Abas na URL 
 */

let hash = window.location.hash;
let hashCerta = '#' + hash.split('#')[1];
hash && $('#abas_padrao a[href="' + hashCerta + '"]').tab('show');

$('#abas_padrao a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    window.location.hash = this.hash;
});


/****************************************************************
* PRONTUARIO
****************************************************************/

/*
* Botão Salvar Prontuario 
*
* Quando clicar no botão SALVAR, executar a funcao `submit()` do formulario.
*/

$('#prontuarios_form').dyform({
    saveButton: '#salvarProntuario'
});


/**
 * Ativar campo obs. quando existir para uma opção do prontuário
 */

let campos_obs = [];

$('[data-obs]').each(function(){
    
    let campo = $(this).data('obs');
    if( campos_obs.indexOf(campo) == -1 ) campos_obs.push(campo);

});

jQuery.each(campos_obs, function(index, item) {

    /**
     * Se algum checkbox com data-obs já está 'checked', mostrar o input de descricao
     */
    $(`[data-obs=${item}]`).each(function(){
        if( $(this).is(":checked") ) {
            $(`[data-obs-parent=${item}]`).first().removeClass('hidden');
            $(`#${item}`).prop("disabled", false);
        }
    });
    
    /**
     * Quando um checkbox mudar de estado [checked|unchecked] verificar se existe mais um checkbox
     * do mesmo campo 'checked', se sim, não fazer nada, se não, desativar o input de descricao
     */
    $(`[data-obs=${item}]`).on('click', function(e){

        let outros_checado = false;

        $(`[data-obs=${item}]`).each(function(){
            if( $(this).is(":checked") ) outros_checado++;
        });

        if(outros_checado == false) {
            $(`[data-obs-parent=${item}]`).first().addClass('hidden');
            $(`#${item}`).attr('value', '');
            $(`#${item}`).prop("disabled", true);
        } else if(outros_checado >= 1){
            $(`[data-obs-parent=${item}]`).first().removeClass('hidden');
            $(`#${item}`).prop("disabled", false);
        }

    });

});

/***************************************************************
 * BATERIA
 **************************************************************/

/*
* Botão Salvar Bateria 
*
* Quando clicar no botão SALVAR, executar a funcao `submit()` do formulario.
*/

$('#baterias_form').dyform({
    saveButton: '#salvarBateria'
});

/**
 * Calendario para pagina de cadastro de bateria
 */

$('.bateria_picker').datepicker({
    format: "dd/mm/yyyy",
    daysOfWeekDisabled: "0,6",
    todayHighlight: true,
    autoclose: true,
    language: "pt-BR",
    todayBtn: "linked"
});

/***************************************************************
 * USUARIO
 **************************************************************/

/*
* Botão Salvar Usuario 
*
* Quando clicar no botão SALVAR, executar a funcao `submit()` do formulario.
*/

$('#usuarios_form').dyform({
    saveButton: '#salvarUsuario'
});

/**
 * Botão Salvar Usuário Paciente
 * 
 * Salvar vinculo entre Usuário e Pacientes
 */
$('#usuario_pacientes_form').dyform({
    saveButton: '#salvarUsuarioPaciente'
});

/**
 * Vincular Pacientes
 * 
 * Codigo para lista de pacientes vinculados
 */

let pacientes_vinculados = $('#pacientes_vinculados');
let pacientes_source     = $('#lista_pacientes');
let usuario_form         = $('#usuario_pacientes_form');

$("#adicionar_paciente").on( "click", function(event){
    event.preventDefault();

    // remover aviso de nenhum
    pacientes_vinculados.children().filter('.nenhum').remove();

    // pegar option selecionado
    let selecionado = pacientes_source.find(':selected');

    // verificar se ja existe esse paciente na lista
    if ( pacientes_vinculados.children().filter( $("[type=hidden]") ).filter( function(){ return $(this).val() === selecionado.val(); } ).length === 0 ) {
        pacientes_source.parent().removeClass("has-error");

        // adicionar nova li
        pacientes_vinculados.append('<li class="list-group-item btn-style06" data-idp="'+ selecionado.val() +'">' + selecionado.text() + ' <span class="botao_rm">Remover</span></li>');

        // adicionar novo hidden
        pacientes_vinculados.append('<input name="pacientes[]" value="'+ selecionado.val() +'" class="hidden" type="hidden">');

    } else {
        pacientes_source.parent().addClass("has-error");
    }
});

$(document.body).on("click", "#pacientes_vinculados .botao_rm", function(event){
    event.preventDefault();

    // pegar a li
    let idp = $(this).parent();

    // remover o hidden com o idp da li
    pacientes_vinculados.find("[value="+ idp.data("idp") +"]").remove();
    
    // remover a li
    idp.remove();

    // verificar se existe algum filho da lista, se não add nenhum
    if( pacientes_vinculados.children().length === 0 ) {
        pacientes_vinculados.append('<li class="list-group-item nenhum">Nenhum paciente vinculado</li>');
    }
});

/**
 * Abrir modal de faltas pela URL
 */
if(window.location.href.indexOf('#faltasUsuario') != -1) {
    $('#faltasUsuario').modal('show');
}


/***************************************************************
 * CANDIDATO
 **************************************************************/

$('#candidato_form').dyform({
    saveButton: '#salvarCandidato'
});

$('#candidato_data_nasc').datepicker({
    format: "dd/mm/yyyy",
    todayHighlight: true,
    autoclose: true,
    language: "pt-BR"
});

//console.log(window.location.href);

//auto complete CID, #cid_search
$( "#cid_search" ).autocomplete({
    source: function( request, response ) {
        $.ajax({
            method: 'GET',
            url: "https://cidapi.herokuapp.com/v1/like/" + request.term,
            crossDomain: true,
            dataType: "json"
        }).done(function( data, textStatus, jqXHR ) {
            if(typeof data.status !== undefined && data.status == 1) {
                let resultado = [];
                data.resultado = data.resultado.slice(0, 50); // limitar 50 resultados
                data.resultado.forEach(function(value, index){
                    resultado.push( value.codigo + ' - ' + value.descricao );
                });
                response(resultado);
            } else {
                response( ['Nada encontrado'] );
            }
        });
    }
} );


/***************************************************************
 * PACIENTE
 **************************************************************/

$('#paciente_form').dyform({
    saveButton: '#salvarPaciente'
});

if(window.location.href.indexOf('#faltasPaciente') != -1) {
    $('#faltasPaciente').modal('show');
}


/***************************************************************
 * CONFIGURACOES
 **************************************************************/

/**
 * Horario Permitidos
 * 
 * Cadastro de horario permitidos para as consultas
 */

let horario_form    = $('#configuracoes_horarios_form');
let horario_lista   = $('#horarios_lista');
let horario_add     = $('#adicionarHorario');

function ativarTimePicker(){
    $('.horario_picker').timepicker({
        showMeridian: false,
        minuteStep: 5
    });
}

ativarTimePicker();

horario_add.on('click', function(e){
    e.preventDefault();

    // remover nenhum se existir
    horario_lista.children().filter('.nenhum').remove();

    // pegar ultimo ID
    let id = horario_lista.children().last().data("hid") + 1 || 1;

    // adicionar li
    let template_li = ` <li class="horario-item" data-hid="${id}">
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="horario-item-nome">Horario ${id}</span>
                                </div>

                                <div class="col-md-7">
                                    <div class="horario-item-inputs">
                                        <div class="horario-item-inicio">
                                            Inicio:
                                            <input class="horario_picker" type="text" name="horario_${id}[]" value="00:00">
                                            <span class=" glyphicon icone-direita glyphicon-arrow-right" aria-hidden="true"></span>
                                        </div>

                                        <div class="horario-item-fim">
                                            Fim:
                                            <input class="horario_picker" type="text" name="horario_${id}[]" value="00:00">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="horario-item-remover">
                                        <span class="glyphicon remover glyphicon-remove" data-hid="${id}"></span>
                                    </div>
                                </div>
                            </div>
                        </li>`;
                
    horario_lista.append(template_li);

    ativarTimePicker();
});

$(document.body).on("click", "#horarios_lista .remover", function(event){
    
    $('#horarios_lista li[data-hid='+ $(this).data('hid') +']').remove();

    if( $(this).data('record') !== undefined ) {
        horario_form.append('<input type="hidden" name="horario_remover[]" value="'+ $(this).data('record') +'"> ');
    }

    if( horario_lista.children().length === 0 ) {
        horario_lista.append('<li class="horario-item nenhum">Nenhum horário cadastrado.</li>');
    }
});

$(document.body).on("ativar_horario_picker", ".horario_picker", function(event){
    console.log(this);
});

$('#configuracoes_horarios_form').dyform({
    saveButton: '#salvarHorario'
});

/**
 * Opções personalizaveis
 * 
 * Cadastro de opções que são usadas no prontuario e nas informacoes do candidato/paciente
 */

let template = `<div class="opcao" id="{{id}}">
    <div class="input-group">
        <input type="text" class="form-control" data-name="{{name}}" name="{{name}}[{{index}}][opcao]" placeholder="Nome da Opção">
        <span class="input-group-addon">
            <label>
                <input type="checkbox" name="{{name}}[{{index}}][obs]" value="1">
                Precisa Descrição?
            </label>
        </span>
        <span class="input-group-btn">
            <button class="btn btn-style08" type="button" id="{{remove}}" data-remove="{{index}}"> 
                <span class="glyphicon glyphicon-remove opcao_remover"></span>
            </button>
        </span>
    </div> <!-- input-group -->
</div> <!-- opcao -->
`;

let vazio = '<span class="vazio">Não existem opções cadastradas.</span> <input type="hidden" name="{{name}}" value="0">';

$("div.dynamic_here").each(function(){
    $(this).dynamic({
        template: template,
        emptyMessage: vazio
    });
});

$("#configuracoes_opcoes_prontuarios_salvar").on('click', function(e){
    $("#configuracoes_opcoes_prontuarios").submit();
});

/***************************************************************
 * CONSULTAS
 **************************************************************/

$('#consulta_data').datepicker({
    format: "dd/mm/yyyy",
    daysOfWeekDisabled: "0,6",
    todayHighlight: true,
    autoclose: true,
    language: "pt-BR",
    todayBtn: "linked"
});

$('.consulta_horario_campo_data').datepicker({
    format: "dd/mm/yyyy",
    daysOfWeekDisabled: "0,6",
    todayHighlight: true,
    autoclose: true,
    language: "pt-BR",
    todayBtn: "linked"
});

$('#consultas_form').dyform({
    saveButton: '#salvarConsulta'
});

$('#consultas_form_parte1').dyform({
    saveButton: '#enviarCadastrarConsulta'
});

$('#consulta_editar_paciente_compareceu').on('change', function() {
    if(this.value == "0")
    {
        $("#consulta_editar_justificacao_paciente").show();
    } else
    {
       $("#consulta_editar_justificacao_paciente").hide();
    }
});

$('#consulta_editar_usuario_compareceu').on('change', function() {
    if(this.value == "0")
    {
        $("#consulta_editar_justificacao_usuario").show();
    } else
    {
       $("#consulta_editar_justificacao_usuario").hide();
    }
});


/**
 * FORM VALIDATION
 */

$("form").validate({
    errorElement: "span"
});

$(".alta_form").validate({
    errorElement: "span"
});

}); // document ready ends here
