-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 15, 2016 at 03:56 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salus`
--

-- --------------------------------------------------------

--
-- Table structure for table `opcoes`
--

-- CREATE TABLE `opcoes` (
--   `id_opcoes` int(11) NOT NULL,
--   `campo` varchar(255) NOT NULL,
--   `opcao` varchar(100) DEFAULT NULL,
--   `obs` char(1) DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opcoes`
--

INSERT INTO `opcoes` (`id_opcoes`, `campo`, `opcao`, `obs`) VALUES
(1, 'adm_visao', 'presente', NULL),
(2, 'adm_visao', 'ausente', NULL),
(3, 'adm_visao', 'parcial - acompanha objeto mas não fixa', NULL),
(4, 'adm_audicao', 'presente', NULL),
(5, 'adm_audicao', 'ausente', NULL),
(6, 'adm_audicao', 'parcial - acompanha sons', NULL),
(7, 'adm_linguagem', 'presente', NULL),
(8, 'adm_linguagem', 'ausente', NULL),
(9, 'adm_linguagem', 'se comunica olhar', NULL),
(10, 'adm_linguagem', 'se comunica gestos', NULL),
(11, 'adm_cognitivo', 'adequado para idade', NULL),
(12, 'adm_cognitivo', 'inadequado para idade', NULL),
(13, 'adm_cognitivo', 'entende ordens simples', NULL),
(14, 'adm_cognitivo', 'contactua - qual resposta?', 's'),
(15, 'adm_cognitivo', 'não contactua', NULL),
(16, 'adm_reflexos_prim', 'integrados', NULL),
(17, 'adm_reflexos_prim', 'presente', 's'),
(18, 'supino_simetria', 'presente', NULL),
(19, 'supino_simetria', 'ausente', NULL),
(20, 'supino_alinhamento', 'presente', NULL),
(21, 'supino_alinhamento', 'ausente', NULL),
(22, 'supino_movimentacao_ativa', 'presente', NULL),
(23, 'supino_movimentacao_ativa', 'ausente', NULL),
(24, 'supino_movimentacao_ativa', 'hipoativa', NULL),
(25, 'supino_movimentacao_ativa', 'agitação', NULL),
(26, 'prono_controle_cervical', 'ausente', NULL),
(27, 'prono_controle_cervical', 'incompleto', NULL),
(28, 'prono_controle_cervical', 'presente', NULL),
(29, 'prono_controle_escapular', 'ausente', NULL),
(30, 'prono_controle_escapular', 'incompleto', NULL),
(31, 'prono_controle_escapular', 'presente', NULL),
(32, 'prono_simetria', 'presente', NULL),
(33, 'prono_simetria', 'ausente', NULL),
(34, 'prono_alinhamento', 'presente', NULL),
(35, 'prono_alinhamento', 'ausente', NULL),
(36, 'prono_movimentacao_ativa', 'presente', NULL),
(37, 'prono_movimentacao_ativa', 'ausente', NULL),
(38, 'prono_movimentacao_ativa', 'hipoativa', NULL),
(39, 'prono_movimentacao_ativa', 'agitação', NULL),
(40, 'rolar', 'realiza', NULL),
(41, 'rolar', 'não realiza', NULL),
(42, 'rolar', 'inicia, porém incompleto - decúbito para direita', NULL),
(44, 'rolar', 'inicia, porém incompleto - decúbito para esquerda', NULL),
(45, 'rolar', 'uso de padrão patológico - qual?', 's'),
(46, 'rolar', 'com dissociação', NULL),
(47, 'rolar', 'sem dissociação', NULL),
(48, 'sentado_controle_cervical', 'ausente', NULL),
(49, 'sentado_controle_cervical', 'incompleto', NULL),
(50, 'sentado_controle_cervical', 'presente', NULL),
(51, 'sentado_controle_tronco', 'presente', NULL),
(52, 'sentado_controle_tronco', 'ausente', NULL),
(53, 'sentado_controle_tronco', 'incompleto', NULL),
(54, 'sentando_simetria', 'presente', NULL),
(55, 'sentando_simetria', 'ausente', NULL),
(56, 'sentado_alinhamento', 'presente', NULL),
(57, 'sentado_alinhamento', 'ausente', NULL),
(58, 'sentado_movimentacao_ativa', 'presente', NULL),
(59, 'sentado_movimentacao_ativa', 'ausente', NULL),
(60, 'sentado_movimentacao_ativa', 'hipoativa', NULL),
(61, 'sentado_movimentacao_ativa', 'agitação', NULL),
(62, 'sentado_postura_quadril', 'alinhada', NULL),
(63, 'sentado_postura_quadril', 'inclinada - direita', NULL),
(64, 'sentado_postura_quadril', 'inclinada - esquerda', NULL),
(65, 'sentado_postura_quadril', 'retroversão', NULL),
(66, 'sentado_postura_quadril', 'anteversão', NULL),
(67, 'sentado_deformidade_coluna', 'ausente', NULL),
(68, 'sentado_deformidade_coluna', 'presente: postural', NULL),
(69, 'sentado_deformidade_coluna', 'presente: fixa', NULL),
(70, 'sentado_deformidade_quadril', 'ausente', NULL),
(71, 'sentado_deformidade_quadril', 'presente - galeazzi - d', NULL),
(72, 'sentado_deformidade_quadril', 'presente - galeazzi - e', NULL),
(73, 'engatinhar', 'sim', NULL),
(74, 'engatinhar', 'não', NULL),
(75, 'arrastar', 'sim', NULL),
(76, 'arrastar', 'não', NULL),
(77, 'ortostatismo', 'presente', NULL),
(78, 'ortostatismo', 'ausente', NULL),
(79, 'ortostatismo', 'sustento parcial', NULL),
(80, 'ortostatismo', 'base de apoio aumentada', NULL),
(81, 'ortostatismo', 'base de apoio diminuída', NULL),
(82, 'ortostatismo', 'posicionamento dos pés', 's'),
(83, 'marcha', 'não realiza', NULL),
(84, 'marcha', 'realiza', NULL),
(85, 'tonus_base_hipertonia_elastica', 'sim', 's'),
(86, 'tonus_base_hipertonia_elastica', 'não', NULL),
(87, 'tonus_base_hipertonia_plastica', 'sim', 's'),
(88, 'tonus_base_hipertonia_plastica', 'não', NULL),
(89, 'tonus_base_discinesias', 'atetose', 's'),
(90, 'tonus_base_discinesias', 'coréia', 's'),
(91, 'tonus_base_discinesias', 'distonia', 's'),
(92, 'tonus_base_discinesias', 'não', NULL),
(93, 'tonus_base_hipotonia', 'sim', 's'),
(94, 'tonus_base_hipotonia', 'não', NULL),
(95, 'tonus_base_incordenacao_movimentos', 'ataxia', NULL),
(96, 'tonus_base_incordenacao_movimentos', 'dismetria', NULL),
(97, 'tonus_base_incordenacao_movimentos', 'hipometria', NULL),
(98, 'tonus_base_incordenacao_movimentos', 'hipermetria', NULL),
(99, 'atividades_vida_diaria_alimentacao', 'dependente', NULL),
(100, 'atividades_vida_diaria_alimentacao', 'semi-dependente', NULL),
(101, 'atividades_vida_diaria_alimentacao', 'independente', NULL),
(102, 'atividades_vida_diaria_higiene', 'dependente', NULL),
(103, 'atividades_vida_diaria_higiene', 'semi-dependente', NULL),
(104, 'atividades_vida_diaria_higiene', 'independente', NULL),
(105, 'atividades_vida_diaria_vestuario', 'dependente', NULL),
(106, 'atividades_vida_diaria_vestuario', 'semi-dependente', NULL),
(107, 'atividades_vida_diaria_vestuario', 'independente', NULL),
(108, 'atividades_vida_diaria_locomocao', 'dependente', NULL),
(109, 'atividades_vida_diaria_locomocao', 'semi-dependente', NULL),
(110, 'atividades_vida_diaria_locomocao', 'independente', NULL),
(113, 'adnpm', 'Sim', NULL),
(114, 'adnpm', 'Não', NULL),
(115, 'sindrome_de_down', 'Sim', NULL),
(116, 'sindrome_de_down', 'Não', NULL),
(117, 'paralisia_braquial', 'Sim', NULL),
(118, 'paralisia_braquial', 'Não', NULL),
(119, 'mielo', 'Sim', NULL),
(120, 'mielo', 'Não', NULL),
(121, 'encefalopatia', 'Sim', NULL),
(122, 'encefalopatia', 'Não', NULL),
(123, 'class_topografia', 'Tetraparesia', NULL),
(124, 'class_topografia', 'Diparesia', NULL),
(125, 'class_topografia', 'Hemiparesia', NULL),
(126, 'class_topografia', 'Dupla hemiparesia', NULL),
(127, 'nivel', 'Leve', NULL),
(128, 'nivel', 'Moderado', NULL),
(129, 'nivel', 'Grave', NULL),
(130, 'historia_molestia', 'Pré natal', NULL),
(131, 'historia_molestia', 'Peri natal', NULL),
(132, 'historia_molestia', 'Pos natal', NULL),
(148, 'horario', 'a:2:{i:0;s:5:"02:00";i:1;s:5:"03:00";}', NULL),
(149, 'horario', 'a:2:{i:0;s:5:"11:00";i:1;s:5:"12:00";}', NULL),
(150, 'horario', 'a:2:{i:0;s:6:"130:00";i:1;s:5:"33:00";}', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `opcoes`
--
ALTER TABLE `opcoes`
  ADD PRIMARY KEY (`id_opcoes`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `opcoes`
--
ALTER TABLE `opcoes`
  MODIFY `id_opcoes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
