-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 09, 2016 at 06:36 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `bateria`
--

CREATE TABLE `bateria` (
  `id_bateria` int(11) NOT NULL,
  `nome_bateria` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `data_inicio` date NOT NULL,
  `data_fim` date NOT NULL,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime NOT NULL,
  `desabilitado` int(11) DEFAULT '0',
  `bloqueado` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `consulta`
--

CREATE TABLE `consulta` (
  `id_consulta` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fim` time NOT NULL,
  `comparecimento_paciente` int(11) DEFAULT NULL,
  `justi_paciente` text COLLATE utf8_unicode_ci,
  `comparecimento_estagiario` int(11) DEFAULT NULL,
  `justi_estagiario` text COLLATE utf8_unicode_ci,
  `observacao_paciente` longtext COLLATE utf8_unicode_ci,
  `observacao_estagiario` longtext COLLATE utf8_unicode_ci,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `opcoes`
--

CREATE TABLE `opcoes` (
  `id_opcoes` int(11) NOT NULL,
  `campo` varchar(255) NOT NULL,
  `opcao` varchar(100) DEFAULT NULL,
  `obs` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opcoes`
--

INSERT INTO `opcoes` (`id_opcoes`, `campo`, `opcao`, `obs`) VALUES
(1, 'adm_visao', 'presente', NULL),
(2, 'adm_visao', 'ausente', NULL),
(3, 'adm_visao', 'parcial - acompanha objeto mas não fixa', NULL),
(4, 'adm_audicao', 'presente', NULL),
(5, 'adm_audicao', 'ausente', NULL),
(6, 'adm_audicao', 'parcial - acompanha sons', NULL),
(7, 'adm_linguagem', 'presente', NULL),
(8, 'adm_linguagem', 'ausente', NULL),
(9, 'adm_linguagem', 'se comunica olhar', NULL),
(10, 'adm_linguagem', 'se comunica gestos', NULL),
(11, 'adm_cognitivo', 'adequado para idade', NULL),
(12, 'adm_cognitivo', 'inadequado para idade', NULL),
(13, 'adm_cognitivo', 'entende ordens simples', NULL),
(14, 'adm_cognitivo', 'contactua - qual resposta?', 's'),
(15, 'adm_cognitivo', 'não contactua', NULL),
(16, 'adm_reflexos_prim', 'integrados', NULL),
(17, 'adm_reflexos_prim', 'presente', 's'),
(18, 'supino_simetria', 'presente', NULL),
(19, 'supino_simetria', 'ausente', NULL),
(20, 'supino_alinhamento', 'presente', NULL),
(21, 'supino_alinhamento', 'ausente', NULL),
(22, 'supino_movimentacao_ativa', 'presente', NULL),
(23, 'supino_movimentacao_ativa', 'ausente', NULL),
(24, 'supino_movimentacao_ativa', 'hipoativa', NULL),
(25, 'supino_movimentacao_ativa', 'agitação', NULL),
(26, 'prono_controle_cervical', 'ausente', NULL),
(27, 'prono_controle_cervical', 'incompleto', NULL),
(28, 'prono_controle_cervical', 'presente', NULL),
(29, 'prono_controle_escapular', 'ausente', NULL),
(30, 'prono_controle_escapular', 'incompleto', NULL),
(31, 'prono_controle_escapular', 'presente', NULL),
(32, 'prono_simetria', 'presente', NULL),
(33, 'prono_simetria', 'ausente', NULL),
(34, 'prono_alinhamento', 'presente', NULL),
(35, 'prono_alinhamento', 'ausente', NULL),
(36, 'prono_movimentacao_ativa', 'presente', NULL),
(37, 'prono_movimentacao_ativa', 'ausente', NULL),
(38, 'prono_movimentacao_ativa', 'hipoativa', NULL),
(39, 'prono_movimentacao_ativa', 'agitação', NULL),
(40, 'rolar', 'realiza', NULL),
(41, 'rolar', 'não realiza', NULL),
(42, 'rolar', 'inicia, porém incompleto - decúbito para direita', NULL),
(44, 'rolar', 'inicia, porém incompleto - decúbito para esquerda', NULL),
(45, 'rolar', 'uso de padrão patológico - qual?', 's'),
(46, 'rolar', 'com dissociação', NULL),
(47, 'rolar', 'sem dissociação', NULL),
(48, 'sentado_controle_cervical', 'ausente', NULL),
(49, 'sentado_controle_cervical', 'incompleto', NULL),
(50, 'sentado_controle_cervical', 'presente', NULL),
(51, 'sentado_controle_tronco', 'presente', NULL),
(52, 'sentado_controle_tronco', 'ausente', NULL),
(53, 'sentado_controle_tronco', 'incompleto', NULL),
(54, 'sentando_simetria', 'presente', NULL),
(55, 'sentando_simetria', 'ausente', NULL),
(56, 'sentado_alinhamento', 'presente', NULL),
(57, 'sentado_alinhamento', 'ausente', NULL),
(58, 'sentado_movimentacao_ativa', 'presente', NULL),
(59, 'sentado_movimentacao_ativa', 'ausente', NULL),
(60, 'sentado_movimentacao_ativa', 'hipoativa', NULL),
(61, 'sentado_movimentacao_ativa', 'agitação', NULL),
(62, 'sentado_postura_quadril', 'alinhada', NULL),
(63, 'sentado_postura_quadril', 'inclinada - direita', NULL),
(64, 'sentado_postura_quadril', 'inclinada - esquerda', NULL),
(65, 'sentado_postura_quadril', 'retroversão', NULL),
(66, 'sentado_postura_quadril', 'anteversão', NULL),
(67, 'sentado_deformidade_coluna', 'ausente', NULL),
(68, 'sentado_deformidade_coluna', 'presente: postural', NULL),
(69, 'sentado_deformidade_coluna', 'presente: fixa', NULL),
(70, 'sentado_deformidade_quadril', 'ausente', NULL),
(71, 'sentado_deformidade_quadril', 'presente - galeazzi - d', NULL),
(72, 'sentado_deformidade_quadril', 'presente - galeazzi - e', NULL),
(73, 'engatinhar', 'sim', NULL),
(74, 'engatinhar', 'não', NULL),
(75, 'arrastar', 'sim', NULL),
(76, 'arrastar', 'não', NULL),
(77, 'ortostatismo', 'presente', NULL),
(78, 'ortostatismo', 'ausente', NULL),
(79, 'ortostatismo', 'sustento parcial', NULL),
(80, 'ortostatismo', 'base de apoio aumentada', NULL),
(81, 'ortostatismo', 'base de apoio diminuída', NULL),
(82, 'ortostatismo', 'posicionamento dos pés', 's'),
(83, 'marcha', 'não realiza', NULL),
(84, 'marcha', 'realiza', NULL),
(85, 'tonus_base_hipertonia_elastica', 'sim', 's'),
(86, 'tonus_base_hipertonia_elastica', 'não', NULL),
(87, 'tonus_base_hipertonia_plastica', 'sim', 's'),
(88, 'tonus_base_hipertonia_plastica', 'não', NULL),
(89, 'tonus_base_discinesias', 'atetose', 's'),
(90, 'tonus_base_discinesias', 'coréia', 's'),
(91, 'tonus_base_discinesias', 'distonia', 's'),
(92, 'tonus_base_discinesias', 'não', NULL),
(93, 'tonus_base_hipotonia', 'sim', 's'),
(94, 'tonus_base_hipotonia', 'não', NULL),
(95, 'tonus_base_incordenacao_movimentos', 'ataxia', NULL),
(96, 'tonus_base_incordenacao_movimentos', 'dismetria', NULL),
(97, 'tonus_base_incordenacao_movimentos', 'hipometria', NULL),
(98, 'tonus_base_incordenacao_movimentos', 'hipermetria', NULL),
(99, 'atividades_vida_diaria_alimentacao', 'dependente', NULL),
(100, 'atividades_vida_diaria_alimentacao', 'semi-dependente', NULL),
(101, 'atividades_vida_diaria_alimentacao', 'independente', NULL),
(102, 'atividades_vida_diaria_higiene', 'dependente', NULL),
(103, 'atividades_vida_diaria_higiene', 'semi-dependente', NULL),
(104, 'atividades_vida_diaria_higiene', 'independente', NULL),
(105, 'atividades_vida_diaria_vestuario', 'dependente', NULL),
(106, 'atividades_vida_diaria_vestuario', 'semi-dependente', NULL),
(107, 'atividades_vida_diaria_vestuario', 'independente', NULL),
(108, 'atividades_vida_diaria_locomocao', 'dependente', NULL),
(109, 'atividades_vida_diaria_locomocao', 'semi-dependente', NULL),
(110, 'atividades_vida_diaria_locomocao', 'independente', NULL),
(113, 'adnpm', 'Sim', NULL),
(114, 'adnpm', 'Não', NULL),
(115, 'sindrome_de_down', 'Sim', NULL),
(116, 'sindrome_de_down', 'Não', NULL),
(117, 'paralisia_braquial', 'Sim', NULL),
(118, 'paralisia_braquial', 'Não', NULL),
(119, 'mielo', 'Sim', NULL),
(120, 'mielo', 'Não', NULL),
(121, 'encefalopatia', 'Sim', NULL),
(122, 'encefalopatia', 'Não', NULL),
(123, 'class_topografia', 'Tetraparesia', NULL),
(124, 'class_topografia', 'Diparesia', NULL),
(125, 'class_topografia', 'Hemiparesia', NULL),
(126, 'class_topografia', 'Dupla hemiparesia', NULL),
(127, 'nivel', 'Leve', NULL),
(128, 'nivel', 'Moderado', NULL),
(129, 'nivel', 'Grave', NULL),
(130, 'historia_molestia', 'Pré natal', NULL),
(131, 'historia_molestia', 'Peri natal', NULL),
(132, 'historia_molestia', 'Pos natal', NULL),
(151, 'horario', '["16:00","17:51"]', NULL),
(152, 'horario', '["09:00","10:00"]', NULL),
(153, 'horario', '["13:00","14:00"]', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `paciente`
--

CREATE TABLE `paciente` (
  `id_paciente` int(11) NOT NULL,
  `candidato` int(11) NOT NULL DEFAULT '1',
  `desabilitado` int(11) NOT NULL DEFAULT '0',
  `desabilitado_motivo` mediumtext COLLATE utf8_unicode_ci,
  `nome_completo` char(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `sexo` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hipotese_diagnostico` text COLLATE utf8_unicode_ci,
  `cid` char(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome_responsavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentesco_resp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `queixa_principal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deficit_funcional` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medico_resp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hospital_procedencia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tratamento_previo` text COLLATE utf8_unicode_ci,
  `adnpm` int(11) DEFAULT NULL,
  `adnpm_motivo` text COLLATE utf8_unicode_ci,
  `sindrome_de_down` int(11) DEFAULT NULL,
  `paralisia_braquial` int(11) DEFAULT NULL,
  `mielo` int(11) DEFAULT NULL,
  `outras_sindromes` longtext COLLATE utf8_unicode_ci,
  `encefalopatia` int(11) DEFAULT NULL,
  `class_topografia` int(11) DEFAULT NULL,
  `class_clinica` text COLLATE utf8_unicode_ci,
  `nivel` int(11) DEFAULT NULL,
  `gmfcs_nivel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `historia_molestia` int(11) DEFAULT NULL,
  `historia_molestia_obs` longtext COLLATE utf8_unicode_ci,
  `patol_disturbio_assoc` longtext COLLATE utf8_unicode_ci,
  `medicamento_uso` longtext COLLATE utf8_unicode_ci,
  `exames_complementares` longtext COLLATE utf8_unicode_ci,
  `orteses_proteses_adaptacoes` longtext COLLATE utf8_unicode_ci,
  `caracteristicas_sindromicas` longtext COLLATE utf8_unicode_ci,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prontuario`
--

CREATE TABLE `prontuario` (
  `id_prontuario` int(11) NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `bloqueado` int(11) NOT NULL DEFAULT '0',
  `adm_visao` mediumtext COLLATE utf8_unicode_ci,
  `adm_audicao` mediumtext COLLATE utf8_unicode_ci,
  `adm_linguagem` mediumtext COLLATE utf8_unicode_ci,
  `adm_cognitivo` mediumtext COLLATE utf8_unicode_ci,
  `adm_reflexos_prim` mediumtext COLLATE utf8_unicode_ci,
  `supino_simetria` mediumtext COLLATE utf8_unicode_ci,
  `supino_alinhamento` mediumtext COLLATE utf8_unicode_ci,
  `supino_movimentacao_ativa` mediumtext COLLATE utf8_unicode_ci,
  `supino_obs` longtext COLLATE utf8_unicode_ci,
  `prono_controle_cervical` mediumtext COLLATE utf8_unicode_ci,
  `prono_controle_escapular` mediumtext COLLATE utf8_unicode_ci,
  `prono_simetria` mediumtext COLLATE utf8_unicode_ci,
  `prono_alinhamento` mediumtext COLLATE utf8_unicode_ci,
  `prono_movimentacao_ativa` mediumtext COLLATE utf8_unicode_ci,
  `prono_obs` longtext COLLATE utf8_unicode_ci,
  `rolar` mediumtext COLLATE utf8_unicode_ci,
  `sentado_controle_cervical` mediumtext COLLATE utf8_unicode_ci,
  `sentado_controle_tronco` mediumtext COLLATE utf8_unicode_ci,
  `sentando_simetria` mediumtext COLLATE utf8_unicode_ci,
  `sentado_alinhamento` mediumtext COLLATE utf8_unicode_ci,
  `sentado_movimentacao_ativa` mediumtext COLLATE utf8_unicode_ci,
  `sentado_obs` longtext COLLATE utf8_unicode_ci,
  `sentado_troca_postural` longtext COLLATE utf8_unicode_ci,
  `sentado_postura_quadril` mediumtext COLLATE utf8_unicode_ci,
  `sentado_deformidade_coluna` mediumtext COLLATE utf8_unicode_ci,
  `sentado_deformidade_quadril` mediumtext COLLATE utf8_unicode_ci,
  `engatinhar` mediumtext COLLATE utf8_unicode_ci,
  `engatinhar_obs` longtext COLLATE utf8_unicode_ci,
  `arrastar` mediumtext COLLATE utf8_unicode_ci,
  `arrastar_obs` longtext COLLATE utf8_unicode_ci,
  `ortostatismo` mediumtext COLLATE utf8_unicode_ci,
  `marcha` mediumtext COLLATE utf8_unicode_ci,
  `marcha_obs` longtext COLLATE utf8_unicode_ci,
  `observacao` longtext COLLATE utf8_unicode_ci,
  `tonus_base_hipertonia_elastica` mediumtext COLLATE utf8_unicode_ci,
  `tonus_base_hipertonia_elastica_sinais_clinicos` longtext COLLATE utf8_unicode_ci,
  `tonus_base_asworth` longtext COLLATE utf8_unicode_ci,
  `tonus_base_hipertonia_plastica` mediumtext COLLATE utf8_unicode_ci,
  `tonus_base_hipertonia_plastica_sinais_clinicos` longtext COLLATE utf8_unicode_ci,
  `tonus_base_discinesias` mediumtext COLLATE utf8_unicode_ci,
  `tonus_base_hipotonia` mediumtext COLLATE utf8_unicode_ci,
  `tonus_base_incordenacao_movimentos` mediumtext COLLATE utf8_unicode_ci,
  `tonus_dinamico` longtext COLLATE utf8_unicode_ci,
  `encurtamento_musculares_deformidades` longtext COLLATE utf8_unicode_ci,
  `forca_muscular_mms_gm` longtext COLLATE utf8_unicode_ci,
  `forca_muscular_mms_d` longtext COLLATE utf8_unicode_ci,
  `forca_muscular_mms_e` longtext COLLATE utf8_unicode_ci,
  `forca_muscular_mmii_gm` longtext COLLATE utf8_unicode_ci,
  `forca_muscular_mmii_d` longtext COLLATE utf8_unicode_ci,
  `forca_muscular_mmii_e` longtext COLLATE utf8_unicode_ci,
  `mrp_reacoes_endireitamento_postura_sentada` longtext COLLATE utf8_unicode_ci,
  `mrp_reacoes_endireitamento_bipede` longtext COLLATE utf8_unicode_ci,
  `mrp_reacoes_equilibrio_postura_sentada` longtext COLLATE utf8_unicode_ci,
  `mrp_reacoes_equilibrio_bipede` longtext COLLATE utf8_unicode_ci,
  `mrp_reacoes_protecao_postura_sentada` longtext COLLATE utf8_unicode_ci,
  `mrp_reacoes_protecao_bipede` longtext COLLATE utf8_unicode_ci,
  `atividades_vida_diaria_alimentacao` mediumtext COLLATE utf8_unicode_ci,
  `atividades_vida_diaria_alimentacao_obs` longtext COLLATE utf8_unicode_ci,
  `atividades_vida_diaria_higiene` mediumtext COLLATE utf8_unicode_ci,
  `atividades_vida_diaria_higiene_obs` longtext COLLATE utf8_unicode_ci,
  `atividades_vida_diaria_vestuario` mediumtext COLLATE utf8_unicode_ci,
  `atividades_vida_diaria_vestuario_obs` longtext COLLATE utf8_unicode_ci,
  `atividades_vida_diaria_locomocao` mediumtext COLLATE utf8_unicode_ci,
  `atividades_vida_diaria_locomocao_obs` longtext COLLATE utf8_unicode_ci,
  `sistema_respiratorio` longtext COLLATE utf8_unicode_ci,
  `objetivos` longtext COLLATE utf8_unicode_ci,
  `condutas` longtext COLLATE utf8_unicode_ci,
  `evolucao_periodo` longtext COLLATE utf8_unicode_ci,
  `data_atualizacao` datetime NOT NULL,
  `data_criacao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci KEY_BLOCK_SIZE=8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ra` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(11) NOT NULL,
  `id_bateria` int(11) NOT NULL,
  `desabilitado` int(11) NOT NULL DEFAULT '0',
  `forcar_troca_senha` int(11) NOT NULL DEFAULT '1',
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome_usuario`, `email`, `senha`, `ra`, `tipo_usuario`, `id_bateria`, `desabilitado`, `forcar_troca_senha`, `data_criacao`, `data_atualizacao`) VALUES
(1, 'Admin', '1@1.com', '$2y$12$7KFWI/m.MkbxlXn4LyPYRu/BLGYxImhUKr3uRXUaH/2fzkmaP1F6G', '0', 1, 0, 0, 0, '2016-09-10 01:07:40', '2016-10-15 16:09:55'),
(2, 'Supervisor', '2@2.com', '$2y$12$Fd9dZw0qphpJOrOlT2tz0O20J6nIT8Nd0OHsAHWvAd8NLY3oLj3KS', '0', 2, 0, 0, 0, '2016-10-10 17:55:35', '2016-11-28 16:48:45'),
(3, 'Estagiário', '3@3.com', '$2y$12$C2Uqtt4JP4nCPHVWLgmQDumXnYSkZj8YHUFOulWLu6KPQ9KR0zC8S', '0', 3, 2, 0, 0, '2016-10-10 17:56:10', '2016-11-26 15:02:48'),
(4, 'TecLab', '4@4.com', '$2y$12$eqS/RNgEbFF1YQHDPgElWusZzNd6NNBbJdCAA3n23bDNAYlEZjg2C', '0', 4, 0, 0, 0, '2016-10-10 17:59:07', '2016-10-10 18:00:19'),
(5, 'Estagiária', '0@0.com', '$2y$12$2jnHE244ky3YEP43dBtcWeyqJkOUujrrw4wmnm9h3GtXhxVICpWYK', '0', 3, 2, 0, 0, '2016-11-16 23:42:09', '2016-11-26 15:23:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bateria`
--
ALTER TABLE `bateria`
  ADD PRIMARY KEY (`id_bateria`);

--
-- Indexes for table `consulta`
--
ALTER TABLE `consulta`
  ADD PRIMARY KEY (`id_consulta`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_paciente` (`id_paciente`);

--
-- Indexes for table `opcoes`
--
ALTER TABLE `opcoes`
  ADD PRIMARY KEY (`id_opcoes`);

--
-- Indexes for table `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id_paciente`);

--
-- Indexes for table `prontuario`
--
ALTER TABLE `prontuario`
  ADD PRIMARY KEY (`id_prontuario`),
  ADD KEY `id_paciente` (`id_paciente`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bateria`
--
ALTER TABLE `bateria`
  MODIFY `id_bateria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `consulta`
--
ALTER TABLE `consulta`
  MODIFY `id_consulta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `opcoes`
--
ALTER TABLE `opcoes`
  MODIFY `id_opcoes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `paciente`
--
ALTER TABLE `paciente`
  MODIFY `id_paciente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prontuario`
--
ALTER TABLE `prontuario`
  MODIFY `id_prontuario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `consulta`
--
ALTER TABLE `consulta`
  ADD CONSTRAINT `consulta_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `consulta_ibfk_2` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`);

--
-- Constraints for table `prontuario`
--
ALTER TABLE `prontuario`
  ADD CONSTRAINT `prontuario_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`),
  ADD CONSTRAINT `prontuario_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
